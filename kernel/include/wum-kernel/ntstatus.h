/*
 * Copyright 2015 Steven Stewart-Gallus
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
 * implied.  See the License for the specific language governing
 * permissions and limitations under the License.
 */
#ifndef _WUM_KERNEL_NTSTATUS_H
#define _WUM_KERNEL_NTSTATUS_H 1

#include "wum-kernel/ntdef.h"

/* All the status values here have been taken from
 * http://msdn.microsoft.com/en-us/library/cc704588.aspx
 */

#define _WUM_KERNEL_STATUS_SUCCESS ((__wum_kernel_NTSTATUS)0x00000000)
#define _WUM_KERNEL_STATUS_WAIT_0 ((__wum_kernel_NTSTATUS)0x00000000)
#define _WUM_KERNEL_STATUS_WAIT_1 ((__wum_kernel_NTSTATUS)0x00000001)
#define _WUM_KERNEL_STATUS_WAIT_2 ((__wum_kernel_NTSTATUS)0x00000002)
#define _WUM_KERNEL_STATUS_WAIT_3 ((__wum_kernel_NTSTATUS)0x00000003)
#define _WUM_KERNEL_STATUS_WAIT_63 ((__wum_kernel_NTSTATUS)0x0000003F)
#define _WUM_KERNEL_STATUS_ABANDONED ((__wum_kernel_NTSTATUS)0x00000080)
#define _WUM_KERNEL_STATUS_ABANDONED_WAIT_0                            \
	((__wum_kernel_NTSTATUS)0x00000080)
#define _WUM_KERNEL_STATUS_ABANDONED_WAIT_63                           \
	((__wum_kernel_NTSTATUS)0x000000BF)
#define _WUM_KERNEL_STATUS_USER_APC ((__wum_kernel_NTSTATUS)0x000000C0)
#define _WUM_KERNEL_STATUS_ALERTED ((__wum_kernel_NTSTATUS)0x00000101)
#define _WUM_KERNEL_STATUS_TIMEOUT ((__wum_kernel_NTSTATUS)0x00000102)
#define _WUM_KERNEL_STATUS_PENDING ((__wum_kernel_NTSTATUS)0x00000103)
#define _WUM_KERNEL_STATUS_REPARSE ((__wum_kernel_NTSTATUS)0x00000104)
#define _WUM_KERNEL_STATUS_MORE_ENTRIES                                \
	((__wum_kernel_NTSTATUS)0x00000105)
#define _WUM_KERNEL_STATUS_NOT_ALL_ASSIGNED                            \
	((__wum_kernel_NTSTATUS)0x00000106)
#define _WUM_KERNEL_STATUS_SOME_NOT_MAPPED                             \
	((__wum_kernel_NTSTATUS)0x00000107)
#define _WUM_KERNEL_STATUS_OPLOCK_BREAK_IN_PROGRESS                    \
	((__wum_kernel_NTSTATUS)0x00000108)
#define _WUM_KERNEL_STATUS_VOLUME_MOUNTED                              \
	((__wum_kernel_NTSTATUS)0x00000109)
#define _WUM_KERNEL_STATUS_RXACT_COMMITTED                             \
	((__wum_kernel_NTSTATUS)0x0000010A)
#define _WUM_KERNEL_STATUS_NOTIFY_CLEANUP                              \
	((__wum_kernel_NTSTATUS)0x0000010B)
#define _WUM_KERNEL_STATUS_NOTIFY_ENUM_DIR                             \
	((__wum_kernel_NTSTATUS)0x0000010C)
#define _WUM_KERNEL_STATUS_NO_QUOTAS_FOR_ACCOUNT                       \
	((__wum_kernel_NTSTATUS)0x0000010D)
#define _WUM_KERNEL_STATUS_PRIMARY_TRANSPORT_CONNECT_FAILED            \
	((__wum_kernel_NTSTATUS)0x0000010E)
#define _WUM_KERNEL_STATUS_PAGE_FAULT_TRANSITION                       \
	((__wum_kernel_NTSTATUS)0x00000110)
#define _WUM_KERNEL_STATUS_PAGE_FAULT_DEMAND_ZERO                      \
	((__wum_kernel_NTSTATUS)0x00000111)
#define _WUM_KERNEL_STATUS_PAGE_FAULT_COPY_ON_WRITE                    \
	((__wum_kernel_NTSTATUS)0x00000112)
#define _WUM_KERNEL_STATUS_PAGE_FAULT_GUARD_PAGE                       \
	((__wum_kernel_NTSTATUS)0x00000113)
#define _WUM_KERNEL_STATUS_PAGE_FAULT_PAGING_FILE                      \
	((__wum_kernel_NTSTATUS)0x00000114)
#define _WUM_KERNEL_STATUS_CACHE_PAGE_LOCKED                           \
	((__wum_kernel_NTSTATUS)0x00000115)
#define _WUM_KERNEL_STATUS_CRASH_DUMP                                  \
	((__wum_kernel_NTSTATUS)0x00000116)
#define _WUM_KERNEL_STATUS_BUFFER_ALL_ZEROS                            \
	((__wum_kernel_NTSTATUS)0x00000117)
#define _WUM_KERNEL_STATUS_REPARSE_OBJECT                              \
	((__wum_kernel_NTSTATUS)0x00000118)
#define _WUM_KERNEL_STATUS_RESOURCE_REQUIREMENTS_CHANGED               \
	((__wum_kernel_NTSTATUS)0x00000119)
#define _WUM_KERNEL_STATUS_TRANSLATION_COMPLETE                        \
	((__wum_kernel_NTSTATUS)0x00000120)
#define _WUM_KERNEL_STATUS_DS_MEMBERSHIP_EVALUATED_LOCALLY             \
	((__wum_kernel_NTSTATUS)0x00000121)
#define _WUM_KERNEL_STATUS_NOTHING_TO_TERMINATE                        \
	((__wum_kernel_NTSTATUS)0x00000122)
#define _WUM_KERNEL_STATUS_PROCESS_NOT_IN_JOB                          \
	((__wum_kernel_NTSTATUS)0x00000123)
#define _WUM_KERNEL_STATUS_PROCESS_IN_JOB                              \
	((__wum_kernel_NTSTATUS)0x00000124)
#define _WUM_KERNEL_STATUS_VOLSNAP_HIBERNATE_READY                     \
	((__wum_kernel_NTSTATUS)0x00000125)
#define _WUM_KERNEL_STATUS_FSFILTER_OP_COMPLETED_SUCCESSFULLY          \
	((__wum_kernel_NTSTATUS)0x00000126)
#define _WUM_KERNEL_STATUS_INTERRUPT_VECTOR_ALREADY_CONNECTED          \
	((__wum_kernel_NTSTATUS)0x00000127)
#define _WUM_KERNEL_STATUS_INTERRUPT_STILL_CONNECTED                   \
	((__wum_kernel_NTSTATUS)0x00000128)
#define _WUM_KERNEL_STATUS_PROCESS_CLONED                              \
	((__wum_kernel_NTSTATUS)0x00000129)
#define _WUM_KERNEL_STATUS_FILE_LOCKED_WITH_ONLY_READERS               \
	((__wum_kernel_NTSTATUS)0x0000012A)
#define _WUM_KERNEL_STATUS_FILE_LOCKED_WITH_WRITERS                    \
	((__wum_kernel_NTSTATUS)0x0000012B)
#define _WUM_KERNEL_STATUS_RESOURCEMANAGER_READ_ONLY                   \
	((__wum_kernel_NTSTATUS)0x00000202)
#define _WUM_KERNEL_STATUS_WAIT_FOR_OPLOCK                             \
	((__wum_kernel_NTSTATUS)0x00000367)
#define _WUM_MSVCRT_DBG_EXCEPTION_HANDLED                              \
	((__wum_kernel_NTSTATUS)0x00010001)
#define _WUM_MSVCRT_DBG_CONTINUE ((__wum_kernel_NTSTATUS)0x00010002)
#define _WUM_KERNEL_STATUS_FLT_IO_COMPLETE                             \
	((__wum_kernel_NTSTATUS)0x001C0001)
#define _WUM_KERNEL_STATUS_OBJECT_NAME_EXISTS                          \
	((__wum_kernel_NTSTATUS)0x40000000)
#define _WUM_KERNEL_STATUS_THREAD_WAS_SUSPENDED                        \
	((__wum_kernel_NTSTATUS)0x40000001)
#define _WUM_KERNEL_STATUS_WORKING_SET_LIMIT_RANGE                     \
	((__wum_kernel_NTSTATUS)0x40000002)
#define _WUM_KERNEL_STATUS_IMAGE_NOT_AT_BASE                           \
	((__wum_kernel_NTSTATUS)0x40000003)
#define _WUM_KERNEL_STATUS_RXACT_STATE_CREATED                         \
	((__wum_kernel_NTSTATUS)0x40000004)
#define _WUM_KERNEL_STATUS_SEGMENT_NOTIFICATION                        \
	((__wum_kernel_NTSTATUS)0x40000005)
#define _WUM_KERNEL_STATUS_LOCAL_USER_SESSION_KEY                      \
	((__wum_kernel_NTSTATUS)0x40000006)
#define _WUM_KERNEL_STATUS_BAD_CURRENT_DIRECTORY                       \
	((__wum_kernel_NTSTATUS)0x40000007)
#define _WUM_KERNEL_STATUS_SERIAL_MORE_WRITES                          \
	((__wum_kernel_NTSTATUS)0x40000008)
#define _WUM_KERNEL_STATUS_REGISTRY_RECOVERED                          \
	((__wum_kernel_NTSTATUS)0x40000009)
#define _WUM_KERNEL_STATUS_FT_READ_RECOVERY_FROM_BACKUP                \
	((__wum_kernel_NTSTATUS)0x4000000A)
#define _WUM_KERNEL_STATUS_FT_WRITE_RECOVERY                           \
	((__wum_kernel_NTSTATUS)0x4000000B)
#define _WUM_KERNEL_STATUS_SERIAL_COUNTER_TIMEOUT                      \
	((__wum_kernel_NTSTATUS)0x4000000C)
#define _WUM_KERNEL_STATUS_NULL_LM_PASSWORD                            \
	((__wum_kernel_NTSTATUS)0x4000000D)
#define _WUM_KERNEL_STATUS_IMAGE_MACHINE_TYPE_MISMATCH                 \
	((__wum_kernel_NTSTATUS)0x4000000E)
#define _WUM_KERNEL_STATUS_RECEIVE_PARTIAL                             \
	((__wum_kernel_NTSTATUS)0x4000000F)
#define _WUM_KERNEL_STATUS_RECEIVE_EXPEDITED                           \
	((__wum_kernel_NTSTATUS)0x40000010)
#define _WUM_KERNEL_STATUS_RECEIVE_PARTIAL_EXPEDITED                   \
	((__wum_kernel_NTSTATUS)0x40000011)
#define _WUM_KERNEL_STATUS_EVENT_DONE                                  \
	((__wum_kernel_NTSTATUS)0x40000012)
#define _WUM_KERNEL_STATUS_EVENT_PENDING                               \
	((__wum_kernel_NTSTATUS)0x40000013)
#define _WUM_KERNEL_STATUS_CHECKING_FILE_SYSTEM                        \
	((__wum_kernel_NTSTATUS)0x40000014)
#define _WUM_KERNEL_STATUS_FATAL_APP_EXIT                              \
	((__wum_kernel_NTSTATUS)0x40000015)
#define _WUM_KERNEL_STATUS_PREDEFINED_HANDLE                           \
	((__wum_kernel_NTSTATUS)0x40000016)
#define _WUM_KERNEL_STATUS_WAS_UNLOCKED                                \
	((__wum_kernel_NTSTATUS)0x40000017)
#define _WUM_KERNEL_STATUS_SERVICE_NOTIFICATION                        \
	((__wum_kernel_NTSTATUS)0x40000018)
#define _WUM_KERNEL_STATUS_WAS_LOCKED                                  \
	((__wum_kernel_NTSTATUS)0x40000019)
#define _WUM_KERNEL_STATUS_LOG_HARD_ERROR                              \
	((__wum_kernel_NTSTATUS)0x4000001A)
#define _WUM_KERNEL_STATUS_ALREADY_WIN32                               \
	((__wum_kernel_NTSTATUS)0x4000001B)
#define _WUM_KERNEL_STATUS_WX86_UNSIMULATE                             \
	((__wum_kernel_NTSTATUS)0x4000001C)
#define _WUM_KERNEL_STATUS_WX86_CONTINUE                               \
	((__wum_kernel_NTSTATUS)0x4000001D)
#define _WUM_KERNEL_STATUS_WX86_SINGLE_STEP                            \
	((__wum_kernel_NTSTATUS)0x4000001E)
#define _WUM_KERNEL_STATUS_WX86_BREAKPOINT                             \
	((__wum_kernel_NTSTATUS)0x4000001F)
#define _WUM_KERNEL_STATUS_WX86_EXCEPTION_CONTINUE                     \
	((__wum_kernel_NTSTATUS)0x40000020)
#define _WUM_KERNEL_STATUS_WX86_EXCEPTION_LASTCHANCE                   \
	((__wum_kernel_NTSTATUS)0x40000021)
#define _WUM_KERNEL_STATUS_WX86_EXCEPTION_CHAIN                        \
	((__wum_kernel_NTSTATUS)0x40000022)
#define _WUM_KERNEL_STATUS_IMAGE_MACHINE_TYPE_MISMATCH_EXE             \
	((__wum_kernel_NTSTATUS)0x40000023)
#define _WUM_KERNEL_STATUS_NO_YIELD_PERFORMED                          \
	((__wum_kernel_NTSTATUS)0x40000024)
#define _WUM_KERNEL_STATUS_TIMER_RESUME_IGNORED                        \
	((__wum_kernel_NTSTATUS)0x40000025)
#define _WUM_KERNEL_STATUS_ARBITRATION_UNHANDLED                       \
	((__wum_kernel_NTSTATUS)0x40000026)
#define _WUM_KERNEL_STATUS_CARDBUS_NOT_SUPPORTED                       \
	((__wum_kernel_NTSTATUS)0x40000027)
#define _WUM_KERNEL_STATUS_WX86_CREATEWX86TIB                          \
	((__wum_kernel_NTSTATUS)0x40000028)
#define _WUM_KERNEL_STATUS_MP_PROCESSOR_MISMATCH                       \
	((__wum_kernel_NTSTATUS)0x40000029)
#define _WUM_KERNEL_STATUS_HIBERNATED                                  \
	((__wum_kernel_NTSTATUS)0x4000002A)
#define _WUM_KERNEL_STATUS_RESUME_HIBERNATION                          \
	((__wum_kernel_NTSTATUS)0x4000002B)
#define _WUM_KERNEL_STATUS_FIRMWARE_UPDATED                            \
	((__wum_kernel_NTSTATUS)0x4000002C)
#define _WUM_KERNEL_STATUS_DRIVERS_LEAKING_LOCKED_PAGES                \
	((__wum_kernel_NTSTATUS)0x4000002D)
#define _WUM_KERNEL_STATUS_MESSAGE_RETRIEVED                           \
	((__wum_kernel_NTSTATUS)0x4000002E)
#define _WUM_KERNEL_STATUS_SYSTEM_POWERSTATE_TRANSITION                \
	((__wum_kernel_NTSTATUS)0x4000002F)
#define _WUM_KERNEL_STATUS_ALPC_CHECK_COMPLETION_LIST                  \
	((__wum_kernel_NTSTATUS)0x40000030)
#define _WUM_KERNEL_STATUS_SYSTEM_POWERSTATE_COMPLEX_TRANSITION        \
	((__wum_kernel_NTSTATUS)0x40000031)
#define _WUM_KERNEL_STATUS_ACCESS_AUDIT_BY_POLICY                      \
	((__wum_kernel_NTSTATUS)0x40000032)
#define _WUM_KERNEL_STATUS_ABANDON_HIBERFILE                           \
	((__wum_kernel_NTSTATUS)0x40000033)
#define _WUM_KERNEL_STATUS_BIZRULES_NOT_ENABLED                        \
	((__wum_kernel_NTSTATUS)0x40000034)
#define _WUM_KERNEL_STATUS_WAKE_SYSTEM                                 \
	((__wum_kernel_NTSTATUS)0x40000294)
#define _WUM_KERNEL_STATUS_DS_SHUTTING_DOWN                            \
	((__wum_kernel_NTSTATUS)0x40000370)
#define _WUM_MSVCRT_DBG_REPLY_LATER ((__wum_kernel_NTSTATUS)0x40010001)
#define _WUM_MSVCRT_DBG_UNABLE_TO_PROVIDE_HANDLE                       \
	((__wum_kernel_NTSTATUS)0x40010002)
#define _WUM_MSVCRT_DBG_TERMINATE_THREAD                               \
	((__wum_kernel_NTSTATUS)0x40010003)
#define _WUM_MSVCRT_DBG_TERMINATE_PROCESS                              \
	((__wum_kernel_NTSTATUS)0x40010004)
#define _WUM_MSVCRT_DBG_CONTROL_C ((__wum_kernel_NTSTATUS)0x40010005)
#define _WUM_MSVCRT_DBG_PRINTEXCEPTION_C                               \
	((__wum_kernel_NTSTATUS)0x40010006)
#define _WUM_MSVCRT_DBG_RIPEXCEPTION ((__wum_kernel_NTSTATUS)0x40010007)
#define _WUM_MSVCRT_DBG_CONTROL_BREAK                                  \
	((__wum_kernel_NTSTATUS)0x40010008)
#define _WUM_MSVCRT_DBG_COMMAND_EXCEPTION                              \
	((__wum_kernel_NTSTATUS)0x40010009)
#define _WUM_MSVCRT_RPC_NT_UUID_LOCAL_ONLY                             \
	((__wum_kernel_NTSTATUS)0x40020056)
#define _WUM_MSVCRT_RPC_NT_SEND_INCOMPLETE                             \
	((__wum_kernel_NTSTATUS)0x400200AF)
#define _WUM_KERNEL_STATUS_CTX_CDM_CONNECT                             \
	((__wum_kernel_NTSTATUS)0x400A0004)
#define _WUM_KERNEL_STATUS_CTX_CDM_DISCONNECT                          \
	((__wum_kernel_NTSTATUS)0x400A0005)
#define _WUM_KERNEL_STATUS_SXS_RELEASE_ACTIVATION_CONTEXT              \
	((__wum_kernel_NTSTATUS)0x4015000D)
#define _WUM_KERNEL_STATUS_RECOVERY_NOT_NEEDED                         \
	((__wum_kernel_NTSTATUS)0x40190034)
#define _WUM_KERNEL_STATUS_RM_ALREADY_STARTED                          \
	((__wum_kernel_NTSTATUS)0x40190035)
#define _WUM_KERNEL_STATUS_LOG_NO_RESTART                              \
	((__wum_kernel_NTSTATUS)0x401A000C)
#define _WUM_KERNEL_STATUS_VIDEO_DRIVER_DEBUG_REPORT_REQUEST           \
	((__wum_kernel_NTSTATUS)0x401B00EC)
#define _WUM_KERNEL_STATUS_GRAPHICS_PARTIAL_DATA_POPULATED             \
	((__wum_kernel_NTSTATUS)0x401E000A)
#define _WUM_KERNEL_STATUS_GRAPHICS_DRIVER_MISMATCH                    \
	((__wum_kernel_NTSTATUS)0x401E0117)
#define _WUM_KERNEL_STATUS_GRAPHICS_MODE_NOT_PINNED                    \
	((__wum_kernel_NTSTATUS)0x401E0307)
#define _WUM_KERNEL_STATUS_GRAPHICS_NO_PREFERRED_MODE                  \
	((__wum_kernel_NTSTATUS)0x401E031E)
#define _WUM_KERNEL_STATUS_GRAPHICS_DATASET_IS_EMPTY                   \
	((__wum_kernel_NTSTATUS)0x401E034B)
#define _WUM_KERNEL_STATUS_GRAPHICS_NO_MORE_ELEMENTS_IN_DATASET        \
	((__wum_kernel_NTSTATUS)0x401E034C)
#define _WUM_KERNEL_STATUS_GRAPHICS_PATH_CONTENT_GEOMETRY_TRANSFORMATION_NOT_PINNED \
	((__wum_kernel_NTSTATUS)0x401E0351)
#define _WUM_KERNEL_STATUS_GRAPHICS_UNKNOWN_CHILD_STATUS               \
	((__wum_kernel_NTSTATUS)0x401E042F)
#define _WUM_KERNEL_STATUS_GRAPHICS_LEADLINK_START_DEFERRED            \
	((__wum_kernel_NTSTATUS)0x401E0437)
#define _WUM_KERNEL_STATUS_GRAPHICS_POLLING_TOO_FREQUENTLY             \
	((__wum_kernel_NTSTATUS)0x401E0439)
#define _WUM_KERNEL_STATUS_GRAPHICS_START_DEFERRED                     \
	((__wum_kernel_NTSTATUS)0x401E043A)
#define _WUM_KERNEL_STATUS_NDIS_INDICATION_REQUIRED                    \
	((__wum_kernel_NTSTATUS)0x40230001)
#define _WUM_KERNEL_STATUS_GUARD_PAGE_VIOLATION                        \
	((__wum_kernel_NTSTATUS)0x80000001)
#define _WUM_KERNEL_STATUS_DATATYPE_MISALIGNMENT                       \
	((__wum_kernel_NTSTATUS)0x80000002)
#define _WUM_KERNEL_STATUS_BREAKPOINT                                  \
	((__wum_kernel_NTSTATUS)0x80000003)
#define _WUM_KERNEL_STATUS_SINGLE_STEP                                 \
	((__wum_kernel_NTSTATUS)0x80000004)
#define _WUM_KERNEL_STATUS_BUFFER_OVERFLOW                             \
	((__wum_kernel_NTSTATUS)0x80000005)
#define _WUM_KERNEL_STATUS_NO_MORE_FILES                               \
	((__wum_kernel_NTSTATUS)0x80000006)
#define _WUM_KERNEL_STATUS_WAKE_SYSTEM_DEBUGGER                        \
	((__wum_kernel_NTSTATUS)0x80000007)
#define _WUM_KERNEL_STATUS_HANDLES_CLOSED                              \
	((__wum_kernel_NTSTATUS)0x8000000A)
#define _WUM_KERNEL_STATUS_NO_INHERITANCE                              \
	((__wum_kernel_NTSTATUS)0x8000000B)
#define _WUM_KERNEL_STATUS_GUID_SUBSTITUTION_MADE                      \
	((__wum_kernel_NTSTATUS)0x8000000C)
#define _WUM_KERNEL_STATUS_PARTIAL_COPY                                \
	((__wum_kernel_NTSTATUS)0x8000000D)
#define _WUM_KERNEL_STATUS_DEVICE_PAPER_EMPTY                          \
	((__wum_kernel_NTSTATUS)0x8000000E)
#define _WUM_KERNEL_STATUS_DEVICE_POWERED_OFF                          \
	((__wum_kernel_NTSTATUS)0x8000000F)
#define _WUM_KERNEL_STATUS_DEVICE_OFF_LINE                             \
	((__wum_kernel_NTSTATUS)0x80000010)
#define _WUM_KERNEL_STATUS_DEVICE_BUSY                                 \
	((__wum_kernel_NTSTATUS)0x80000011)
#define _WUM_KERNEL_STATUS_NO_MORE_EAS                                 \
	((__wum_kernel_NTSTATUS)0x80000012)
#define _WUM_KERNEL_STATUS_INVALID_EA_NAME                             \
	((__wum_kernel_NTSTATUS)0x80000013)
#define _WUM_KERNEL_STATUS_EA_LIST_INCONSISTENT                        \
	((__wum_kernel_NTSTATUS)0x80000014)
#define _WUM_KERNEL_STATUS_INVALID_EA_FLAG                             \
	((__wum_kernel_NTSTATUS)0x80000015)
#define _WUM_KERNEL_STATUS_VERIFY_REQUIRED                             \
	((__wum_kernel_NTSTATUS)0x80000016)
#define _WUM_KERNEL_STATUS_EXTRANEOUS_INFORMATION                      \
	((__wum_kernel_NTSTATUS)0x80000017)
#define _WUM_KERNEL_STATUS_RXACT_COMMIT_NECESSARY                      \
	((__wum_kernel_NTSTATUS)0x80000018)
#define _WUM_KERNEL_STATUS_NO_MORE_ENTRIES                             \
	((__wum_kernel_NTSTATUS)0x8000001A)
#define _WUM_KERNEL_STATUS_FILEMARK_DETECTED                           \
	((__wum_kernel_NTSTATUS)0x8000001B)
#define _WUM_KERNEL_STATUS_MEDIA_CHANGED                               \
	((__wum_kernel_NTSTATUS)0x8000001C)
#define _WUM_KERNEL_STATUS_BUS_RESET ((__wum_kernel_NTSTATUS)0x8000001D)
#define _WUM_KERNEL_STATUS_END_OF_MEDIA                                \
	((__wum_kernel_NTSTATUS)0x8000001E)
#define _WUM_KERNEL_STATUS_BEGINNING_OF_MEDIA                          \
	((__wum_kernel_NTSTATUS)0x8000001F)
#define _WUM_KERNEL_STATUS_MEDIA_CHECK                                 \
	((__wum_kernel_NTSTATUS)0x80000020)
#define _WUM_KERNEL_STATUS_SETMARK_DETECTED                            \
	((__wum_kernel_NTSTATUS)0x80000021)
#define _WUM_KERNEL_STATUS_NO_DATA_DETECTED                            \
	((__wum_kernel_NTSTATUS)0x80000022)
#define _WUM_KERNEL_STATUS_REDIRECTOR_HAS_OPEN_HANDLES                 \
	((__wum_kernel_NTSTATUS)0x80000023)
#define _WUM_KERNEL_STATUS_SERVER_HAS_OPEN_HANDLES                     \
	((__wum_kernel_NTSTATUS)0x80000024)
#define _WUM_KERNEL_STATUS_ALREADY_DISCONNECTED                        \
	((__wum_kernel_NTSTATUS)0x80000025)
#define _WUM_KERNEL_STATUS_LONGJUMP ((__wum_kernel_NTSTATUS)0x80000026)
#define _WUM_KERNEL_STATUS_CLEANER_CARTRIDGE_INSTALLED                 \
	((__wum_kernel_NTSTATUS)0x80000027)
#define _WUM_KERNEL_STATUS_PLUGPLAY_QUERY_VETOED                       \
	((__wum_kernel_NTSTATUS)0x80000028)
#define _WUM_KERNEL_STATUS_UNWIND_CONSOLIDATE                          \
	((__wum_kernel_NTSTATUS)0x80000029)
#define _WUM_KERNEL_STATUS_REGISTRY_HIVE_RECOVERED                     \
	((__wum_kernel_NTSTATUS)0x8000002A)
#define _WUM_KERNEL_STATUS_DLL_MIGHT_BE_INSECURE                       \
	((__wum_kernel_NTSTATUS)0x8000002B)
#define _WUM_KERNEL_STATUS_DLL_MIGHT_BE_INCOMPATIBLE                   \
	((__wum_kernel_NTSTATUS)0x8000002C)
#define _WUM_KERNEL_STATUS_STOPPED_ON_SYMLINK                          \
	((__wum_kernel_NTSTATUS)0x8000002D)
#define _WUM_KERNEL_STATUS_DEVICE_REQUIRES_CLEANING                    \
	((__wum_kernel_NTSTATUS)0x80000288)
#define _WUM_KERNEL_STATUS_DEVICE_DOOR_OPEN                            \
	((__wum_kernel_NTSTATUS)0x80000289)
#define _WUM_KERNEL_STATUS_DATA_LOST_REPAIR                            \
	((__wum_kernel_NTSTATUS)0x80000803)
#define _WUM_MSVCRT_DBG_EXCEPTION_NOT_HANDLED                          \
	((__wum_kernel_NTSTATUS)0x80010001)
#define _WUM_KERNEL_STATUS_CLUSTER_NODE_ALREADY_UP                     \
	((__wum_kernel_NTSTATUS)0x80130001)
#define _WUM_KERNEL_STATUS_CLUSTER_NODE_ALREADY_DOWN                   \
	((__wum_kernel_NTSTATUS)0x80130002)
#define _WUM_KERNEL_STATUS_CLUSTER_NETWORK_ALREADY_ONLINE              \
	((__wum_kernel_NTSTATUS)0x80130003)
#define _WUM_KERNEL_STATUS_CLUSTER_NETWORK_ALREADY_OFFLINE             \
	((__wum_kernel_NTSTATUS)0x80130004)
#define _WUM_KERNEL_STATUS_CLUSTER_NODE_ALREADY_MEMBER                 \
	((__wum_kernel_NTSTATUS)0x80130005)
#define _WUM_KERNEL_STATUS_COULD_NOT_RESIZE_LOG                        \
	((__wum_kernel_NTSTATUS)0x80190009)
#define _WUM_KERNEL_STATUS_NO_TXF_METADATA                             \
	((__wum_kernel_NTSTATUS)0x80190029)
#define _WUM_KERNEL_STATUS_CANT_RECOVER_WITH_HANDLE_OPEN               \
	((__wum_kernel_NTSTATUS)0x80190031)
#define _WUM_KERNEL_STATUS_TXF_METADATA_ALREADY_PRESENT                \
	((__wum_kernel_NTSTATUS)0x80190041)
#define _WUM_KERNEL_STATUS_TRANSACTION_SCOPE_CALLBACKS_NOT_SET         \
	((__wum_kernel_NTSTATUS)0x80190042)
#define _WUM_KERNEL_STATUS_VIDEO_HUNG_DISPLAY_DRIVER_THREAD_RECOVERED  \
	((__wum_kernel_NTSTATUS)0x801B00EB)
#define _WUM_KERNEL_STATUS_FLT_BUFFER_TOO_SMALL                        \
	((__wum_kernel_NTSTATUS)0x801C0001)
#define _WUM_KERNEL_STATUS_FVE_PARTIAL_METADATA                        \
	((__wum_kernel_NTSTATUS)0x80210001)
#define _WUM_KERNEL_STATUS_FVE_TRANSIENT_STATE                         \
	((__wum_kernel_NTSTATUS)0x80210002)
#define _WUM_KERNEL_STATUS_UNSUCCESSFUL                                \
	((__wum_kernel_NTSTATUS)0xC0000001)
#define _WUM_KERNEL_STATUS_NOT_IMPLEMENTED                             \
	((__wum_kernel_NTSTATUS)0xC0000002)
#define _WUM_KERNEL_STATUS_INVALID_INFO_CLASS                          \
	((__wum_kernel_NTSTATUS)0xC0000003)
#define _WUM_KERNEL_STATUS_INFO_LENGTH_MISMATCH                        \
	((__wum_kernel_NTSTATUS)0xC0000004)
#define _WUM_KERNEL_STATUS_ACCESS_VIOLATION                            \
	((__wum_kernel_NTSTATUS)0xC0000005)
#define _WUM_KERNEL_STATUS_IN_PAGE_ERROR                               \
	((__wum_kernel_NTSTATUS)0xC0000006)
#define _WUM_KERNEL_STATUS_PAGEFILE_QUOTA                              \
	((__wum_kernel_NTSTATUS)0xC0000007)
#define _WUM_KERNEL_STATUS_INVALID_HANDLE                              \
	((__wum_kernel_NTSTATUS)0xC0000008)
#define _WUM_KERNEL_STATUS_BAD_INITIAL_STACK                           \
	((__wum_kernel_NTSTATUS)0xC0000009)
#define _WUM_KERNEL_STATUS_BAD_INITIAL_PC                              \
	((__wum_kernel_NTSTATUS)0xC000000A)
#define _WUM_KERNEL_STATUS_INVALID_CID                                 \
	((__wum_kernel_NTSTATUS)0xC000000B)
#define _WUM_KERNEL_STATUS_TIMER_NOT_CANCELED                          \
	((__wum_kernel_NTSTATUS)0xC000000C)
#define _WUM_KERNEL_STATUS_INVALID_PARAMETER                           \
	((__wum_kernel_NTSTATUS)0xC000000D)
#define _WUM_KERNEL_STATUS_NO_SUCH_DEVICE                              \
	((__wum_kernel_NTSTATUS)0xC000000E)
#define _WUM_KERNEL_STATUS_NO_SUCH_FILE                                \
	((__wum_kernel_NTSTATUS)0xC000000F)
#define _WUM_KERNEL_STATUS_INVALID_DEVICE_REQUEST                      \
	((__wum_kernel_NTSTATUS)0xC0000010)
#define _WUM_KERNEL_STATUS_END_OF_FILE                                 \
	((__wum_kernel_NTSTATUS)0xC0000011)
#define _WUM_KERNEL_STATUS_WRONG_VOLUME                                \
	((__wum_kernel_NTSTATUS)0xC0000012)
#define _WUM_KERNEL_STATUS_NO_MEDIA_IN_DEVICE                          \
	((__wum_kernel_NTSTATUS)0xC0000013)
#define _WUM_KERNEL_STATUS_UNRECOGNIZED_MEDIA                          \
	((__wum_kernel_NTSTATUS)0xC0000014)
#define _WUM_KERNEL_STATUS_NONEXISTENT_SECTOR                          \
	((__wum_kernel_NTSTATUS)0xC0000015)
#define _WUM_KERNEL_STATUS_MORE_PROCESSING_REQUIRED                    \
	((__wum_kernel_NTSTATUS)0xC0000016)
#define _WUM_KERNEL_STATUS_NO_MEMORY ((__wum_kernel_NTSTATUS)0xC0000017)
#define _WUM_KERNEL_STATUS_CONFLICTING_ADDRESSES                       \
	((__wum_kernel_NTSTATUS)0xC0000018)
#define _WUM_KERNEL_STATUS_NOT_MAPPED_VIEW                             \
	((__wum_kernel_NTSTATUS)0xC0000019)
#define _WUM_KERNEL_STATUS_UNABLE_TO_FREE_VM                           \
	((__wum_kernel_NTSTATUS)0xC000001A)
#define _WUM_KERNEL_STATUS_UNABLE_TO_DELETE_SECTION                    \
	((__wum_kernel_NTSTATUS)0xC000001B)
#define _WUM_KERNEL_STATUS_INVALID_SYSTEM_SERVICE                      \
	((__wum_kernel_NTSTATUS)0xC000001C)
#define _WUM_KERNEL_STATUS_ILLEGAL_INSTRUCTION                         \
	((__wum_kernel_NTSTATUS)0xC000001D)
#define _WUM_KERNEL_STATUS_INVALID_LOCK_SEQUENCE                       \
	((__wum_kernel_NTSTATUS)0xC000001E)
#define _WUM_KERNEL_STATUS_INVALID_VIEW_SIZE                           \
	((__wum_kernel_NTSTATUS)0xC000001F)
#define _WUM_KERNEL_STATUS_INVALID_FILE_FOR_SECTION                    \
	((__wum_kernel_NTSTATUS)0xC0000020)
#define _WUM_KERNEL_STATUS_ALREADY_COMMITTED                           \
	((__wum_kernel_NTSTATUS)0xC0000021)
#define _WUM_KERNEL_STATUS_ACCESS_DENIED                               \
	((__wum_kernel_NTSTATUS)0xC0000022)
#define _WUM_KERNEL_STATUS_BUFFER_TOO_SMALL                            \
	((__wum_kernel_NTSTATUS)0xC0000023)
#define _WUM_KERNEL_STATUS_OBJECT_TYPE_MISMATCH                        \
	((__wum_kernel_NTSTATUS)0xC0000024)
#define _WUM_KERNEL_STATUS_NONCONTINUABLE_EXCEPTION                    \
	((__wum_kernel_NTSTATUS)0xC0000025)
#define _WUM_KERNEL_STATUS_INVALID_DISPOSITION                         \
	((__wum_kernel_NTSTATUS)0xC0000026)
#define _WUM_KERNEL_STATUS_UNWIND ((__wum_kernel_NTSTATUS)0xC0000027)
#define _WUM_KERNEL_STATUS_BAD_STACK ((__wum_kernel_NTSTATUS)0xC0000028)
#define _WUM_KERNEL_STATUS_INVALID_UNWIND_TARGET                       \
	((__wum_kernel_NTSTATUS)0xC0000029)
#define _WUM_KERNEL_STATUS_NOT_LOCKED                                  \
	((__wum_kernel_NTSTATUS)0xC000002A)
#define _WUM_KERNEL_STATUS_PARITY_ERROR                                \
	((__wum_kernel_NTSTATUS)0xC000002B)
#define _WUM_KERNEL_STATUS_UNABLE_TO_DECOMMIT_VM                       \
	((__wum_kernel_NTSTATUS)0xC000002C)
#define _WUM_KERNEL_STATUS_NOT_COMMITTED                               \
	((__wum_kernel_NTSTATUS)0xC000002D)
#define _WUM_KERNEL_STATUS_INVALID_PORT_ATTRIBUTES                     \
	((__wum_kernel_NTSTATUS)0xC000002E)
#define _WUM_KERNEL_STATUS_PORT_MESSAGE_TOO_LONG                       \
	((__wum_kernel_NTSTATUS)0xC000002F)
#define _WUM_KERNEL_STATUS_INVALID_PARAMETER_MIX                       \
	((__wum_kernel_NTSTATUS)0xC0000030)
#define _WUM_KERNEL_STATUS_INVALID_QUOTA_LOWER                         \
	((__wum_kernel_NTSTATUS)0xC0000031)
#define _WUM_KERNEL_STATUS_DISK_CORRUPT_ERROR                          \
	((__wum_kernel_NTSTATUS)0xC0000032)
#define _WUM_KERNEL_STATUS_OBJECT_NAME_INVALID                         \
	((__wum_kernel_NTSTATUS)0xC0000033)
#define _WUM_KERNEL_STATUS_OBJECT_NAME_NOT_FOUND                       \
	((__wum_kernel_NTSTATUS)0xC0000034)
#define _WUM_KERNEL_STATUS_OBJECT_NAME_COLLISION                       \
	((__wum_kernel_NTSTATUS)0xC0000035)
#define _WUM_KERNEL_STATUS_PORT_DISCONNECTED                           \
	((__wum_kernel_NTSTATUS)0xC0000037)
#define _WUM_KERNEL_STATUS_DEVICE_ALREADY_ATTACHED                     \
	((__wum_kernel_NTSTATUS)0xC0000038)
#define _WUM_KERNEL_STATUS_OBJECT_PATH_INVALID                         \
	((__wum_kernel_NTSTATUS)0xC0000039)
#define _WUM_KERNEL_STATUS_OBJECT_PATH_NOT_FOUND                       \
	((__wum_kernel_NTSTATUS)0xC000003A)
#define _WUM_KERNEL_STATUS_OBJECT_PATH_SYNTAX_BAD                      \
	((__wum_kernel_NTSTATUS)0xC000003B)
#define _WUM_KERNEL_STATUS_DATA_OVERRUN                                \
	((__wum_kernel_NTSTATUS)0xC000003C)
#define _WUM_KERNEL_STATUS_DATA_LATE_ERROR                             \
	((__wum_kernel_NTSTATUS)0xC000003D)
#define _WUM_KERNEL_STATUS_DATA_ERROR                                  \
	((__wum_kernel_NTSTATUS)0xC000003E)
#define _WUM_KERNEL_STATUS_CRC_ERROR ((__wum_kernel_NTSTATUS)0xC000003F)
#define _WUM_KERNEL_STATUS_SECTION_TOO_BIG                             \
	((__wum_kernel_NTSTATUS)0xC0000040)
#define _WUM_KERNEL_STATUS_PORT_CONNECTION_REFUSED                     \
	((__wum_kernel_NTSTATUS)0xC0000041)
#define _WUM_KERNEL_STATUS_INVALID_PORT_HANDLE                         \
	((__wum_kernel_NTSTATUS)0xC0000042)
#define _WUM_KERNEL_STATUS_SHARING_VIOLATION                           \
	((__wum_kernel_NTSTATUS)0xC0000043)
#define _WUM_KERNEL_STATUS_QUOTA_EXCEEDED                              \
	((__wum_kernel_NTSTATUS)0xC0000044)
#define _WUM_KERNEL_STATUS_INVALID_PAGE_PROTECTION                     \
	((__wum_kernel_NTSTATUS)0xC0000045)
#define _WUM_KERNEL_STATUS_MUTANT_NOT_OWNED                            \
	((__wum_kernel_NTSTATUS)0xC0000046)
#define _WUM_KERNEL_STATUS_SEMAPHORE_LIMIT_EXCEEDED                    \
	((__wum_kernel_NTSTATUS)0xC0000047)
#define _WUM_KERNEL_STATUS_PORT_ALREADY_SET                            \
	((__wum_kernel_NTSTATUS)0xC0000048)
#define _WUM_KERNEL_STATUS_SECTION_NOT_IMAGE                           \
	((__wum_kernel_NTSTATUS)0xC0000049)
#define _WUM_KERNEL_STATUS_SUSPEND_COUNT_EXCEEDED                      \
	((__wum_kernel_NTSTATUS)0xC000004A)
#define _WUM_KERNEL_STATUS_THREAD_IS_TERMINATING                       \
	((__wum_kernel_NTSTATUS)0xC000004B)
#define _WUM_KERNEL_STATUS_BAD_WORKING_SET_LIMIT                       \
	((__wum_kernel_NTSTATUS)0xC000004C)
#define _WUM_KERNEL_STATUS_INCOMPATIBLE_FILE_MAP                       \
	((__wum_kernel_NTSTATUS)0xC000004D)
#define _WUM_KERNEL_STATUS_SECTION_PROTECTION                          \
	((__wum_kernel_NTSTATUS)0xC000004E)
#define _WUM_KERNEL_STATUS_EAS_NOT_SUPPORTED                           \
	((__wum_kernel_NTSTATUS)0xC000004F)
#define _WUM_KERNEL_STATUS_EA_TOO_LARGE                                \
	((__wum_kernel_NTSTATUS)0xC0000050)
#define _WUM_KERNEL_STATUS_NONEXISTENT_EA_ENTRY                        \
	((__wum_kernel_NTSTATUS)0xC0000051)
#define _WUM_KERNEL_STATUS_NO_EAS_ON_FILE                              \
	((__wum_kernel_NTSTATUS)0xC0000052)
#define _WUM_KERNEL_STATUS_EA_CORRUPT_ERROR                            \
	((__wum_kernel_NTSTATUS)0xC0000053)
#define _WUM_KERNEL_STATUS_FILE_LOCK_CONFLICT                          \
	((__wum_kernel_NTSTATUS)0xC0000054)
#define _WUM_KERNEL_STATUS_LOCK_NOT_GRANTED                            \
	((__wum_kernel_NTSTATUS)0xC0000055)
#define _WUM_KERNEL_STATUS_DELETE_PENDING                              \
	((__wum_kernel_NTSTATUS)0xC0000056)
#define _WUM_KERNEL_STATUS_CTL_FILE_NOT_SUPPORTED                      \
	((__wum_kernel_NTSTATUS)0xC0000057)
#define _WUM_KERNEL_STATUS_UNKNOWN_REVISION                            \
	((__wum_kernel_NTSTATUS)0xC0000058)
#define _WUM_KERNEL_STATUS_REVISION_MISMATCH                           \
	((__wum_kernel_NTSTATUS)0xC0000059)
#define _WUM_KERNEL_STATUS_INVALID_OWNER                               \
	((__wum_kernel_NTSTATUS)0xC000005A)
#define _WUM_KERNEL_STATUS_INVALID_PRIMARY_GROUP                       \
	((__wum_kernel_NTSTATUS)0xC000005B)
#define _WUM_KERNEL_STATUS_NO_IMPERSONATION_TOKEN                      \
	((__wum_kernel_NTSTATUS)0xC000005C)
#define _WUM_KERNEL_STATUS_CANT_DISABLE_MANDATORY                      \
	((__wum_kernel_NTSTATUS)0xC000005D)
#define _WUM_KERNEL_STATUS_NO_LOGON_SERVERS                            \
	((__wum_kernel_NTSTATUS)0xC000005E)
#define _WUM_KERNEL_STATUS_NO_SUCH_LOGON_SESSION                       \
	((__wum_kernel_NTSTATUS)0xC000005F)
#define _WUM_KERNEL_STATUS_NO_SUCH_PRIVILEGE                           \
	((__wum_kernel_NTSTATUS)0xC0000060)
#define _WUM_KERNEL_STATUS_PRIVILEGE_NOT_HELD                          \
	((__wum_kernel_NTSTATUS)0xC0000061)
#define _WUM_KERNEL_STATUS_INVALID_ACCOUNT_NAME                        \
	((__wum_kernel_NTSTATUS)0xC0000062)
#define _WUM_KERNEL_STATUS_USER_EXISTS                                 \
	((__wum_kernel_NTSTATUS)0xC0000063)
#define _WUM_KERNEL_STATUS_NO_SUCH_USER                                \
	((__wum_kernel_NTSTATUS)0xC0000064)
#define _WUM_KERNEL_STATUS_GROUP_EXISTS                                \
	((__wum_kernel_NTSTATUS)0xC0000065)
#define _WUM_KERNEL_STATUS_NO_SUCH_GROUP                               \
	((__wum_kernel_NTSTATUS)0xC0000066)
#define _WUM_KERNEL_STATUS_MEMBER_IN_GROUP                             \
	((__wum_kernel_NTSTATUS)0xC0000067)
#define _WUM_KERNEL_STATUS_MEMBER_NOT_IN_GROUP                         \
	((__wum_kernel_NTSTATUS)0xC0000068)
#define _WUM_KERNEL_STATUS_LAST_ADMIN                                  \
	((__wum_kernel_NTSTATUS)0xC0000069)
#define _WUM_KERNEL_STATUS_WRONG_PASSWORD                              \
	((__wum_kernel_NTSTATUS)0xC000006A)
#define _WUM_KERNEL_STATUS_ILL_FORMED_PASSWORD                         \
	((__wum_kernel_NTSTATUS)0xC000006B)
#define _WUM_KERNEL_STATUS_PASSWORD_RESTRICTION                        \
	((__wum_kernel_NTSTATUS)0xC000006C)
#define _WUM_KERNEL_STATUS_LOGON_FAILURE                               \
	((__wum_kernel_NTSTATUS)0xC000006D)
#define _WUM_KERNEL_STATUS_ACCOUNT_RESTRICTION                         \
	((__wum_kernel_NTSTATUS)0xC000006E)
#define _WUM_KERNEL_STATUS_INVALID_LOGON_HOURS                         \
	((__wum_kernel_NTSTATUS)0xC000006F)
#define _WUM_KERNEL_STATUS_INVALID_WORKSTATION                         \
	((__wum_kernel_NTSTATUS)0xC0000070)
#define _WUM_KERNEL_STATUS_PASSWORD_EXPIRED                            \
	((__wum_kernel_NTSTATUS)0xC0000071)
#define _WUM_KERNEL_STATUS_ACCOUNT_DISABLED                            \
	((__wum_kernel_NTSTATUS)0xC0000072)
#define _WUM_KERNEL_STATUS_NONE_MAPPED                                 \
	((__wum_kernel_NTSTATUS)0xC0000073)
#define _WUM_KERNEL_STATUS_TOO_MANY_LUIDS_REQUESTED                    \
	((__wum_kernel_NTSTATUS)0xC0000074)
#define _WUM_KERNEL_STATUS_LUIDS_EXHAUSTED                             \
	((__wum_kernel_NTSTATUS)0xC0000075)
#define _WUM_KERNEL_STATUS_INVALID_SUB_AUTHORITY                       \
	((__wum_kernel_NTSTATUS)0xC0000076)
#define _WUM_KERNEL_STATUS_INVALID_ACL                                 \
	((__wum_kernel_NTSTATUS)0xC0000077)
#define _WUM_KERNEL_STATUS_INVALID_SID                                 \
	((__wum_kernel_NTSTATUS)0xC0000078)
#define _WUM_KERNEL_STATUS_INVALID_SECURITY_DESCR                      \
	((__wum_kernel_NTSTATUS)0xC0000079)
#define _WUM_KERNEL_STATUS_PROCEDURE_NOT_FOUND                         \
	((__wum_kernel_NTSTATUS)0xC000007A)
#define _WUM_KERNEL_STATUS_INVALID_IMAGE_FORMAT                        \
	((__wum_kernel_NTSTATUS)0xC000007B)
#define _WUM_KERNEL_STATUS_NO_TOKEN ((__wum_kernel_NTSTATUS)0xC000007C)
#define _WUM_KERNEL_STATUS_BAD_INHERITANCE_ACL                         \
	((__wum_kernel_NTSTATUS)0xC000007D)
#define _WUM_KERNEL_STATUS_RANGE_NOT_LOCKED                            \
	((__wum_kernel_NTSTATUS)0xC000007E)
#define _WUM_KERNEL_STATUS_DISK_FULL ((__wum_kernel_NTSTATUS)0xC000007F)
#define _WUM_KERNEL_STATUS_SERVER_DISABLED                             \
	((__wum_kernel_NTSTATUS)0xC0000080)
#define _WUM_KERNEL_STATUS_SERVER_NOT_DISABLED                         \
	((__wum_kernel_NTSTATUS)0xC0000081)
#define _WUM_KERNEL_STATUS_TOO_MANY_GUIDS_REQUESTED                    \
	((__wum_kernel_NTSTATUS)0xC0000082)
#define _WUM_KERNEL_STATUS_GUIDS_EXHAUSTED                             \
	((__wum_kernel_NTSTATUS)0xC0000083)
#define _WUM_KERNEL_STATUS_INVALID_ID_AUTHORITY                        \
	((__wum_kernel_NTSTATUS)0xC0000084)
#define _WUM_KERNEL_STATUS_AGENTS_EXHAUSTED                            \
	((__wum_kernel_NTSTATUS)0xC0000085)
#define _WUM_KERNEL_STATUS_INVALID_VOLUME_LABEL                        \
	((__wum_kernel_NTSTATUS)0xC0000086)
#define _WUM_KERNEL_STATUS_SECTION_NOT_EXTENDED                        \
	((__wum_kernel_NTSTATUS)0xC0000087)
#define _WUM_KERNEL_STATUS_NOT_MAPPED_DATA                             \
	((__wum_kernel_NTSTATUS)0xC0000088)
#define _WUM_KERNEL_STATUS_RESOURCE_DATA_NOT_FOUND                     \
	((__wum_kernel_NTSTATUS)0xC0000089)
#define _WUM_KERNEL_STATUS_RESOURCE_TYPE_NOT_FOUND                     \
	((__wum_kernel_NTSTATUS)0xC000008A)
#define _WUM_KERNEL_STATUS_RESOURCE_NAME_NOT_FOUND                     \
	((__wum_kernel_NTSTATUS)0xC000008B)
#define _WUM_KERNEL_STATUS_ARRAY_BOUNDS_EXCEEDED                       \
	((__wum_kernel_NTSTATUS)0xC000008C)
#define _WUM_KERNEL_STATUS_FLOAT_DENORMAL_OPERAND                      \
	((__wum_kernel_NTSTATUS)0xC000008D)
#define _WUM_KERNEL_STATUS_FLOAT_DIVIDE_BY_ZERO                        \
	((__wum_kernel_NTSTATUS)0xC000008E)
#define _WUM_KERNEL_STATUS_FLOAT_INEXACT_RESULT                        \
	((__wum_kernel_NTSTATUS)0xC000008F)
#define _WUM_KERNEL_STATUS_FLOAT_INVALID_OPERATION                     \
	((__wum_kernel_NTSTATUS)0xC0000090)
#define _WUM_KERNEL_STATUS_FLOAT_OVERFLOW                              \
	((__wum_kernel_NTSTATUS)0xC0000091)
#define _WUM_KERNEL_STATUS_FLOAT_STACK_CHECK                           \
	((__wum_kernel_NTSTATUS)0xC0000092)
#define _WUM_KERNEL_STATUS_FLOAT_UNDERFLOW                             \
	((__wum_kernel_NTSTATUS)0xC0000093)
#define _WUM_KERNEL_STATUS_INTEGER_DIVIDE_BY_ZERO                      \
	((__wum_kernel_NTSTATUS)0xC0000094)
#define _WUM_KERNEL_STATUS_INTEGER_OVERFLOW                            \
	((__wum_kernel_NTSTATUS)0xC0000095)
#define _WUM_KERNEL_STATUS_PRIVILEGED_INSTRUCTION                      \
	((__wum_kernel_NTSTATUS)0xC0000096)
#define _WUM_KERNEL_STATUS_TOO_MANY_PAGING_FILES                       \
	((__wum_kernel_NTSTATUS)0xC0000097)
#define _WUM_KERNEL_STATUS_FILE_INVALID                                \
	((__wum_kernel_NTSTATUS)0xC0000098)
#define _WUM_KERNEL_STATUS_ALLOTTED_SPACE_EXCEEDED                     \
	((__wum_kernel_NTSTATUS)0xC0000099)
#define _WUM_KERNEL_STATUS_INSUFFICIENT_RESOURCES                      \
	((__wum_kernel_NTSTATUS)0xC000009A)
#define _WUM_KERNEL_STATUS_DFS_EXIT_PATH_FOUND                         \
	((__wum_kernel_NTSTATUS)0xC000009B)
#define _WUM_KERNEL_STATUS_DEVICE_DATA_ERROR                           \
	((__wum_kernel_NTSTATUS)0xC000009C)
#define _WUM_KERNEL_STATUS_DEVICE_NOT_CONNECTED                        \
	((__wum_kernel_NTSTATUS)0xC000009D)
#define _WUM_KERNEL_STATUS_FREE_VM_NOT_AT_BASE                         \
	((__wum_kernel_NTSTATUS)0xC000009F)
#define _WUM_KERNEL_STATUS_MEMORY_NOT_ALLOCATED                        \
	((__wum_kernel_NTSTATUS)0xC00000A0)
#define _WUM_KERNEL_STATUS_WORKING_SET_QUOTA                           \
	((__wum_kernel_NTSTATUS)0xC00000A1)
#define _WUM_KERNEL_STATUS_MEDIA_WRITE_PROTECTED                       \
	((__wum_kernel_NTSTATUS)0xC00000A2)
#define _WUM_KERNEL_STATUS_DEVICE_NOT_READY                            \
	((__wum_kernel_NTSTATUS)0xC00000A3)
#define _WUM_KERNEL_STATUS_INVALID_GROUP_ATTRIBUTES                    \
	((__wum_kernel_NTSTATUS)0xC00000A4)
#define _WUM_KERNEL_STATUS_BAD_IMPERSONATION_LEVEL                     \
	((__wum_kernel_NTSTATUS)0xC00000A5)
#define _WUM_KERNEL_STATUS_CANT_OPEN_ANONYMOUS                         \
	((__wum_kernel_NTSTATUS)0xC00000A6)
#define _WUM_KERNEL_STATUS_BAD_VALIDATION_CLASS                        \
	((__wum_kernel_NTSTATUS)0xC00000A7)
#define _WUM_KERNEL_STATUS_BAD_TOKEN_TYPE                              \
	((__wum_kernel_NTSTATUS)0xC00000A8)
#define _WUM_KERNEL_STATUS_BAD_MASTER_BOOT_RECORD                      \
	((__wum_kernel_NTSTATUS)0xC00000A9)
#define _WUM_KERNEL_STATUS_INSTRUCTION_MISALIGNMENT                    \
	((__wum_kernel_NTSTATUS)0xC00000AA)
#define _WUM_KERNEL_STATUS_INSTANCE_NOT_AVAILABLE                      \
	((__wum_kernel_NTSTATUS)0xC00000AB)
#define _WUM_KERNEL_STATUS_PIPE_NOT_AVAILABLE                          \
	((__wum_kernel_NTSTATUS)0xC00000AC)
#define _WUM_KERNEL_STATUS_INVALID_PIPE_STATE                          \
	((__wum_kernel_NTSTATUS)0xC00000AD)
#define _WUM_KERNEL_STATUS_PIPE_BUSY ((__wum_kernel_NTSTATUS)0xC00000AE)
#define _WUM_KERNEL_STATUS_ILLEGAL_FUNCTION                            \
	((__wum_kernel_NTSTATUS)0xC00000AF)
#define _WUM_KERNEL_STATUS_PIPE_DISCONNECTED                           \
	((__wum_kernel_NTSTATUS)0xC00000B0)
#define _WUM_KERNEL_STATUS_PIPE_CLOSING                                \
	((__wum_kernel_NTSTATUS)0xC00000B1)
#define _WUM_KERNEL_STATUS_PIPE_CONNECTED                              \
	((__wum_kernel_NTSTATUS)0xC00000B2)
#define _WUM_KERNEL_STATUS_PIPE_LISTENING                              \
	((__wum_kernel_NTSTATUS)0xC00000B3)
#define _WUM_KERNEL_STATUS_INVALID_READ_MODE                           \
	((__wum_kernel_NTSTATUS)0xC00000B4)
#define _WUM_KERNEL_STATUS_IO_TIMEOUT                                  \
	((__wum_kernel_NTSTATUS)0xC00000B5)
#define _WUM_KERNEL_STATUS_FILE_FORCED_CLOSED                          \
	((__wum_kernel_NTSTATUS)0xC00000B6)
#define _WUM_KERNEL_STATUS_PROFILING_NOT_STARTED                       \
	((__wum_kernel_NTSTATUS)0xC00000B7)
#define _WUM_KERNEL_STATUS_PROFILING_NOT_STOPPED                       \
	((__wum_kernel_NTSTATUS)0xC00000B8)
#define _WUM_KERNEL_STATUS_COULD_NOT_INTERPRET                         \
	((__wum_kernel_NTSTATUS)0xC00000B9)
#define _WUM_KERNEL_STATUS_FILE_IS_A_DIRECTORY                         \
	((__wum_kernel_NTSTATUS)0xC00000BA)
#define _WUM_KERNEL_STATUS_NOT_SUPPORTED                               \
	((__wum_kernel_NTSTATUS)0xC00000BB)
#define _WUM_KERNEL_STATUS_REMOTE_NOT_LISTENING                        \
	((__wum_kernel_NTSTATUS)0xC00000BC)
#define _WUM_KERNEL_STATUS_DUPLICATE_NAME                              \
	((__wum_kernel_NTSTATUS)0xC00000BD)
#define _WUM_KERNEL_STATUS_BAD_NETWORK_PATH                            \
	((__wum_kernel_NTSTATUS)0xC00000BE)
#define _WUM_KERNEL_STATUS_NETWORK_BUSY                                \
	((__wum_kernel_NTSTATUS)0xC00000BF)
#define _WUM_KERNEL_STATUS_DEVICE_DOES_NOT_EXIST                       \
	((__wum_kernel_NTSTATUS)0xC00000C0)
#define _WUM_KERNEL_STATUS_TOO_MANY_COMMANDS                           \
	((__wum_kernel_NTSTATUS)0xC00000C1)
#define _WUM_KERNEL_STATUS_ADAPTER_HARDWARE_ERROR                      \
	((__wum_kernel_NTSTATUS)0xC00000C2)
#define _WUM_KERNEL_STATUS_INVALID_NETWORK_RESPONSE                    \
	((__wum_kernel_NTSTATUS)0xC00000C3)
#define _WUM_KERNEL_STATUS_UNEXPECTED_NETWORK_ERROR                    \
	((__wum_kernel_NTSTATUS)0xC00000C4)
#define _WUM_KERNEL_STATUS_BAD_REMOTE_ADAPTER                          \
	((__wum_kernel_NTSTATUS)0xC00000C5)
#define _WUM_KERNEL_STATUS_PRINT_QUEUE_FULL                            \
	((__wum_kernel_NTSTATUS)0xC00000C6)
#define _WUM_KERNEL_STATUS_NO_SPOOL_SPACE                              \
	((__wum_kernel_NTSTATUS)0xC00000C7)
#define _WUM_KERNEL_STATUS_PRINT_CANCELLED                             \
	((__wum_kernel_NTSTATUS)0xC00000C8)
#define _WUM_KERNEL_STATUS_NETWORK_NAME_DELETED                        \
	((__wum_kernel_NTSTATUS)0xC00000C9)
#define _WUM_KERNEL_STATUS_NETWORK_ACCESS_DENIED                       \
	((__wum_kernel_NTSTATUS)0xC00000CA)
#define _WUM_KERNEL_STATUS_BAD_DEVICE_TYPE                             \
	((__wum_kernel_NTSTATUS)0xC00000CB)
#define _WUM_KERNEL_STATUS_BAD_NETWORK_NAME                            \
	((__wum_kernel_NTSTATUS)0xC00000CC)
#define _WUM_KERNEL_STATUS_TOO_MANY_NAMES                              \
	((__wum_kernel_NTSTATUS)0xC00000CD)
#define _WUM_KERNEL_STATUS_TOO_MANY_SESSIONS                           \
	((__wum_kernel_NTSTATUS)0xC00000CE)
#define _WUM_KERNEL_STATUS_SHARING_PAUSED                              \
	((__wum_kernel_NTSTATUS)0xC00000CF)
#define _WUM_KERNEL_STATUS_REQUEST_NOT_ACCEPTED                        \
	((__wum_kernel_NTSTATUS)0xC00000D0)
#define _WUM_KERNEL_STATUS_REDIRECTOR_PAUSED                           \
	((__wum_kernel_NTSTATUS)0xC00000D1)
#define _WUM_KERNEL_STATUS_NET_WRITE_FAULT                             \
	((__wum_kernel_NTSTATUS)0xC00000D2)
#define _WUM_KERNEL_STATUS_PROFILING_AT_LIMIT                          \
	((__wum_kernel_NTSTATUS)0xC00000D3)
#define _WUM_KERNEL_STATUS_NOT_SAME_DEVICE                             \
	((__wum_kernel_NTSTATUS)0xC00000D4)
#define _WUM_KERNEL_STATUS_FILE_RENAMED                                \
	((__wum_kernel_NTSTATUS)0xC00000D5)
#define _WUM_KERNEL_STATUS_VIRTUAL_CIRCUIT_CLOSED                      \
	((__wum_kernel_NTSTATUS)0xC00000D6)
#define _WUM_KERNEL_STATUS_NO_SECURITY_ON_OBJECT                       \
	((__wum_kernel_NTSTATUS)0xC00000D7)
#define _WUM_KERNEL_STATUS_CANT_WAIT ((__wum_kernel_NTSTATUS)0xC00000D8)
#define _WUM_KERNEL_STATUS_PIPE_EMPTY                                  \
	((__wum_kernel_NTSTATUS)0xC00000D9)
#define _WUM_KERNEL_STATUS_CANT_ACCESS_DOMAIN_INFO                     \
	((__wum_kernel_NTSTATUS)0xC00000DA)
#define _WUM_KERNEL_STATUS_CANT_TERMINATE_SELF                         \
	((__wum_kernel_NTSTATUS)0xC00000DB)
#define _WUM_KERNEL_STATUS_INVALID_SERVER_STATE                        \
	((__wum_kernel_NTSTATUS)0xC00000DC)
#define _WUM_KERNEL_STATUS_INVALID_DOMAIN_STATE                        \
	((__wum_kernel_NTSTATUS)0xC00000DD)
#define _WUM_KERNEL_STATUS_INVALID_DOMAIN_ROLE                         \
	((__wum_kernel_NTSTATUS)0xC00000DE)
#define _WUM_KERNEL_STATUS_NO_SUCH_DOMAIN                              \
	((__wum_kernel_NTSTATUS)0xC00000DF)
#define _WUM_KERNEL_STATUS_DOMAIN_EXISTS                               \
	((__wum_kernel_NTSTATUS)0xC00000E0)
#define _WUM_KERNEL_STATUS_DOMAIN_LIMIT_EXCEEDED                       \
	((__wum_kernel_NTSTATUS)0xC00000E1)
#define _WUM_KERNEL_STATUS_OPLOCK_NOT_GRANTED                          \
	((__wum_kernel_NTSTATUS)0xC00000E2)
#define _WUM_KERNEL_STATUS_INVALID_OPLOCK_PROTOCOL                     \
	((__wum_kernel_NTSTATUS)0xC00000E3)
#define _WUM_KERNEL_STATUS_INTERNAL_DB_CORRUPTION                      \
	((__wum_kernel_NTSTATUS)0xC00000E4)
#define _WUM_KERNEL_STATUS_INTERNAL_ERROR                              \
	((__wum_kernel_NTSTATUS)0xC00000E5)
#define _WUM_KERNEL_STATUS_GENERIC_NOT_MAPPED                          \
	((__wum_kernel_NTSTATUS)0xC00000E6)
#define _WUM_KERNEL_STATUS_BAD_DESCRIPTOR_FORMAT                       \
	((__wum_kernel_NTSTATUS)0xC00000E7)
#define _WUM_KERNEL_STATUS_INVALID_USER_BUFFER                         \
	((__wum_kernel_NTSTATUS)0xC00000E8)
#define _WUM_KERNEL_STATUS_UNEXPECTED_IO_ERROR                         \
	((__wum_kernel_NTSTATUS)0xC00000E9)
#define _WUM_KERNEL_STATUS_UNEXPECTED_MM_CREATE_ERR                    \
	((__wum_kernel_NTSTATUS)0xC00000EA)
#define _WUM_KERNEL_STATUS_UNEXPECTED_MM_MAP_ERROR                     \
	((__wum_kernel_NTSTATUS)0xC00000EB)
#define _WUM_KERNEL_STATUS_UNEXPECTED_MM_EXTEND_ERR                    \
	((__wum_kernel_NTSTATUS)0xC00000EC)
#define _WUM_KERNEL_STATUS_NOT_LOGON_PROCESS                           \
	((__wum_kernel_NTSTATUS)0xC00000ED)
#define _WUM_KERNEL_STATUS_LOGON_SESSION_EXISTS                        \
	((__wum_kernel_NTSTATUS)0xC00000EE)
#define _WUM_KERNEL_STATUS_INVALID_PARAMETER_1                         \
	((__wum_kernel_NTSTATUS)0xC00000EF)
#define _WUM_KERNEL_STATUS_INVALID_PARAMETER_2                         \
	((__wum_kernel_NTSTATUS)0xC00000F0)
#define _WUM_KERNEL_STATUS_INVALID_PARAMETER_3                         \
	((__wum_kernel_NTSTATUS)0xC00000F1)
#define _WUM_KERNEL_STATUS_INVALID_PARAMETER_4                         \
	((__wum_kernel_NTSTATUS)0xC00000F2)
#define _WUM_KERNEL_STATUS_INVALID_PARAMETER_5                         \
	((__wum_kernel_NTSTATUS)0xC00000F3)
#define _WUM_KERNEL_STATUS_INVALID_PARAMETER_6                         \
	((__wum_kernel_NTSTATUS)0xC00000F4)
#define _WUM_KERNEL_STATUS_INVALID_PARAMETER_7                         \
	((__wum_kernel_NTSTATUS)0xC00000F5)
#define _WUM_KERNEL_STATUS_INVALID_PARAMETER_8                         \
	((__wum_kernel_NTSTATUS)0xC00000F6)
#define _WUM_KERNEL_STATUS_INVALID_PARAMETER_9                         \
	((__wum_kernel_NTSTATUS)0xC00000F7)
#define _WUM_KERNEL_STATUS_INVALID_PARAMETER_10                        \
	((__wum_kernel_NTSTATUS)0xC00000F8)
#define _WUM_KERNEL_STATUS_INVALID_PARAMETER_11                        \
	((__wum_kernel_NTSTATUS)0xC00000F9)
#define _WUM_KERNEL_STATUS_INVALID_PARAMETER_12                        \
	((__wum_kernel_NTSTATUS)0xC00000FA)
#define _WUM_KERNEL_STATUS_REDIRECTOR_NOT_STARTED                      \
	((__wum_kernel_NTSTATUS)0xC00000FB)
#define _WUM_KERNEL_STATUS_REDIRECTOR_STARTED                          \
	((__wum_kernel_NTSTATUS)0xC00000FC)
#define _WUM_KERNEL_STATUS_STACK_OVERFLOW                              \
	((__wum_kernel_NTSTATUS)0xC00000FD)
#define _WUM_KERNEL_STATUS_NO_SUCH_PACKAGE                             \
	((__wum_kernel_NTSTATUS)0xC00000FE)
#define _WUM_KERNEL_STATUS_BAD_FUNCTION_TABLE                          \
	((__wum_kernel_NTSTATUS)0xC00000FF)
#define _WUM_KERNEL_STATUS_VARIABLE_NOT_FOUND                          \
	((__wum_kernel_NTSTATUS)0xC0000100)
#define _WUM_KERNEL_STATUS_DIRECTORY_NOT_EMPTY                         \
	((__wum_kernel_NTSTATUS)0xC0000101)
#define _WUM_KERNEL_STATUS_FILE_CORRUPT_ERROR                          \
	((__wum_kernel_NTSTATUS)0xC0000102)
#define _WUM_KERNEL_STATUS_NOT_A_DIRECTORY                             \
	((__wum_kernel_NTSTATUS)0xC0000103)
#define _WUM_KERNEL_STATUS_BAD_LOGON_SESSION_STATE                     \
	((__wum_kernel_NTSTATUS)0xC0000104)
#define _WUM_KERNEL_STATUS_LOGON_SESSION_COLLISION                     \
	((__wum_kernel_NTSTATUS)0xC0000105)
#define _WUM_KERNEL_STATUS_NAME_TOO_LONG                               \
	((__wum_kernel_NTSTATUS)0xC0000106)
#define _WUM_KERNEL_STATUS_FILES_OPEN                                  \
	((__wum_kernel_NTSTATUS)0xC0000107)
#define _WUM_KERNEL_STATUS_CONNECTION_IN_USE                           \
	((__wum_kernel_NTSTATUS)0xC0000108)
#define _WUM_KERNEL_STATUS_MESSAGE_NOT_FOUND                           \
	((__wum_kernel_NTSTATUS)0xC0000109)
#define _WUM_KERNEL_STATUS_PROCESS_IS_TERMINATING                      \
	((__wum_kernel_NTSTATUS)0xC000010A)
#define _WUM_KERNEL_STATUS_INVALID_LOGON_TYPE                          \
	((__wum_kernel_NTSTATUS)0xC000010B)
#define _WUM_KERNEL_STATUS_NO_GUID_TRANSLATION                         \
	((__wum_kernel_NTSTATUS)0xC000010C)
#define _WUM_KERNEL_STATUS_CANNOT_IMPERSONATE                          \
	((__wum_kernel_NTSTATUS)0xC000010D)
#define _WUM_KERNEL_STATUS_IMAGE_ALREADY_LOADED                        \
	((__wum_kernel_NTSTATUS)0xC000010E)
#define _WUM_KERNEL_STATUS_NO_LDT ((__wum_kernel_NTSTATUS)0xC0000117)
#define _WUM_KERNEL_STATUS_INVALID_LDT_SIZE                            \
	((__wum_kernel_NTSTATUS)0xC0000118)
#define _WUM_KERNEL_STATUS_INVALID_LDT_OFFSET                          \
	((__wum_kernel_NTSTATUS)0xC0000119)
#define _WUM_KERNEL_STATUS_INVALID_LDT_DESCRIPTOR                      \
	((__wum_kernel_NTSTATUS)0xC000011A)
#define _WUM_KERNEL_STATUS_INVALID_IMAGE_NE_FORMAT                     \
	((__wum_kernel_NTSTATUS)0xC000011B)
#define _WUM_KERNEL_STATUS_RXACT_INVALID_STATE                         \
	((__wum_kernel_NTSTATUS)0xC000011C)
#define _WUM_KERNEL_STATUS_RXACT_COMMIT_FAILURE                        \
	((__wum_kernel_NTSTATUS)0xC000011D)
#define _WUM_KERNEL_STATUS_MAPPED_FILE_SIZE_ZERO                       \
	((__wum_kernel_NTSTATUS)0xC000011E)
#define _WUM_KERNEL_STATUS_TOO_MANY_OPENED_FILES                       \
	((__wum_kernel_NTSTATUS)0xC000011F)
#define _WUM_KERNEL_STATUS_CANCELLED ((__wum_kernel_NTSTATUS)0xC0000120)
#define _WUM_KERNEL_STATUS_CANNOT_DELETE                               \
	((__wum_kernel_NTSTATUS)0xC0000121)
#define _WUM_KERNEL_STATUS_INVALID_COMPUTER_NAME                       \
	((__wum_kernel_NTSTATUS)0xC0000122)
#define _WUM_KERNEL_STATUS_FILE_DELETED                                \
	((__wum_kernel_NTSTATUS)0xC0000123)
#define _WUM_KERNEL_STATUS_SPECIAL_ACCOUNT                             \
	((__wum_kernel_NTSTATUS)0xC0000124)
#define _WUM_KERNEL_STATUS_SPECIAL_GROUP                               \
	((__wum_kernel_NTSTATUS)0xC0000125)
#define _WUM_KERNEL_STATUS_SPECIAL_USER                                \
	((__wum_kernel_NTSTATUS)0xC0000126)
#define _WUM_KERNEL_STATUS_MEMBERS_PRIMARY_GROUP                       \
	((__wum_kernel_NTSTATUS)0xC0000127)
#define _WUM_KERNEL_STATUS_FILE_CLOSED                                 \
	((__wum_kernel_NTSTATUS)0xC0000128)
#define _WUM_KERNEL_STATUS_TOO_MANY_THREADS                            \
	((__wum_kernel_NTSTATUS)0xC0000129)
#define _WUM_KERNEL_STATUS_THREAD_NOT_IN_PROCESS                       \
	((__wum_kernel_NTSTATUS)0xC000012A)
#define _WUM_KERNEL_STATUS_TOKEN_ALREADY_IN_USE                        \
	((__wum_kernel_NTSTATUS)0xC000012B)
#define _WUM_KERNEL_STATUS_PAGEFILE_QUOTA_EXCEEDED                     \
	((__wum_kernel_NTSTATUS)0xC000012C)
#define _WUM_KERNEL_STATUS_COMMITMENT_LIMIT                            \
	((__wum_kernel_NTSTATUS)0xC000012D)
#define _WUM_KERNEL_STATUS_INVALID_IMAGE_LE_FORMAT                     \
	((__wum_kernel_NTSTATUS)0xC000012E)
#define _WUM_KERNEL_STATUS_INVALID_IMAGE_NOT_MZ                        \
	((__wum_kernel_NTSTATUS)0xC000012F)
#define _WUM_KERNEL_STATUS_INVALID_IMAGE_PROTECT                       \
	((__wum_kernel_NTSTATUS)0xC0000130)
#define _WUM_KERNEL_STATUS_INVALID_IMAGE_WIN_16                        \
	((__wum_kernel_NTSTATUS)0xC0000131)
#define _WUM_KERNEL_STATUS_LOGON_SERVER_CONFLICT                       \
	((__wum_kernel_NTSTATUS)0xC0000132)
#define _WUM_KERNEL_STATUS_TIME_DIFFERENCE_AT_DC                       \
	((__wum_kernel_NTSTATUS)0xC0000133)
#define _WUM_KERNEL_STATUS_SYNCHRONIZATION_REQUIRED                    \
	((__wum_kernel_NTSTATUS)0xC0000134)
#define _WUM_KERNEL_STATUS_DLL_NOT_FOUND                               \
	((__wum_kernel_NTSTATUS)0xC0000135)
#define _WUM_KERNEL_STATUS_OPEN_FAILED                                 \
	((__wum_kernel_NTSTATUS)0xC0000136)
#define _WUM_KERNEL_STATUS_IO_PRIVILEGE_FAILED                         \
	((__wum_kernel_NTSTATUS)0xC0000137)
#define _WUM_KERNEL_STATUS_ORDINAL_NOT_FOUND                           \
	((__wum_kernel_NTSTATUS)0xC0000138)
#define _WUM_KERNEL_STATUS_ENTRYPOINT_NOT_FOUND                        \
	((__wum_kernel_NTSTATUS)0xC0000139)
#define _WUM_KERNEL_STATUS_CONTROL_C_EXIT                              \
	((__wum_kernel_NTSTATUS)0xC000013A)
#define _WUM_KERNEL_STATUS_LOCAL_DISCONNECT                            \
	((__wum_kernel_NTSTATUS)0xC000013B)
#define _WUM_KERNEL_STATUS_REMOTE_DISCONNECT                           \
	((__wum_kernel_NTSTATUS)0xC000013C)
#define _WUM_KERNEL_STATUS_REMOTE_RESOURCES                            \
	((__wum_kernel_NTSTATUS)0xC000013D)
#define _WUM_KERNEL_STATUS_LINK_FAILED                                 \
	((__wum_kernel_NTSTATUS)0xC000013E)
#define _WUM_KERNEL_STATUS_LINK_TIMEOUT                                \
	((__wum_kernel_NTSTATUS)0xC000013F)
#define _WUM_KERNEL_STATUS_INVALID_CONNECTION                          \
	((__wum_kernel_NTSTATUS)0xC0000140)
#define _WUM_KERNEL_STATUS_INVALID_ADDRESS                             \
	((__wum_kernel_NTSTATUS)0xC0000141)
#define _WUM_KERNEL_STATUS_DLL_INIT_FAILED                             \
	((__wum_kernel_NTSTATUS)0xC0000142)
#define _WUM_KERNEL_STATUS_MISSING_SYSTEMFILE                          \
	((__wum_kernel_NTSTATUS)0xC0000143)
#define _WUM_KERNEL_STATUS_UNHANDLED_EXCEPTION                         \
	((__wum_kernel_NTSTATUS)0xC0000144)
#define _WUM_KERNEL_STATUS_APP_INIT_FAILURE                            \
	((__wum_kernel_NTSTATUS)0xC0000145)
#define _WUM_KERNEL_STATUS_PAGEFILE_CREATE_FAILED                      \
	((__wum_kernel_NTSTATUS)0xC0000146)
#define _WUM_KERNEL_STATUS_NO_PAGEFILE                                 \
	((__wum_kernel_NTSTATUS)0xC0000147)
#define _WUM_KERNEL_STATUS_INVALID_LEVEL                               \
	((__wum_kernel_NTSTATUS)0xC0000148)
#define _WUM_KERNEL_STATUS_WRONG_PASSWORD_CORE                         \
	((__wum_kernel_NTSTATUS)0xC0000149)
#define _WUM_KERNEL_STATUS_ILLEGAL_FLOAT_CONTEXT                       \
	((__wum_kernel_NTSTATUS)0xC000014A)
#define _WUM_KERNEL_STATUS_PIPE_BROKEN                                 \
	((__wum_kernel_NTSTATUS)0xC000014B)
#define _WUM_KERNEL_STATUS_REGISTRY_CORRUPT                            \
	((__wum_kernel_NTSTATUS)0xC000014C)
#define _WUM_KERNEL_STATUS_REGISTRY_IO_FAILED                          \
	((__wum_kernel_NTSTATUS)0xC000014D)
#define _WUM_KERNEL_STATUS_NO_EVENT_PAIR                               \
	((__wum_kernel_NTSTATUS)0xC000014E)
#define _WUM_KERNEL_STATUS_UNRECOGNIZED_VOLUME                         \
	((__wum_kernel_NTSTATUS)0xC000014F)
#define _WUM_KERNEL_STATUS_SERIAL_NO_DEVICE_INITED                     \
	((__wum_kernel_NTSTATUS)0xC0000150)
#define _WUM_KERNEL_STATUS_NO_SUCH_ALIAS                               \
	((__wum_kernel_NTSTATUS)0xC0000151)
#define _WUM_KERNEL_STATUS_MEMBER_NOT_IN_ALIAS                         \
	((__wum_kernel_NTSTATUS)0xC0000152)
#define _WUM_KERNEL_STATUS_MEMBER_IN_ALIAS                             \
	((__wum_kernel_NTSTATUS)0xC0000153)
#define _WUM_KERNEL_STATUS_ALIAS_EXISTS                                \
	((__wum_kernel_NTSTATUS)0xC0000154)
#define _WUM_KERNEL_STATUS_LOGON_NOT_GRANTED                           \
	((__wum_kernel_NTSTATUS)0xC0000155)
#define _WUM_KERNEL_STATUS_TOO_MANY_SECRETS                            \
	((__wum_kernel_NTSTATUS)0xC0000156)
#define _WUM_KERNEL_STATUS_SECRET_TOO_LONG                             \
	((__wum_kernel_NTSTATUS)0xC0000157)
#define _WUM_KERNEL_STATUS_INTERNAL_DB_ERROR                           \
	((__wum_kernel_NTSTATUS)0xC0000158)
#define _WUM_KERNEL_STATUS_FULLSCREEN_MODE                             \
	((__wum_kernel_NTSTATUS)0xC0000159)
#define _WUM_KERNEL_STATUS_TOO_MANY_CONTEXT_IDS                        \
	((__wum_kernel_NTSTATUS)0xC000015A)
#define _WUM_KERNEL_STATUS_LOGON_TYPE_NOT_GRANTED                      \
	((__wum_kernel_NTSTATUS)0xC000015B)
#define _WUM_KERNEL_STATUS_NOT_REGISTRY_FILE                           \
	((__wum_kernel_NTSTATUS)0xC000015C)
#define _WUM_KERNEL_STATUS_NT_CROSS_ENCRYPTION_REQUIRED                \
	((__wum_kernel_NTSTATUS)0xC000015D)
#define _WUM_KERNEL_STATUS_DOMAIN_CTRLR_CONFIG_ERROR                   \
	((__wum_kernel_NTSTATUS)0xC000015E)
#define _WUM_KERNEL_STATUS_FT_MISSING_MEMBER                           \
	((__wum_kernel_NTSTATUS)0xC000015F)
#define _WUM_KERNEL_STATUS_ILL_FORMED_SERVICE_ENTRY                    \
	((__wum_kernel_NTSTATUS)0xC0000160)
#define _WUM_KERNEL_STATUS_ILLEGAL_CHARACTER                           \
	((__wum_kernel_NTSTATUS)0xC0000161)
#define _WUM_KERNEL_STATUS_UNMAPPABLE_CHARACTER                        \
	((__wum_kernel_NTSTATUS)0xC0000162)
#define _WUM_KERNEL_STATUS_UNDEFINED_CHARACTER                         \
	((__wum_kernel_NTSTATUS)0xC0000163)
#define _WUM_KERNEL_STATUS_FLOPPY_VOLUME                               \
	((__wum_kernel_NTSTATUS)0xC0000164)
#define _WUM_KERNEL_STATUS_FLOPPY_ID_MARK_NOT_FOUND                    \
	((__wum_kernel_NTSTATUS)0xC0000165)
#define _WUM_KERNEL_STATUS_FLOPPY_WRONG_CYLINDER                       \
	((__wum_kernel_NTSTATUS)0xC0000166)
#define _WUM_KERNEL_STATUS_FLOPPY_UNKNOWN_ERROR                        \
	((__wum_kernel_NTSTATUS)0xC0000167)
#define _WUM_KERNEL_STATUS_FLOPPY_BAD_REGISTERS                        \
	((__wum_kernel_NTSTATUS)0xC0000168)
#define _WUM_KERNEL_STATUS_DISK_RECALIBRATE_FAILED                     \
	((__wum_kernel_NTSTATUS)0xC0000169)
#define _WUM_KERNEL_STATUS_DISK_OPERATION_FAILED                       \
	((__wum_kernel_NTSTATUS)0xC000016A)
#define _WUM_KERNEL_STATUS_DISK_RESET_FAILED                           \
	((__wum_kernel_NTSTATUS)0xC000016B)
#define _WUM_KERNEL_STATUS_SHARED_IRQ_BUSY                             \
	((__wum_kernel_NTSTATUS)0xC000016C)
#define _WUM_KERNEL_STATUS_FT_ORPHANING                                \
	((__wum_kernel_NTSTATUS)0xC000016D)
#define _WUM_KERNEL_STATUS_BIOS_FAILED_TO_CONNECT_INTERRUPT            \
	((__wum_kernel_NTSTATUS)0xC000016E)
#define _WUM_KERNEL_STATUS_PARTITION_FAILURE                           \
	((__wum_kernel_NTSTATUS)0xC0000172)
#define _WUM_KERNEL_STATUS_INVALID_BLOCK_LENGTH                        \
	((__wum_kernel_NTSTATUS)0xC0000173)
#define _WUM_KERNEL_STATUS_DEVICE_NOT_PARTITIONED                      \
	((__wum_kernel_NTSTATUS)0xC0000174)
#define _WUM_KERNEL_STATUS_UNABLE_TO_LOCK_MEDIA                        \
	((__wum_kernel_NTSTATUS)0xC0000175)
#define _WUM_KERNEL_STATUS_UNABLE_TO_UNLOAD_MEDIA                      \
	((__wum_kernel_NTSTATUS)0xC0000176)
#define _WUM_KERNEL_STATUS_EOM_OVERFLOW                                \
	((__wum_kernel_NTSTATUS)0xC0000177)
#define _WUM_KERNEL_STATUS_NO_MEDIA ((__wum_kernel_NTSTATUS)0xC0000178)
#define _WUM_KERNEL_STATUS_NO_SUCH_MEMBER                              \
	((__wum_kernel_NTSTATUS)0xC000017A)
#define _WUM_KERNEL_STATUS_INVALID_MEMBER                              \
	((__wum_kernel_NTSTATUS)0xC000017B)
#define _WUM_KERNEL_STATUS_KEY_DELETED                                 \
	((__wum_kernel_NTSTATUS)0xC000017C)
#define _WUM_KERNEL_STATUS_NO_LOG_SPACE                                \
	((__wum_kernel_NTSTATUS)0xC000017D)
#define _WUM_KERNEL_STATUS_TOO_MANY_SIDS                               \
	((__wum_kernel_NTSTATUS)0xC000017E)
#define _WUM_KERNEL_STATUS_LM_CROSS_ENCRYPTION_REQUIRED                \
	((__wum_kernel_NTSTATUS)0xC000017F)
#define _WUM_KERNEL_STATUS_KEY_HAS_CHILDREN                            \
	((__wum_kernel_NTSTATUS)0xC0000180)
#define _WUM_KERNEL_STATUS_CHILD_MUST_BE_VOLATILE                      \
	((__wum_kernel_NTSTATUS)0xC0000181)
#define _WUM_KERNEL_STATUS_DEVICE_CONFIGURATION_ERROR                  \
	((__wum_kernel_NTSTATUS)0xC0000182)
#define _WUM_KERNEL_STATUS_DRIVER_INTERNAL_ERROR                       \
	((__wum_kernel_NTSTATUS)0xC0000183)
#define _WUM_KERNEL_STATUS_INVALID_DEVICE_STATE                        \
	((__wum_kernel_NTSTATUS)0xC0000184)
#define _WUM_KERNEL_STATUS_IO_DEVICE_ERROR                             \
	((__wum_kernel_NTSTATUS)0xC0000185)
#define _WUM_KERNEL_STATUS_DEVICE_PROTOCOL_ERROR                       \
	((__wum_kernel_NTSTATUS)0xC0000186)
#define _WUM_KERNEL_STATUS_BACKUP_CONTROLLER                           \
	((__wum_kernel_NTSTATUS)0xC0000187)
#define _WUM_KERNEL_STATUS_LOG_FILE_FULL                               \
	((__wum_kernel_NTSTATUS)0xC0000188)
#define _WUM_KERNEL_STATUS_TOO_LATE ((__wum_kernel_NTSTATUS)0xC0000189)
#define _WUM_KERNEL_STATUS_NO_TRUST_LSA_SECRET                         \
	((__wum_kernel_NTSTATUS)0xC000018A)
#define _WUM_KERNEL_STATUS_NO_TRUST_SAM_ACCOUNT                        \
	((__wum_kernel_NTSTATUS)0xC000018B)
#define _WUM_KERNEL_STATUS_TRUSTED_DOMAIN_FAILURE                      \
	((__wum_kernel_NTSTATUS)0xC000018C)
#define _WUM_KERNEL_STATUS_TRUSTED_RELATIONSHIP_FAILURE                \
	((__wum_kernel_NTSTATUS)0xC000018D)
#define _WUM_KERNEL_STATUS_EVENTLOG_FILE_CORRUPT                       \
	((__wum_kernel_NTSTATUS)0xC000018E)
#define _WUM_KERNEL_STATUS_EVENTLOG_CANT_START                         \
	((__wum_kernel_NTSTATUS)0xC000018F)
#define _WUM_KERNEL_STATUS_TRUST_FAILURE                               \
	((__wum_kernel_NTSTATUS)0xC0000190)
#define _WUM_KERNEL_STATUS_MUTANT_LIMIT_EXCEEDED                       \
	((__wum_kernel_NTSTATUS)0xC0000191)
#define _WUM_KERNEL_STATUS_NETLOGON_NOT_STARTED                        \
	((__wum_kernel_NTSTATUS)0xC0000192)
#define _WUM_KERNEL_STATUS_ACCOUNT_EXPIRED                             \
	((__wum_kernel_NTSTATUS)0xC0000193)
#define _WUM_KERNEL_STATUS_POSSIBLE_DEADLOCK                           \
	((__wum_kernel_NTSTATUS)0xC0000194)
#define _WUM_KERNEL_STATUS_NETWORK_CREDENTIAL_CONFLICT                 \
	((__wum_kernel_NTSTATUS)0xC0000195)
#define _WUM_KERNEL_STATUS_REMOTE_SESSION_LIMIT                        \
	((__wum_kernel_NTSTATUS)0xC0000196)
#define _WUM_KERNEL_STATUS_EVENTLOG_FILE_CHANGED                       \
	((__wum_kernel_NTSTATUS)0xC0000197)
#define _WUM_KERNEL_STATUS_NOLOGON_INTERDOMAIN_TRUST_ACCOUNT           \
	((__wum_kernel_NTSTATUS)0xC0000198)
#define _WUM_KERNEL_STATUS_NOLOGON_WORKSTATION_TRUST_ACCOUNT           \
	((__wum_kernel_NTSTATUS)0xC0000199)
#define _WUM_KERNEL_STATUS_NOLOGON_SERVER_TRUST_ACCOUNT                \
	((__wum_kernel_NTSTATUS)0xC000019A)
#define _WUM_KERNEL_STATUS_DOMAIN_TRUST_INCONSISTENT                   \
	((__wum_kernel_NTSTATUS)0xC000019B)
#define _WUM_KERNEL_STATUS_FS_DRIVER_REQUIRED                          \
	((__wum_kernel_NTSTATUS)0xC000019C)
#define _WUM_KERNEL_STATUS_IMAGE_ALREADY_LOADED_AS_DLL                 \
	((__wum_kernel_NTSTATUS)0xC000019D)
#define _WUM_KERNEL_STATUS_INCOMPATIBLE_WITH_GLOBAL_SHORT_NAME_REGISTRY_SETTING \
	((__wum_kernel_NTSTATUS)0xC000019E)
#define _WUM_KERNEL_STATUS_SHORT_NAMES_NOT_ENABLED_ON_VOLUME           \
	((__wum_kernel_NTSTATUS)0xC000019F)
#define _WUM_KERNEL_STATUS_SECURITY_STREAM_IS_INCONSISTENT             \
	((__wum_kernel_NTSTATUS)0xC00001A0)
#define _WUM_KERNEL_STATUS_INVALID_LOCK_RANGE                          \
	((__wum_kernel_NTSTATUS)0xC00001A1)
#define _WUM_KERNEL_STATUS_INVALID_ACE_CONDITION                       \
	((__wum_kernel_NTSTATUS)0xC00001A2)
#define _WUM_KERNEL_STATUS_IMAGE_SUBSYSTEM_NOT_PRESENT                 \
	((__wum_kernel_NTSTATUS)0xC00001A3)
#define _WUM_KERNEL_STATUS_NOTIFICATION_GUID_ALREADY_DEFINED           \
	((__wum_kernel_NTSTATUS)0xC00001A4)
#define _WUM_KERNEL_STATUS_NETWORK_OPEN_RESTRICTION                    \
	((__wum_kernel_NTSTATUS)0xC0000201)
#define _WUM_KERNEL_STATUS_NO_USER_SESSION_KEY                         \
	((__wum_kernel_NTSTATUS)0xC0000202)
#define _WUM_KERNEL_STATUS_USER_SESSION_DELETED                        \
	((__wum_kernel_NTSTATUS)0xC0000203)
#define _WUM_KERNEL_STATUS_RESOURCE_LANG_NOT_FOUND                     \
	((__wum_kernel_NTSTATUS)0xC0000204)
#define _WUM_KERNEL_STATUS_INSUFF_SERVER_RESOURCES                     \
	((__wum_kernel_NTSTATUS)0xC0000205)
#define _WUM_KERNEL_STATUS_INVALID_BUFFER_SIZE                         \
	((__wum_kernel_NTSTATUS)0xC0000206)
#define _WUM_KERNEL_STATUS_INVALID_ADDRESS_COMPONENT                   \
	((__wum_kernel_NTSTATUS)0xC0000207)
#define _WUM_KERNEL_STATUS_INVALID_ADDRESS_WILDCARD                    \
	((__wum_kernel_NTSTATUS)0xC0000208)
#define _WUM_KERNEL_STATUS_TOO_MANY_ADDRESSES                          \
	((__wum_kernel_NTSTATUS)0xC0000209)
#define _WUM_KERNEL_STATUS_ADDRESS_ALREADY_EXISTS                      \
	((__wum_kernel_NTSTATUS)0xC000020A)
#define _WUM_KERNEL_STATUS_ADDRESS_CLOSED                              \
	((__wum_kernel_NTSTATUS)0xC000020B)
#define _WUM_KERNEL_STATUS_CONNECTION_DISCONNECTED                     \
	((__wum_kernel_NTSTATUS)0xC000020C)
#define _WUM_KERNEL_STATUS_CONNECTION_RESET                            \
	((__wum_kernel_NTSTATUS)0xC000020D)
#define _WUM_KERNEL_STATUS_TOO_MANY_NODES                              \
	((__wum_kernel_NTSTATUS)0xC000020E)
#define _WUM_KERNEL_STATUS_TRANSACTION_ABORTED                         \
	((__wum_kernel_NTSTATUS)0xC000020F)
#define _WUM_KERNEL_STATUS_TRANSACTION_TIMED_OUT                       \
	((__wum_kernel_NTSTATUS)0xC0000210)
#define _WUM_KERNEL_STATUS_TRANSACTION_NO_RELEASE                      \
	((__wum_kernel_NTSTATUS)0xC0000211)
#define _WUM_KERNEL_STATUS_TRANSACTION_NO_MATCH                        \
	((__wum_kernel_NTSTATUS)0xC0000212)
#define _WUM_KERNEL_STATUS_TRANSACTION_RESPONDED                       \
	((__wum_kernel_NTSTATUS)0xC0000213)
#define _WUM_KERNEL_STATUS_TRANSACTION_INVALID_ID                      \
	((__wum_kernel_NTSTATUS)0xC0000214)
#define _WUM_KERNEL_STATUS_TRANSACTION_INVALID_TYPE                    \
	((__wum_kernel_NTSTATUS)0xC0000215)
#define _WUM_KERNEL_STATUS_NOT_SERVER_SESSION                          \
	((__wum_kernel_NTSTATUS)0xC0000216)
#define _WUM_KERNEL_STATUS_NOT_CLIENT_SESSION                          \
	((__wum_kernel_NTSTATUS)0xC0000217)
#define _WUM_KERNEL_STATUS_CANNOT_LOAD_REGISTRY_FILE                   \
	((__wum_kernel_NTSTATUS)0xC0000218)
#define _WUM_KERNEL_STATUS_DEBUG_ATTACH_FAILED                         \
	((__wum_kernel_NTSTATUS)0xC0000219)
#define _WUM_KERNEL_STATUS_SYSTEM_PROCESS_TERMINATED                   \
	((__wum_kernel_NTSTATUS)0xC000021A)
#define _WUM_KERNEL_STATUS_DATA_NOT_ACCEPTED                           \
	((__wum_kernel_NTSTATUS)0xC000021B)
#define _WUM_KERNEL_STATUS_NO_BROWSER_SERVERS_FOUND                    \
	((__wum_kernel_NTSTATUS)0xC000021C)
#define _WUM_KERNEL_STATUS_VDM_HARD_ERROR                              \
	((__wum_kernel_NTSTATUS)0xC000021D)
#define _WUM_KERNEL_STATUS_DRIVER_CANCEL_TIMEOUT                       \
	((__wum_kernel_NTSTATUS)0xC000021E)
#define _WUM_KERNEL_STATUS_REPLY_MESSAGE_MISMATCH                      \
	((__wum_kernel_NTSTATUS)0xC000021F)
#define _WUM_KERNEL_STATUS_MAPPED_ALIGNMENT                            \
	((__wum_kernel_NTSTATUS)0xC0000220)
#define _WUM_KERNEL_STATUS_IMAGE_CHECKSUM_MISMATCH                     \
	((__wum_kernel_NTSTATUS)0xC0000221)
#define _WUM_KERNEL_STATUS_LOST_WRITEBEHIND_DATA                       \
	((__wum_kernel_NTSTATUS)0xC0000222)
#define _WUM_KERNEL_STATUS_CLIENT_SERVER_PARAMETERS_INVALID            \
	((__wum_kernel_NTSTATUS)0xC0000223)
#define _WUM_KERNEL_STATUS_PASSWORD_MUST_CHANGE                        \
	((__wum_kernel_NTSTATUS)0xC0000224)
#define _WUM_KERNEL_STATUS_NOT_FOUND ((__wum_kernel_NTSTATUS)0xC0000225)
#define _WUM_KERNEL_STATUS_NOT_TINY_STREAM                             \
	((__wum_kernel_NTSTATUS)0xC0000226)
#define _WUM_KERNEL_STATUS_RECOVERY_FAILURE                            \
	((__wum_kernel_NTSTATUS)0xC0000227)
#define _WUM_KERNEL_STATUS_STACK_OVERFLOW_READ                         \
	((__wum_kernel_NTSTATUS)0xC0000228)
#define _WUM_KERNEL_STATUS_FAIL_CHECK                                  \
	((__wum_kernel_NTSTATUS)0xC0000229)
#define _WUM_KERNEL_STATUS_DUPLICATE_OBJECTID                          \
	((__wum_kernel_NTSTATUS)0xC000022A)
#define _WUM_KERNEL_STATUS_OBJECTID_EXISTS                             \
	((__wum_kernel_NTSTATUS)0xC000022B)
#define _WUM_KERNEL_STATUS_CONVERT_TO_LARGE                            \
	((__wum_kernel_NTSTATUS)0xC000022C)
#define _WUM_KERNEL_STATUS_RETRY ((__wum_kernel_NTSTATUS)0xC000022D)
#define _WUM_KERNEL_STATUS_FOUND_OUT_OF_SCOPE                          \
	((__wum_kernel_NTSTATUS)0xC000022E)
#define _WUM_KERNEL_STATUS_ALLOCATE_BUCKET                             \
	((__wum_kernel_NTSTATUS)0xC000022F)
#define _WUM_KERNEL_STATUS_PROPSET_NOT_FOUND                           \
	((__wum_kernel_NTSTATUS)0xC0000230)
#define _WUM_KERNEL_STATUS_MARSHALL_OVERFLOW                           \
	((__wum_kernel_NTSTATUS)0xC0000231)
#define _WUM_KERNEL_STATUS_INVALID_VARIANT                             \
	((__wum_kernel_NTSTATUS)0xC0000232)
#define _WUM_KERNEL_STATUS_DOMAIN_CONTROLLER_NOT_FOUND                 \
	((__wum_kernel_NTSTATUS)0xC0000233)
#define _WUM_KERNEL_STATUS_ACCOUNT_LOCKED_OUT                          \
	((__wum_kernel_NTSTATUS)0xC0000234)
#define _WUM_KERNEL_STATUS_HANDLE_NOT_CLOSABLE                         \
	((__wum_kernel_NTSTATUS)0xC0000235)
#define _WUM_KERNEL_STATUS_CONNECTION_REFUSED                          \
	((__wum_kernel_NTSTATUS)0xC0000236)
#define _WUM_KERNEL_STATUS_GRACEFUL_DISCONNECT                         \
	((__wum_kernel_NTSTATUS)0xC0000237)
#define _WUM_KERNEL_STATUS_ADDRESS_ALREADY_ASSOCIATED                  \
	((__wum_kernel_NTSTATUS)0xC0000238)
#define _WUM_KERNEL_STATUS_ADDRESS_NOT_ASSOCIATED                      \
	((__wum_kernel_NTSTATUS)0xC0000239)
#define _WUM_KERNEL_STATUS_CONNECTION_INVALID                          \
	((__wum_kernel_NTSTATUS)0xC000023A)
#define _WUM_KERNEL_STATUS_CONNECTION_ACTIVE                           \
	((__wum_kernel_NTSTATUS)0xC000023B)
#define _WUM_KERNEL_STATUS_NETWORK_UNREACHABLE                         \
	((__wum_kernel_NTSTATUS)0xC000023C)
#define _WUM_KERNEL_STATUS_HOST_UNREACHABLE                            \
	((__wum_kernel_NTSTATUS)0xC000023D)
#define _WUM_KERNEL_STATUS_PROTOCOL_UNREACHABLE                        \
	((__wum_kernel_NTSTATUS)0xC000023E)
#define _WUM_KERNEL_STATUS_PORT_UNREACHABLE                            \
	((__wum_kernel_NTSTATUS)0xC000023F)
#define _WUM_KERNEL_STATUS_REQUEST_ABORTED                             \
	((__wum_kernel_NTSTATUS)0xC0000240)
#define _WUM_KERNEL_STATUS_CONNECTION_ABORTED                          \
	((__wum_kernel_NTSTATUS)0xC0000241)
#define _WUM_KERNEL_STATUS_BAD_COMPRESSION_BUFFER                      \
	((__wum_kernel_NTSTATUS)0xC0000242)
#define _WUM_KERNEL_STATUS_USER_MAPPED_FILE                            \
	((__wum_kernel_NTSTATUS)0xC0000243)
#define _WUM_KERNEL_STATUS_AUDIT_FAILED                                \
	((__wum_kernel_NTSTATUS)0xC0000244)
#define _WUM_KERNEL_STATUS_TIMER_RESOLUTION_NOT_SET                    \
	((__wum_kernel_NTSTATUS)0xC0000245)
#define _WUM_KERNEL_STATUS_CONNECTION_COUNT_LIMIT                      \
	((__wum_kernel_NTSTATUS)0xC0000246)
#define _WUM_KERNEL_STATUS_LOGIN_TIME_RESTRICTION                      \
	((__wum_kernel_NTSTATUS)0xC0000247)
#define _WUM_KERNEL_STATUS_LOGIN_WKSTA_RESTRICTION                     \
	((__wum_kernel_NTSTATUS)0xC0000248)
#define _WUM_KERNEL_STATUS_IMAGE_MP_UP_MISMATCH                        \
	((__wum_kernel_NTSTATUS)0xC0000249)
#define _WUM_KERNEL_STATUS_INSUFFICIENT_LOGON_INFO                     \
	((__wum_kernel_NTSTATUS)0xC0000250)
#define _WUM_KERNEL_STATUS_BAD_DLL_ENTRYPOINT                          \
	((__wum_kernel_NTSTATUS)0xC0000251)
#define _WUM_KERNEL_STATUS_BAD_SERVICE_ENTRYPOINT                      \
	((__wum_kernel_NTSTATUS)0xC0000252)
#define _WUM_KERNEL_STATUS_LPC_REPLY_LOST                              \
	((__wum_kernel_NTSTATUS)0xC0000253)
#define _WUM_KERNEL_STATUS_IP_ADDRESS_CONFLICT1                        \
	((__wum_kernel_NTSTATUS)0xC0000254)
#define _WUM_KERNEL_STATUS_IP_ADDRESS_CONFLICT2                        \
	((__wum_kernel_NTSTATUS)0xC0000255)
#define _WUM_KERNEL_STATUS_REGISTRY_QUOTA_LIMIT                        \
	((__wum_kernel_NTSTATUS)0xC0000256)
#define _WUM_KERNEL_STATUS_PATH_NOT_COVERED                            \
	((__wum_kernel_NTSTATUS)0xC0000257)
#define _WUM_KERNEL_STATUS_NO_CALLBACK_ACTIVE                          \
	((__wum_kernel_NTSTATUS)0xC0000258)
#define _WUM_KERNEL_STATUS_LICENSE_QUOTA_EXCEEDED                      \
	((__wum_kernel_NTSTATUS)0xC0000259)
#define _WUM_KERNEL_STATUS_PWD_TOO_SHORT                               \
	((__wum_kernel_NTSTATUS)0xC000025A)
#define _WUM_KERNEL_STATUS_PWD_TOO_RECENT                              \
	((__wum_kernel_NTSTATUS)0xC000025B)
#define _WUM_KERNEL_STATUS_PWD_HISTORY_CONFLICT                        \
	((__wum_kernel_NTSTATUS)0xC000025C)
#define _WUM_KERNEL_STATUS_PLUGPLAY_NO_DEVICE                          \
	((__wum_kernel_NTSTATUS)0xC000025E)
#define _WUM_KERNEL_STATUS_UNSUPPORTED_COMPRESSION                     \
	((__wum_kernel_NTSTATUS)0xC000025F)
#define _WUM_KERNEL_STATUS_INVALID_HW_PROFILE                          \
	((__wum_kernel_NTSTATUS)0xC0000260)
#define _WUM_KERNEL_STATUS_INVALID_PLUGPLAY_DEVICE_PATH                \
	((__wum_kernel_NTSTATUS)0xC0000261)
#define _WUM_KERNEL_STATUS_DRIVER_ORDINAL_NOT_FOUND                    \
	((__wum_kernel_NTSTATUS)0xC0000262)
#define _WUM_KERNEL_STATUS_DRIVER_ENTRYPOINT_NOT_FOUND                 \
	((__wum_kernel_NTSTATUS)0xC0000263)
#define _WUM_KERNEL_STATUS_RESOURCE_NOT_OWNED                          \
	((__wum_kernel_NTSTATUS)0xC0000264)
#define _WUM_KERNEL_STATUS_TOO_MANY_LINKS                              \
	((__wum_kernel_NTSTATUS)0xC0000265)
#define _WUM_KERNEL_STATUS_QUOTA_LIST_INCONSISTENT                     \
	((__wum_kernel_NTSTATUS)0xC0000266)
#define _WUM_KERNEL_STATUS_FILE_IS_OFFLINE                             \
	((__wum_kernel_NTSTATUS)0xC0000267)
#define _WUM_KERNEL_STATUS_EVALUATION_EXPIRATION                       \
	((__wum_kernel_NTSTATUS)0xC0000268)
#define _WUM_KERNEL_STATUS_ILLEGAL_DLL_RELOCATION                      \
	((__wum_kernel_NTSTATUS)0xC0000269)
#define _WUM_KERNEL_STATUS_LICENSE_VIOLATION                           \
	((__wum_kernel_NTSTATUS)0xC000026A)
#define _WUM_KERNEL_STATUS_DLL_INIT_FAILED_LOGOFF                      \
	((__wum_kernel_NTSTATUS)0xC000026B)
#define _WUM_KERNEL_STATUS_DRIVER_UNABLE_TO_LOAD                       \
	((__wum_kernel_NTSTATUS)0xC000026C)
#define _WUM_KERNEL_STATUS_DFS_UNAVAILABLE                             \
	((__wum_kernel_NTSTATUS)0xC000026D)
#define _WUM_KERNEL_STATUS_VOLUME_DISMOUNTED                           \
	((__wum_kernel_NTSTATUS)0xC000026E)
#define _WUM_KERNEL_STATUS_WX86_INTERNAL_ERROR                         \
	((__wum_kernel_NTSTATUS)0xC000026F)
#define _WUM_KERNEL_STATUS_WX86_FLOAT_STACK_CHECK                      \
	((__wum_kernel_NTSTATUS)0xC0000270)
#define _WUM_KERNEL_STATUS_VALIDATE_CONTINUE                           \
	((__wum_kernel_NTSTATUS)0xC0000271)
#define _WUM_KERNEL_STATUS_NO_MATCH ((__wum_kernel_NTSTATUS)0xC0000272)
#define _WUM_KERNEL_STATUS_NO_MORE_MATCHES                             \
	((__wum_kernel_NTSTATUS)0xC0000273)
#define _WUM_KERNEL_STATUS_NOT_A_REPARSE_POINT                         \
	((__wum_kernel_NTSTATUS)0xC0000275)
#define _WUM_KERNEL_STATUS_IO_REPARSE_TAG_INVALID                      \
	((__wum_kernel_NTSTATUS)0xC0000276)
#define _WUM_KERNEL_STATUS_IO_REPARSE_TAG_MISMATCH                     \
	((__wum_kernel_NTSTATUS)0xC0000277)
#define _WUM_KERNEL_STATUS_IO_REPARSE_DATA_INVALID                     \
	((__wum_kernel_NTSTATUS)0xC0000278)
#define _WUM_KERNEL_STATUS_IO_REPARSE_TAG_NOT_HANDLED                  \
	((__wum_kernel_NTSTATUS)0xC0000279)
#define _WUM_KERNEL_STATUS_REPARSE_POINT_NOT_RESOLVED                  \
	((__wum_kernel_NTSTATUS)0xC0000280)
#define _WUM_KERNEL_STATUS_DIRECTORY_IS_A_REPARSE_POINT                \
	((__wum_kernel_NTSTATUS)0xC0000281)
#define _WUM_KERNEL_STATUS_RANGE_LIST_CONFLICT                         \
	((__wum_kernel_NTSTATUS)0xC0000282)
#define _WUM_KERNEL_STATUS_SOURCE_ELEMENT_EMPTY                        \
	((__wum_kernel_NTSTATUS)0xC0000283)
#define _WUM_KERNEL_STATUS_DESTINATION_ELEMENT_FULL                    \
	((__wum_kernel_NTSTATUS)0xC0000284)
#define _WUM_KERNEL_STATUS_ILLEGAL_ELEMENT_ADDRESS                     \
	((__wum_kernel_NTSTATUS)0xC0000285)
#define _WUM_KERNEL_STATUS_MAGAZINE_NOT_PRESENT                        \
	((__wum_kernel_NTSTATUS)0xC0000286)
#define _WUM_KERNEL_STATUS_REINITIALIZATION_NEEDED                     \
	((__wum_kernel_NTSTATUS)0xC0000287)
#define _WUM_KERNEL_STATUS_ENCRYPTION_FAILED                           \
	((__wum_kernel_NTSTATUS)0xC000028A)
#define _WUM_KERNEL_STATUS_DECRYPTION_FAILED                           \
	((__wum_kernel_NTSTATUS)0xC000028B)
#define _WUM_KERNEL_STATUS_RANGE_NOT_FOUND                             \
	((__wum_kernel_NTSTATUS)0xC000028C)
#define _WUM_KERNEL_STATUS_NO_RECOVERY_POLICY                          \
	((__wum_kernel_NTSTATUS)0xC000028D)
#define _WUM_KERNEL_STATUS_NO_EFS ((__wum_kernel_NTSTATUS)0xC000028E)
#define _WUM_KERNEL_STATUS_WRONG_EFS ((__wum_kernel_NTSTATUS)0xC000028F)
#define _WUM_KERNEL_STATUS_NO_USER_KEYS                                \
	((__wum_kernel_NTSTATUS)0xC0000290)
#define _WUM_KERNEL_STATUS_FILE_NOT_ENCRYPTED                          \
	((__wum_kernel_NTSTATUS)0xC0000291)
#define _WUM_KERNEL_STATUS_NOT_EXPORT_FORMAT                           \
	((__wum_kernel_NTSTATUS)0xC0000292)
#define _WUM_KERNEL_STATUS_FILE_ENCRYPTED                              \
	((__wum_kernel_NTSTATUS)0xC0000293)
#define _WUM_KERNEL_STATUS_WMI_GUID_NOT_FOUND                          \
	((__wum_kernel_NTSTATUS)0xC0000295)
#define _WUM_KERNEL_STATUS_WMI_INSTANCE_NOT_FOUND                      \
	((__wum_kernel_NTSTATUS)0xC0000296)
#define _WUM_KERNEL_STATUS_WMI_ITEMID_NOT_FOUND                        \
	((__wum_kernel_NTSTATUS)0xC0000297)
#define _WUM_KERNEL_STATUS_WMI_TRY_AGAIN                               \
	((__wum_kernel_NTSTATUS)0xC0000298)
#define _WUM_KERNEL_STATUS_SHARED_POLICY                               \
	((__wum_kernel_NTSTATUS)0xC0000299)
#define _WUM_KERNEL_STATUS_POLICY_OBJECT_NOT_FOUND                     \
	((__wum_kernel_NTSTATUS)0xC000029A)
#define _WUM_KERNEL_STATUS_POLICY_ONLY_IN_DS                           \
	((__wum_kernel_NTSTATUS)0xC000029B)
#define _WUM_KERNEL_STATUS_VOLUME_NOT_UPGRADED                         \
	((__wum_kernel_NTSTATUS)0xC000029C)
#define _WUM_KERNEL_STATUS_REMOTE_STORAGE_NOT_ACTIVE                   \
	((__wum_kernel_NTSTATUS)0xC000029D)
#define _WUM_KERNEL_STATUS_REMOTE_STORAGE_MEDIA_ERROR                  \
	((__wum_kernel_NTSTATUS)0xC000029E)
#define _WUM_KERNEL_STATUS_NO_TRACKING_SERVICE                         \
	((__wum_kernel_NTSTATUS)0xC000029F)
#define _WUM_KERNEL_STATUS_SERVER_SID_MISMATCH                         \
	((__wum_kernel_NTSTATUS)0xC00002A0)
#define _WUM_KERNEL_STATUS_DS_NO_ATTRIBUTE_OR_VALUE                    \
	((__wum_kernel_NTSTATUS)0xC00002A1)
#define _WUM_KERNEL_STATUS_DS_INVALID_ATTRIBUTE_SYNTAX                 \
	((__wum_kernel_NTSTATUS)0xC00002A2)
#define _WUM_KERNEL_STATUS_DS_ATTRIBUTE_TYPE_UNDEFINED                 \
	((__wum_kernel_NTSTATUS)0xC00002A3)
#define _WUM_KERNEL_STATUS_DS_ATTRIBUTE_OR_VALUE_EXISTS                \
	((__wum_kernel_NTSTATUS)0xC00002A4)
#define _WUM_KERNEL_STATUS_DS_BUSY ((__wum_kernel_NTSTATUS)0xC00002A5)
#define _WUM_KERNEL_STATUS_DS_UNAVAILABLE                              \
	((__wum_kernel_NTSTATUS)0xC00002A6)
#define _WUM_KERNEL_STATUS_DS_NO_RIDS_ALLOCATED                        \
	((__wum_kernel_NTSTATUS)0xC00002A7)
#define _WUM_KERNEL_STATUS_DS_NO_MORE_RIDS                             \
	((__wum_kernel_NTSTATUS)0xC00002A8)
#define _WUM_KERNEL_STATUS_DS_INCORRECT_ROLE_OWNER                     \
	((__wum_kernel_NTSTATUS)0xC00002A9)
#define _WUM_KERNEL_STATUS_DS_RIDMGR_INIT_ERROR                        \
	((__wum_kernel_NTSTATUS)0xC00002AA)
#define _WUM_KERNEL_STATUS_DS_OBJ_CLASS_VIOLATION                      \
	((__wum_kernel_NTSTATUS)0xC00002AB)
#define _WUM_KERNEL_STATUS_DS_CANT_ON_NON_LEAF                         \
	((__wum_kernel_NTSTATUS)0xC00002AC)
#define _WUM_KERNEL_STATUS_DS_CANT_ON_RDN                              \
	((__wum_kernel_NTSTATUS)0xC00002AD)
#define _WUM_KERNEL_STATUS_DS_CANT_MOD_OBJ_CLASS                       \
	((__wum_kernel_NTSTATUS)0xC00002AE)
#define _WUM_KERNEL_STATUS_DS_CROSS_DOM_MOVE_FAILED                    \
	((__wum_kernel_NTSTATUS)0xC00002AF)
#define _WUM_KERNEL_STATUS_DS_GC_NOT_AVAILABLE                         \
	((__wum_kernel_NTSTATUS)0xC00002B0)
#define _WUM_KERNEL_STATUS_DIRECTORY_SERVICE_REQUIRED                  \
	((__wum_kernel_NTSTATUS)0xC00002B1)
#define _WUM_KERNEL_STATUS_REPARSE_ATTRIBUTE_CONFLICT                  \
	((__wum_kernel_NTSTATUS)0xC00002B2)
#define _WUM_KERNEL_STATUS_CANT_ENABLE_DENY_ONLY                       \
	((__wum_kernel_NTSTATUS)0xC00002B3)
#define _WUM_KERNEL_STATUS_FLOAT_MULTIPLE_FAULTS                       \
	((__wum_kernel_NTSTATUS)0xC00002B4)
#define _WUM_KERNEL_STATUS_FLOAT_MULTIPLE_TRAPS                        \
	((__wum_kernel_NTSTATUS)0xC00002B5)
#define _WUM_KERNEL_STATUS_DEVICE_REMOVED                              \
	((__wum_kernel_NTSTATUS)0xC00002B6)
#define _WUM_KERNEL_STATUS_JOURNAL_DELETE_IN_PROGRESS                  \
	((__wum_kernel_NTSTATUS)0xC00002B7)
#define _WUM_KERNEL_STATUS_JOURNAL_NOT_ACTIVE                          \
	((__wum_kernel_NTSTATUS)0xC00002B8)
#define _WUM_KERNEL_STATUS_NOINTERFACE                                 \
	((__wum_kernel_NTSTATUS)0xC00002B9)
#define _WUM_KERNEL_STATUS_DS_ADMIN_LIMIT_EXCEEDED                     \
	((__wum_kernel_NTSTATUS)0xC00002C1)
#define _WUM_KERNEL_STATUS_DRIVER_FAILED_SLEEP                         \
	((__wum_kernel_NTSTATUS)0xC00002C2)
#define _WUM_KERNEL_STATUS_MUTUAL_AUTHENTICATION_FAILED                \
	((__wum_kernel_NTSTATUS)0xC00002C3)
#define _WUM_KERNEL_STATUS_CORRUPT_SYSTEM_FILE                         \
	((__wum_kernel_NTSTATUS)0xC00002C4)
#define _WUM_KERNEL_STATUS_DATATYPE_MISALIGNMENT_ERROR                 \
	((__wum_kernel_NTSTATUS)0xC00002C5)
#define _WUM_KERNEL_STATUS_WMI_READ_ONLY                               \
	((__wum_kernel_NTSTATUS)0xC00002C6)
#define _WUM_KERNEL_STATUS_WMI_SET_FAILURE                             \
	((__wum_kernel_NTSTATUS)0xC00002C7)
#define _WUM_KERNEL_STATUS_COMMITMENT_MINIMUM                          \
	((__wum_kernel_NTSTATUS)0xC00002C8)
#define _WUM_KERNEL_STATUS_REG_NAT_CONSUMPTION                         \
	((__wum_kernel_NTSTATUS)0xC00002C9)
#define _WUM_KERNEL_STATUS_TRANSPORT_FULL                              \
	((__wum_kernel_NTSTATUS)0xC00002CA)
#define _WUM_KERNEL_STATUS_DS_SAM_INIT_FAILURE                         \
	((__wum_kernel_NTSTATUS)0xC00002CB)
#define _WUM_KERNEL_STATUS_ONLY_IF_CONNECTED                           \
	((__wum_kernel_NTSTATUS)0xC00002CC)
#define _WUM_KERNEL_STATUS_DS_SENSITIVE_GROUP_VIOLATION                \
	((__wum_kernel_NTSTATUS)0xC00002CD)
#define _WUM_KERNEL_STATUS_PNP_RESTART_ENUMERATION                     \
	((__wum_kernel_NTSTATUS)0xC00002CE)
#define _WUM_KERNEL_STATUS_JOURNAL_ENTRY_DELETED                       \
	((__wum_kernel_NTSTATUS)0xC00002CF)
#define _WUM_KERNEL_STATUS_DS_CANT_MOD_PRIMARYGROUPID                  \
	((__wum_kernel_NTSTATUS)0xC00002D0)
#define _WUM_KERNEL_STATUS_SYSTEM_IMAGE_BAD_SIGNATURE                  \
	((__wum_kernel_NTSTATUS)0xC00002D1)
#define _WUM_KERNEL_STATUS_PNP_REBOOT_REQUIRED                         \
	((__wum_kernel_NTSTATUS)0xC00002D2)
#define _WUM_KERNEL_STATUS_POWER_STATE_INVALID                         \
	((__wum_kernel_NTSTATUS)0xC00002D3)
#define _WUM_KERNEL_STATUS_DS_INVALID_GROUP_TYPE                       \
	((__wum_kernel_NTSTATUS)0xC00002D4)
#define _WUM_KERNEL_STATUS_DS_NO_NEST_GLOBALGROUP_IN_MIXEDDOMAIN       \
	((__wum_kernel_NTSTATUS)0xC00002D5)
#define _WUM_KERNEL_STATUS_DS_NO_NEST_LOCALGROUP_IN_MIXEDDOMAIN        \
	((__wum_kernel_NTSTATUS)0xC00002D6)
#define _WUM_KERNEL_STATUS_DS_GLOBAL_CANT_HAVE_LOCAL_MEMBER            \
	((__wum_kernel_NTSTATUS)0xC00002D7)
#define _WUM_KERNEL_STATUS_DS_GLOBAL_CANT_HAVE_UNIVERSAL_MEMBER        \
	((__wum_kernel_NTSTATUS)0xC00002D8)
#define _WUM_KERNEL_STATUS_DS_UNIVERSAL_CANT_HAVE_LOCAL_MEMBER         \
	((__wum_kernel_NTSTATUS)0xC00002D9)
#define _WUM_KERNEL_STATUS_DS_GLOBAL_CANT_HAVE_CROSSDOMAIN_MEMBER      \
	((__wum_kernel_NTSTATUS)0xC00002DA)
#define _WUM_KERNEL_STATUS_DS_LOCAL_CANT_HAVE_CROSSDOMAIN_LOCAL_MEMBER \
	((__wum_kernel_NTSTATUS)0xC00002DB)
#define _WUM_KERNEL_STATUS_DS_HAVE_PRIMARY_MEMBERS                     \
	((__wum_kernel_NTSTATUS)0xC00002DC)
#define _WUM_KERNEL_STATUS_WMI_NOT_SUPPORTED                           \
	((__wum_kernel_NTSTATUS)0xC00002DD)
#define _WUM_KERNEL_STATUS_INSUFFICIENT_POWER                          \
	((__wum_kernel_NTSTATUS)0xC00002DE)
#define _WUM_KERNEL_STATUS_SAM_NEED_BOOTKEY_PASSWORD                   \
	((__wum_kernel_NTSTATUS)0xC00002DF)
#define _WUM_KERNEL_STATUS_SAM_NEED_BOOTKEY_FLOPPY                     \
	((__wum_kernel_NTSTATUS)0xC00002E0)
#define _WUM_KERNEL_STATUS_DS_CANT_START                               \
	((__wum_kernel_NTSTATUS)0xC00002E1)
#define _WUM_KERNEL_STATUS_DS_INIT_FAILURE                             \
	((__wum_kernel_NTSTATUS)0xC00002E2)
#define _WUM_KERNEL_STATUS_SAM_INIT_FAILURE                            \
	((__wum_kernel_NTSTATUS)0xC00002E3)
#define _WUM_KERNEL_STATUS_DS_GC_REQUIRED                              \
	((__wum_kernel_NTSTATUS)0xC00002E4)
#define _WUM_KERNEL_STATUS_DS_LOCAL_MEMBER_OF_LOCAL_ONLY               \
	((__wum_kernel_NTSTATUS)0xC00002E5)
#define _WUM_KERNEL_STATUS_DS_NO_FPO_IN_UNIVERSAL_GROUPS               \
	((__wum_kernel_NTSTATUS)0xC00002E6)
#define _WUM_KERNEL_STATUS_DS_MACHINE_ACCOUNT_QUOTA_EXCEEDED           \
	((__wum_kernel_NTSTATUS)0xC00002E7)
#define _WUM_KERNEL_STATUS_CURRENT_DOMAIN_NOT_ALLOWED                  \
	((__wum_kernel_NTSTATUS)0xC00002E9)
#define _WUM_KERNEL_STATUS_CANNOT_MAKE                                 \
	((__wum_kernel_NTSTATUS)0xC00002EA)
#define _WUM_KERNEL_STATUS_SYSTEM_SHUTDOWN                             \
	((__wum_kernel_NTSTATUS)0xC00002EB)
#define _WUM_KERNEL_STATUS_DS_INIT_FAILURE_CONSOLE                     \
	((__wum_kernel_NTSTATUS)0xC00002EC)
#define _WUM_KERNEL_STATUS_DS_SAM_INIT_FAILURE_CONSOLE                 \
	((__wum_kernel_NTSTATUS)0xC00002ED)
#define _WUM_KERNEL_STATUS_UNFINISHED_CONTEXT_DELETED                  \
	((__wum_kernel_NTSTATUS)0xC00002EE)
#define _WUM_KERNEL_STATUS_NO_TGT_REPLY                                \
	((__wum_kernel_NTSTATUS)0xC00002EF)
#define _WUM_KERNEL_STATUS_OBJECTID_NOT_FOUND                          \
	((__wum_kernel_NTSTATUS)0xC00002F0)
#define _WUM_KERNEL_STATUS_NO_IP_ADDRESSES                             \
	((__wum_kernel_NTSTATUS)0xC00002F1)
#define _WUM_KERNEL_STATUS_WRONG_CREDENTIAL_HANDLE                     \
	((__wum_kernel_NTSTATUS)0xC00002F2)
#define _WUM_KERNEL_STATUS_CRYPTO_SYSTEM_INVALID                       \
	((__wum_kernel_NTSTATUS)0xC00002F3)
#define _WUM_KERNEL_STATUS_MAX_REFERRALS_EXCEEDED                      \
	((__wum_kernel_NTSTATUS)0xC00002F4)
#define _WUM_KERNEL_STATUS_MUST_BE_KDC                                 \
	((__wum_kernel_NTSTATUS)0xC00002F5)
#define _WUM_KERNEL_STATUS_STRONG_CRYPTO_NOT_SUPPORTED                 \
	((__wum_kernel_NTSTATUS)0xC00002F6)
#define _WUM_KERNEL_STATUS_TOO_MANY_PRINCIPALS                         \
	((__wum_kernel_NTSTATUS)0xC00002F7)
#define _WUM_KERNEL_STATUS_NO_PA_DATA                                  \
	((__wum_kernel_NTSTATUS)0xC00002F8)
#define _WUM_KERNEL_STATUS_PKINIT_NAME_MISMATCH                        \
	((__wum_kernel_NTSTATUS)0xC00002F9)
#define _WUM_KERNEL_STATUS_SMARTCARD_LOGON_REQUIRED                    \
	((__wum_kernel_NTSTATUS)0xC00002FA)
#define _WUM_KERNEL_STATUS_KDC_INVALID_REQUEST                         \
	((__wum_kernel_NTSTATUS)0xC00002FB)
#define _WUM_KERNEL_STATUS_KDC_UNABLE_TO_REFER                         \
	((__wum_kernel_NTSTATUS)0xC00002FC)
#define _WUM_KERNEL_STATUS_KDC_UNKNOWN_ETYPE                           \
	((__wum_kernel_NTSTATUS)0xC00002FD)
#define _WUM_KERNEL_STATUS_SHUTDOWN_IN_PROGRESS                        \
	((__wum_kernel_NTSTATUS)0xC00002FE)
#define _WUM_KERNEL_STATUS_SERVER_SHUTDOWN_IN_PROGRESS                 \
	((__wum_kernel_NTSTATUS)0xC00002FF)
#define _WUM_KERNEL_STATUS_NOT_SUPPORTED_ON_SBS                        \
	((__wum_kernel_NTSTATUS)0xC0000300)
#define _WUM_KERNEL_STATUS_WMI_GUID_DISCONNECTED                       \
	((__wum_kernel_NTSTATUS)0xC0000301)
#define _WUM_KERNEL_STATUS_WMI_ALREADY_DISABLED                        \
	((__wum_kernel_NTSTATUS)0xC0000302)
#define _WUM_KERNEL_STATUS_WMI_ALREADY_ENABLED                         \
	((__wum_kernel_NTSTATUS)0xC0000303)
#define _WUM_KERNEL_STATUS_MFT_TOO_FRAGMENTED                          \
	((__wum_kernel_NTSTATUS)0xC0000304)
#define _WUM_KERNEL_STATUS_COPY_PROTECTION_FAILURE                     \
	((__wum_kernel_NTSTATUS)0xC0000305)
#define _WUM_KERNEL_STATUS_CSS_AUTHENTICATION_FAILURE                  \
	((__wum_kernel_NTSTATUS)0xC0000306)
#define _WUM_KERNEL_STATUS_CSS_KEY_NOT_PRESENT                         \
	((__wum_kernel_NTSTATUS)0xC0000307)
#define _WUM_KERNEL_STATUS_CSS_KEY_NOT_ESTABLISHED                     \
	((__wum_kernel_NTSTATUS)0xC0000308)
#define _WUM_KERNEL_STATUS_CSS_SCRAMBLED_SECTOR                        \
	((__wum_kernel_NTSTATUS)0xC0000309)
#define _WUM_KERNEL_STATUS_CSS_REGION_MISMATCH                         \
	((__wum_kernel_NTSTATUS)0xC000030A)
#define _WUM_KERNEL_STATUS_CSS_RESETS_EXHAUSTED                        \
	((__wum_kernel_NTSTATUS)0xC000030B)
#define _WUM_KERNEL_STATUS_PKINIT_FAILURE                              \
	((__wum_kernel_NTSTATUS)0xC0000320)
#define _WUM_KERNEL_STATUS_SMARTCARD_SUBSYSTEM_FAILURE                 \
	((__wum_kernel_NTSTATUS)0xC0000321)
#define _WUM_KERNEL_STATUS_NO_KERB_KEY                                 \
	((__wum_kernel_NTSTATUS)0xC0000322)
#define _WUM_KERNEL_STATUS_HOST_DOWN ((__wum_kernel_NTSTATUS)0xC0000350)
#define _WUM_KERNEL_STATUS_UNSUPPORTED_PREAUTH                         \
	((__wum_kernel_NTSTATUS)0xC0000351)
#define _WUM_KERNEL_STATUS_EFS_ALG_BLOB_TOO_BIG                        \
	((__wum_kernel_NTSTATUS)0xC0000352)
#define _WUM_KERNEL_STATUS_PORT_NOT_SET                                \
	((__wum_kernel_NTSTATUS)0xC0000353)
#define _WUM_KERNEL_STATUS_DEBUGGER_INACTIVE                           \
	((__wum_kernel_NTSTATUS)0xC0000354)
#define _WUM_KERNEL_STATUS_DS_VERSION_CHECK_FAILURE                    \
	((__wum_kernel_NTSTATUS)0xC0000355)
#define _WUM_KERNEL_STATUS_AUDITING_DISABLED                           \
	((__wum_kernel_NTSTATUS)0xC0000356)
#define _WUM_KERNEL_STATUS_PRENT4_MACHINE_ACCOUNT                      \
	((__wum_kernel_NTSTATUS)0xC0000357)
#define _WUM_KERNEL_STATUS_DS_AG_CANT_HAVE_UNIVERSAL_MEMBER            \
	((__wum_kernel_NTSTATUS)0xC0000358)
#define _WUM_KERNEL_STATUS_INVALID_IMAGE_WIN_32                        \
	((__wum_kernel_NTSTATUS)0xC0000359)
#define _WUM_KERNEL_STATUS_INVALID_IMAGE_WIN_64                        \
	((__wum_kernel_NTSTATUS)0xC000035A)
#define _WUM_KERNEL_STATUS_BAD_BINDINGS                                \
	((__wum_kernel_NTSTATUS)0xC000035B)
#define _WUM_KERNEL_STATUS_NETWORK_SESSION_EXPIRED                     \
	((__wum_kernel_NTSTATUS)0xC000035C)
#define _WUM_KERNEL_STATUS_APPHELP_BLOCK                               \
	((__wum_kernel_NTSTATUS)0xC000035D)
#define _WUM_KERNEL_STATUS_ALL_SIDS_FILTERED                           \
	((__wum_kernel_NTSTATUS)0xC000035E)
#define _WUM_KERNEL_STATUS_NOT_SAFE_MODE_DRIVER                        \
	((__wum_kernel_NTSTATUS)0xC000035F)
#define _WUM_KERNEL_STATUS_ACCESS_DISABLED_BY_POLICY_DEFAULT           \
	((__wum_kernel_NTSTATUS)0xC0000361)
#define _WUM_KERNEL_STATUS_ACCESS_DISABLED_BY_POLICY_PATH              \
	((__wum_kernel_NTSTATUS)0xC0000362)
#define _WUM_KERNEL_STATUS_ACCESS_DISABLED_BY_POLICY_PUBLISHER         \
	((__wum_kernel_NTSTATUS)0xC0000363)
#define _WUM_KERNEL_STATUS_ACCESS_DISABLED_BY_POLICY_OTHER             \
	((__wum_kernel_NTSTATUS)0xC0000364)
#define _WUM_KERNEL_STATUS_FAILED_DRIVER_ENTRY                         \
	((__wum_kernel_NTSTATUS)0xC0000365)
#define _WUM_KERNEL_STATUS_DEVICE_ENUMERATION_ERROR                    \
	((__wum_kernel_NTSTATUS)0xC0000366)
#define _WUM_KERNEL_STATUS_MOUNT_POINT_NOT_RESOLVED                    \
	((__wum_kernel_NTSTATUS)0xC0000368)
#define _WUM_KERNEL_STATUS_INVALID_DEVICE_OBJECT_PARAMETER             \
	((__wum_kernel_NTSTATUS)0xC0000369)
#define _WUM_KERNEL_STATUS_MCA_OCCURED                                 \
	((__wum_kernel_NTSTATUS)0xC000036A)
#define _WUM_KERNEL_STATUS_DRIVER_BLOCKED_CRITICAL                     \
	((__wum_kernel_NTSTATUS)0xC000036B)
#define _WUM_KERNEL_STATUS_DRIVER_BLOCKED                              \
	((__wum_kernel_NTSTATUS)0xC000036C)
#define _WUM_KERNEL_STATUS_DRIVER_DATABASE_ERROR                       \
	((__wum_kernel_NTSTATUS)0xC000036D)
#define _WUM_KERNEL_STATUS_SYSTEM_HIVE_TOO_LARGE                       \
	((__wum_kernel_NTSTATUS)0xC000036E)
#define _WUM_KERNEL_STATUS_INVALID_IMPORT_OF_NON_DLL                   \
	((__wum_kernel_NTSTATUS)0xC000036F)
#define _WUM_KERNEL_STATUS_NO_SECRETS                                  \
	((__wum_kernel_NTSTATUS)0xC0000371)
#define _WUM_KERNEL_STATUS_ACCESS_DISABLED_NO_SAFER_UI_BY_POLICY       \
	((__wum_kernel_NTSTATUS)0xC0000372)
#define _WUM_KERNEL_STATUS_FAILED_STACK_SWITCH                         \
	((__wum_kernel_NTSTATUS)0xC0000373)
#define _WUM_KERNEL_STATUS_HEAP_CORRUPTION                             \
	((__wum_kernel_NTSTATUS)0xC0000374)
#define _WUM_KERNEL_STATUS_SMARTCARD_WRONG_PIN                         \
	((__wum_kernel_NTSTATUS)0xC0000380)
#define _WUM_KERNEL_STATUS_SMARTCARD_CARD_BLOCKED                      \
	((__wum_kernel_NTSTATUS)0xC0000381)
#define _WUM_KERNEL_STATUS_SMARTCARD_CARD_NOT_AUTHENTICATED            \
	((__wum_kernel_NTSTATUS)0xC0000382)
#define _WUM_KERNEL_STATUS_SMARTCARD_NO_CARD                           \
	((__wum_kernel_NTSTATUS)0xC0000383)
#define _WUM_KERNEL_STATUS_SMARTCARD_NO_KEY_CONTAINER                  \
	((__wum_kernel_NTSTATUS)0xC0000384)
#define _WUM_KERNEL_STATUS_SMARTCARD_NO_CERTIFICATE                    \
	((__wum_kernel_NTSTATUS)0xC0000385)
#define _WUM_KERNEL_STATUS_SMARTCARD_NO_KEYSET                         \
	((__wum_kernel_NTSTATUS)0xC0000386)
#define _WUM_KERNEL_STATUS_SMARTCARD_IO_ERROR                          \
	((__wum_kernel_NTSTATUS)0xC0000387)
#define _WUM_KERNEL_STATUS_DOWNGRADE_DETECTED                          \
	((__wum_kernel_NTSTATUS)0xC0000388)
#define _WUM_KERNEL_STATUS_SMARTCARD_CERT_REVOKED                      \
	((__wum_kernel_NTSTATUS)0xC0000389)
#define _WUM_KERNEL_STATUS_ISSUING_CA_UNTRUSTED                        \
	((__wum_kernel_NTSTATUS)0xC000038A)
#define _WUM_KERNEL_STATUS_REVOCATION_OFFLINE_C                        \
	((__wum_kernel_NTSTATUS)0xC000038B)
#define _WUM_KERNEL_STATUS_PKINIT_CLIENT_FAILURE                       \
	((__wum_kernel_NTSTATUS)0xC000038C)
#define _WUM_KERNEL_STATUS_SMARTCARD_CERT_EXPIRED                      \
	((__wum_kernel_NTSTATUS)0xC000038D)
#define _WUM_KERNEL_STATUS_DRIVER_FAILED_PRIOR_UNLOAD                  \
	((__wum_kernel_NTSTATUS)0xC000038E)
#define _WUM_KERNEL_STATUS_SMARTCARD_SILENT_CONTEXT                    \
	((__wum_kernel_NTSTATUS)0xC000038F)
#define _WUM_KERNEL_STATUS_PER_USER_TRUST_QUOTA_EXCEEDED               \
	((__wum_kernel_NTSTATUS)0xC0000401)
#define _WUM_KERNEL_STATUS_ALL_USER_TRUST_QUOTA_EXCEEDED               \
	((__wum_kernel_NTSTATUS)0xC0000402)
#define _WUM_KERNEL_STATUS_USER_DELETE_TRUST_QUOTA_EXCEEDED            \
	((__wum_kernel_NTSTATUS)0xC0000403)
#define _WUM_KERNEL_STATUS_DS_NAME_NOT_UNIQUE                          \
	((__wum_kernel_NTSTATUS)0xC0000404)
#define _WUM_KERNEL_STATUS_DS_DUPLICATE_ID_FOUND                       \
	((__wum_kernel_NTSTATUS)0xC0000405)
#define _WUM_KERNEL_STATUS_DS_GROUP_CONVERSION_ERROR                   \
	((__wum_kernel_NTSTATUS)0xC0000406)
#define _WUM_KERNEL_STATUS_VOLSNAP_PREPARE_HIBERNATE                   \
	((__wum_kernel_NTSTATUS)0xC0000407)
#define _WUM_KERNEL_STATUS_USER2USER_REQUIRED                          \
	((__wum_kernel_NTSTATUS)0xC0000408)
#define _WUM_KERNEL_STATUS_STACK_BUFFER_OVERRUN                        \
	((__wum_kernel_NTSTATUS)0xC0000409)
#define _WUM_KERNEL_STATUS_NO_S4U_PROT_SUPPORT                         \
	((__wum_kernel_NTSTATUS)0xC000040A)
#define _WUM_KERNEL_STATUS_CROSSREALM_DELEGATION_FAILURE               \
	((__wum_kernel_NTSTATUS)0xC000040B)
#define _WUM_KERNEL_STATUS_REVOCATION_OFFLINE_KDC                      \
	((__wum_kernel_NTSTATUS)0xC000040C)
#define _WUM_KERNEL_STATUS_ISSUING_CA_UNTRUSTED_KDC                    \
	((__wum_kernel_NTSTATUS)0xC000040D)
#define _WUM_KERNEL_STATUS_KDC_CERT_EXPIRED                            \
	((__wum_kernel_NTSTATUS)0xC000040E)
#define _WUM_KERNEL_STATUS_KDC_CERT_REVOKED                            \
	((__wum_kernel_NTSTATUS)0xC000040F)
#define _WUM_KERNEL_STATUS_PARAMETER_QUOTA_EXCEEDED                    \
	((__wum_kernel_NTSTATUS)0xC0000410)
#define _WUM_KERNEL_STATUS_HIBERNATION_FAILURE                         \
	((__wum_kernel_NTSTATUS)0xC0000411)
#define _WUM_KERNEL_STATUS_DELAY_LOAD_FAILED                           \
	((__wum_kernel_NTSTATUS)0xC0000412)
#define _WUM_KERNEL_STATUS_AUTHENTICATION_FIREWALL_FAILED              \
	((__wum_kernel_NTSTATUS)0xC0000413)
#define _WUM_KERNEL_STATUS_VDM_DISALLOWED                              \
	((__wum_kernel_NTSTATUS)0xC0000414)
#define _WUM_KERNEL_STATUS_HUNG_DISPLAY_DRIVER_THREAD                  \
	((__wum_kernel_NTSTATUS)0xC0000415)
#define _WUM_KERNEL_STATUS_INSUFFICIENT_RESOURCE_FOR_SPECIFIED_SHARED_SECTION_SIZE \
	((__wum_kernel_NTSTATUS)0xC0000416)
#define _WUM_KERNEL_STATUS_INVALID_CRUNTIME_PARAMETER                  \
	((__wum_kernel_NTSTATUS)0xC0000417)
#define _WUM_KERNEL_STATUS_NTLM_BLOCKED                                \
	((__wum_kernel_NTSTATUS)0xC0000418)
#define _WUM_KERNEL_STATUS_DS_SRC_SID_EXISTS_IN_FOREST                 \
	((__wum_kernel_NTSTATUS)0xC0000419)
#define _WUM_KERNEL_STATUS_DS_DOMAIN_NAME_EXISTS_IN_FOREST             \
	((__wum_kernel_NTSTATUS)0xC000041A)
#define _WUM_KERNEL_STATUS_DS_FLAT_NAME_EXISTS_IN_FOREST               \
	((__wum_kernel_NTSTATUS)0xC000041B)
#define _WUM_KERNEL_STATUS_INVALID_USER_PRINCIPAL_NAME                 \
	((__wum_kernel_NTSTATUS)0xC000041C)
#define _WUM_KERNEL_STATUS_ASSERTION_FAILURE                           \
	((__wum_kernel_NTSTATUS)0xC0000420)
#define _WUM_KERNEL_STATUS_VERIFIER_STOP                               \
	((__wum_kernel_NTSTATUS)0xC0000421)
#define _WUM_KERNEL_STATUS_CALLBACK_POP_STACK                          \
	((__wum_kernel_NTSTATUS)0xC0000423)
#define _WUM_KERNEL_STATUS_INCOMPATIBLE_DRIVER_BLOCKED                 \
	((__wum_kernel_NTSTATUS)0xC0000424)
#define _WUM_KERNEL_STATUS_HIVE_UNLOADED                               \
	((__wum_kernel_NTSTATUS)0xC0000425)
#define _WUM_KERNEL_STATUS_COMPRESSION_DISABLED                        \
	((__wum_kernel_NTSTATUS)0xC0000426)
#define _WUM_KERNEL_STATUS_FILE_SYSTEM_LIMITATION                      \
	((__wum_kernel_NTSTATUS)0xC0000427)
#define _WUM_KERNEL_STATUS_INVALID_IMAGE_HASH                          \
	((__wum_kernel_NTSTATUS)0xC0000428)
#define _WUM_KERNEL_STATUS_NOT_CAPABLE                                 \
	((__wum_kernel_NTSTATUS)0xC0000429)
#define _WUM_KERNEL_STATUS_REQUEST_OUT_OF_SEQUENCE                     \
	((__wum_kernel_NTSTATUS)0xC000042A)
#define _WUM_KERNEL_STATUS_IMPLEMENTATION_LIMIT                        \
	((__wum_kernel_NTSTATUS)0xC000042B)
#define _WUM_KERNEL_STATUS_ELEVATION_REQUIRED                          \
	((__wum_kernel_NTSTATUS)0xC000042C)
#define _WUM_KERNEL_STATUS_NO_SECURITY_CONTEXT                         \
	((__wum_kernel_NTSTATUS)0xC000042D)
#define _WUM_KERNEL_STATUS_PKU2U_CERT_FAILURE                          \
	((__wum_kernel_NTSTATUS)0xC000042E)
#define _WUM_KERNEL_STATUS_BEYOND_VDL                                  \
	((__wum_kernel_NTSTATUS)0xC0000432)
#define _WUM_KERNEL_STATUS_ENCOUNTERED_WRITE_IN_PROGRESS               \
	((__wum_kernel_NTSTATUS)0xC0000433)
#define _WUM_KERNEL_STATUS_PTE_CHANGED                                 \
	((__wum_kernel_NTSTATUS)0xC0000434)
#define _WUM_KERNEL_STATUS_PURGE_FAILED                                \
	((__wum_kernel_NTSTATUS)0xC0000435)
#define _WUM_KERNEL_STATUS_CRED_REQUIRES_CONFIRMATION                  \
	((__wum_kernel_NTSTATUS)0xC0000440)
#define _WUM_KERNEL_STATUS_CS_ENCRYPTION_INVALID_SERVER_RESPONSE       \
	((__wum_kernel_NTSTATUS)0xC0000441)
#define _WUM_KERNEL_STATUS_CS_ENCRYPTION_UNSUPPORTED_SERVER            \
	((__wum_kernel_NTSTATUS)0xC0000442)
#define _WUM_KERNEL_STATUS_CS_ENCRYPTION_EXISTING_ENCRYPTED_FILE       \
	((__wum_kernel_NTSTATUS)0xC0000443)
#define _WUM_KERNEL_STATUS_CS_ENCRYPTION_NEW_ENCRYPTED_FILE            \
	((__wum_kernel_NTSTATUS)0xC0000444)
#define _WUM_KERNEL_STATUS_CS_ENCRYPTION_FILE_NOT_CSE                  \
	((__wum_kernel_NTSTATUS)0xC0000445)
#define _WUM_KERNEL_STATUS_INVALID_LABEL                               \
	((__wum_kernel_NTSTATUS)0xC0000446)
#define _WUM_KERNEL_STATUS_DRIVER_PROCESS_TERMINATED                   \
	((__wum_kernel_NTSTATUS)0xC0000450)
#define _WUM_KERNEL_STATUS_AMBIGUOUS_SYSTEM_DEVICE                     \
	((__wum_kernel_NTSTATUS)0xC0000451)
#define _WUM_KERNEL_STATUS_SYSTEM_DEVICE_NOT_FOUND                     \
	((__wum_kernel_NTSTATUS)0xC0000452)
#define _WUM_KERNEL_STATUS_RESTART_BOOT_APPLICATION                    \
	((__wum_kernel_NTSTATUS)0xC0000453)
#define _WUM_KERNEL_STATUS_INSUFFICIENT_NVRAM_RESOURCES                \
	((__wum_kernel_NTSTATUS)0xC0000454)
#define _WUM_KERNEL_STATUS_NO_RANGES_PROCESSED                         \
	((__wum_kernel_NTSTATUS)0xC0000460)
#define _WUM_KERNEL_STATUS_DEVICE_FEATURE_NOT_SUPPORTED                \
	((__wum_kernel_NTSTATUS)0xC0000463)
#define _WUM_KERNEL_STATUS_DEVICE_UNREACHABLE                          \
	((__wum_kernel_NTSTATUS)0xC0000464)
#define _WUM_KERNEL_STATUS_INVALID_TOKEN                               \
	((__wum_kernel_NTSTATUS)0xC0000465)
#define _WUM_KERNEL_STATUS_SERVER_UNAVAILABLE                          \
	((__wum_kernel_NTSTATUS)0xC0000466)
#define _WUM_KERNEL_STATUS_FILE_NOT_AVAILABLE                          \
	((__wum_kernel_NTSTATUS)0xC0000467)
#define _WUM_KERNEL_STATUS_INVALID_TASK_NAME                           \
	((__wum_kernel_NTSTATUS)0xC0000500)
#define _WUM_KERNEL_STATUS_INVALID_TASK_INDEX                          \
	((__wum_kernel_NTSTATUS)0xC0000501)
#define _WUM_KERNEL_STATUS_THREAD_ALREADY_IN_TASK                      \
	((__wum_kernel_NTSTATUS)0xC0000502)
#define _WUM_KERNEL_STATUS_CALLBACK_BYPASS                             \
	((__wum_kernel_NTSTATUS)0xC0000503)
#define _WUM_KERNEL_STATUS_FAIL_FAST_EXCEPTION                         \
	((__wum_kernel_NTSTATUS)0xC0000602)
#define _WUM_KERNEL_STATUS_IMAGE_CERT_REVOKED                          \
	((__wum_kernel_NTSTATUS)0xC0000603)
#define _WUM_KERNEL_STATUS_PORT_CLOSED                                 \
	((__wum_kernel_NTSTATUS)0xC0000700)
#define _WUM_KERNEL_STATUS_MESSAGE_LOST                                \
	((__wum_kernel_NTSTATUS)0xC0000701)
#define _WUM_KERNEL_STATUS_INVALID_MESSAGE                             \
	((__wum_kernel_NTSTATUS)0xC0000702)
#define _WUM_KERNEL_STATUS_REQUEST_CANCELED                            \
	((__wum_kernel_NTSTATUS)0xC0000703)
#define _WUM_KERNEL_STATUS_RECURSIVE_DISPATCH                          \
	((__wum_kernel_NTSTATUS)0xC0000704)
#define _WUM_KERNEL_STATUS_LPC_RECEIVE_BUFFER_EXPECTED                 \
	((__wum_kernel_NTSTATUS)0xC0000705)
#define _WUM_KERNEL_STATUS_LPC_INVALID_CONNECTION_USAGE                \
	((__wum_kernel_NTSTATUS)0xC0000706)
#define _WUM_KERNEL_STATUS_LPC_REQUESTS_NOT_ALLOWED                    \
	((__wum_kernel_NTSTATUS)0xC0000707)
#define _WUM_KERNEL_STATUS_RESOURCE_IN_USE                             \
	((__wum_kernel_NTSTATUS)0xC0000708)
#define _WUM_KERNEL_STATUS_HARDWARE_MEMORY_ERROR                       \
	((__wum_kernel_NTSTATUS)0xC0000709)
#define _WUM_KERNEL_STATUS_THREADPOOL_HANDLE_EXCEPTION                 \
	((__wum_kernel_NTSTATUS)0xC000070A)
#define _WUM_KERNEL_STATUS_THREADPOOL_SET_EVENT_ON_COMPLETION_FAILED   \
	((__wum_kernel_NTSTATUS)0xC000070B)
#define _WUM_KERNEL_STATUS_THREADPOOL_RELEASE_SEMAPHORE_ON_COMPLETION_FAILED \
	((__wum_kernel_NTSTATUS)0xC000070C)
#define _WUM_KERNEL_STATUS_THREADPOOL_RELEASE_MUTEX_ON_COMPLETION_FAILED \
	((__wum_kernel_NTSTATUS)0xC000070D)
#define _WUM_KERNEL_STATUS_THREADPOOL_FREE_LIBRARY_ON_COMPLETION_FAILED \
	((__wum_kernel_NTSTATUS)0xC000070E)
#define _WUM_KERNEL_STATUS_THREADPOOL_RELEASED_DURING_OPERATION        \
	((__wum_kernel_NTSTATUS)0xC000070F)
#define _WUM_KERNEL_STATUS_CALLBACK_RETURNED_WHILE_IMPERSONATING       \
	((__wum_kernel_NTSTATUS)0xC0000710)
#define _WUM_KERNEL_STATUS_APC_RETURNED_WHILE_IMPERSONATING            \
	((__wum_kernel_NTSTATUS)0xC0000711)
#define _WUM_KERNEL_STATUS_PROCESS_IS_PROTECTED                        \
	((__wum_kernel_NTSTATUS)0xC0000712)
#define _WUM_KERNEL_STATUS_MCA_EXCEPTION                               \
	((__wum_kernel_NTSTATUS)0xC0000713)
#define _WUM_KERNEL_STATUS_CERTIFICATE_MAPPING_NOT_UNIQUE              \
	((__wum_kernel_NTSTATUS)0xC0000714)
#define _WUM_KERNEL_STATUS_SYMLINK_CLASS_DISABLED                      \
	((__wum_kernel_NTSTATUS)0xC0000715)
#define _WUM_KERNEL_STATUS_INVALID_IDN_NORMALIZATION                   \
	((__wum_kernel_NTSTATUS)0xC0000716)
#define _WUM_KERNEL_STATUS_NO_UNICODE_TRANSLATION                      \
	((__wum_kernel_NTSTATUS)0xC0000717)
#define _WUM_KERNEL_STATUS_ALREADY_REGISTERED                          \
	((__wum_kernel_NTSTATUS)0xC0000718)
#define _WUM_KERNEL_STATUS_CONTEXT_MISMATCH                            \
	((__wum_kernel_NTSTATUS)0xC0000719)
#define _WUM_KERNEL_STATUS_PORT_ALREADY_HAS_COMPLETION_LIST            \
	((__wum_kernel_NTSTATUS)0xC000071A)
#define _WUM_KERNEL_STATUS_CALLBACK_RETURNED_THREAD_PRIORITY           \
	((__wum_kernel_NTSTATUS)0xC000071B)
#define _WUM_KERNEL_STATUS_INVALID_THREAD                              \
	((__wum_kernel_NTSTATUS)0xC000071C)
#define _WUM_KERNEL_STATUS_CALLBACK_RETURNED_TRANSACTION               \
	((__wum_kernel_NTSTATUS)0xC000071D)
#define _WUM_MSVCRT_RPC_NT_INVALID_STRING_BINDING                      \
	((__wum_kernel_NTSTATUS)0xC0020001)
#define _WUM_MSVCRT_RPC_NT_WRONG_KIND_OF_BINDING                       \
	((__wum_kernel_NTSTATUS)0xC0020002)
#define _WUM_MSVCRT_RPC_NT_INVALID_BINDING                             \
	((__wum_kernel_NTSTATUS)0xC0020003)
#define _WUM_MSVCRT_RPC_NT_PROTSEQ_NOT_SUPPORTED                       \
	((__wum_kernel_NTSTATUS)0xC0020004)
#define _WUM_MSVCRT_RPC_NT_INVALID_RPC_PROTSEQ                         \
	((__wum_kernel_NTSTATUS)0xC0020005)
#define _WUM_MSVCRT_RPC_NT_INVALID_STRING_UUID                         \
	((__wum_kernel_NTSTATUS)0xC0020006)
#define _WUM_MSVCRT_RPC_NT_INVALID_ENDPOINT_FORMAT                     \
	((__wum_kernel_NTSTATUS)0xC0020007)
#define _WUM_MSVCRT_RPC_NT_INVALID_NET_ADDR                            \
	((__wum_kernel_NTSTATUS)0xC0020008)
#define _WUM_MSVCRT_RPC_NT_NO_ENDPOINT_FOUND                           \
	((__wum_kernel_NTSTATUS)0xC0020009)
#define _WUM_MSVCRT_RPC_NT_INVALID_TIMEOUT                             \
	((__wum_kernel_NTSTATUS)0xC002000A)
#define _WUM_MSVCRT_RPC_NT_OBJECT_NOT_FOUND                            \
	((__wum_kernel_NTSTATUS)0xC002000B)
#define _WUM_MSVCRT_RPC_NT_ALREADY_REGISTERED                          \
	((__wum_kernel_NTSTATUS)0xC002000C)
#define _WUM_MSVCRT_RPC_NT_TYPE_ALREADY_REGISTERED                     \
	((__wum_kernel_NTSTATUS)0xC002000D)
#define _WUM_MSVCRT_RPC_NT_ALREADY_LISTENING                           \
	((__wum_kernel_NTSTATUS)0xC002000E)
#define _WUM_MSVCRT_RPC_NT_NO_PROTSEQS_REGISTERED                      \
	((__wum_kernel_NTSTATUS)0xC002000F)
#define _WUM_MSVCRT_RPC_NT_NOT_LISTENING                               \
	((__wum_kernel_NTSTATUS)0xC0020010)
#define _WUM_MSVCRT_RPC_NT_UNKNOWN_MGR_TYPE                            \
	((__wum_kernel_NTSTATUS)0xC0020011)
#define _WUM_MSVCRT_RPC_NT_UNKNOWN_IF                                  \
	((__wum_kernel_NTSTATUS)0xC0020012)
#define _WUM_MSVCRT_RPC_NT_NO_BINDINGS                                 \
	((__wum_kernel_NTSTATUS)0xC0020013)
#define _WUM_MSVCRT_RPC_NT_NO_PROTSEQS                                 \
	((__wum_kernel_NTSTATUS)0xC0020014)
#define _WUM_MSVCRT_RPC_NT_CANT_CREATE_ENDPOINT                        \
	((__wum_kernel_NTSTATUS)0xC0020015)
#define _WUM_MSVCRT_RPC_NT_OUT_OF_RESOURCES                            \
	((__wum_kernel_NTSTATUS)0xC0020016)
#define _WUM_MSVCRT_RPC_NT_SERVER_UNAVAILABLE                          \
	((__wum_kernel_NTSTATUS)0xC0020017)
#define _WUM_MSVCRT_RPC_NT_SERVER_TOO_BUSY                             \
	((__wum_kernel_NTSTATUS)0xC0020018)
#define _WUM_MSVCRT_RPC_NT_INVALID_NETWORK_OPTIONS                     \
	((__wum_kernel_NTSTATUS)0xC0020019)
#define _WUM_MSVCRT_RPC_NT_NO_CALL_ACTIVE                              \
	((__wum_kernel_NTSTATUS)0xC002001A)
#define _WUM_MSVCRT_RPC_NT_CALL_FAILED                                 \
	((__wum_kernel_NTSTATUS)0xC002001B)
#define _WUM_MSVCRT_RPC_NT_CALL_FAILED_DNE                             \
	((__wum_kernel_NTSTATUS)0xC002001C)
#define _WUM_MSVCRT_RPC_NT_PROTOCOL_ERROR                              \
	((__wum_kernel_NTSTATUS)0xC002001D)
#define _WUM_MSVCRT_RPC_NT_UNSUPPORTED_TRANS_SYN                       \
	((__wum_kernel_NTSTATUS)0xC002001F)
#define _WUM_MSVCRT_RPC_NT_UNSUPPORTED_TYPE                            \
	((__wum_kernel_NTSTATUS)0xC0020021)
#define _WUM_MSVCRT_RPC_NT_INVALID_TAG                                 \
	((__wum_kernel_NTSTATUS)0xC0020022)
#define _WUM_MSVCRT_RPC_NT_INVALID_BOUND                               \
	((__wum_kernel_NTSTATUS)0xC0020023)
#define _WUM_MSVCRT_RPC_NT_NO_ENTRY_NAME                               \
	((__wum_kernel_NTSTATUS)0xC0020024)
#define _WUM_MSVCRT_RPC_NT_INVALID_NAME_SYNTAX                         \
	((__wum_kernel_NTSTATUS)0xC0020025)
#define _WUM_MSVCRT_RPC_NT_UNSUPPORTED_NAME_SYNTAX                     \
	((__wum_kernel_NTSTATUS)0xC0020026)
#define _WUM_MSVCRT_RPC_NT_UUID_NO_ADDRESS                             \
	((__wum_kernel_NTSTATUS)0xC0020028)
#define _WUM_MSVCRT_RPC_NT_DUPLICATE_ENDPOINT                          \
	((__wum_kernel_NTSTATUS)0xC0020029)
#define _WUM_MSVCRT_RPC_NT_UNKNOWN_AUTHN_TYPE                          \
	((__wum_kernel_NTSTATUS)0xC002002A)
#define _WUM_MSVCRT_RPC_NT_MAX_CALLS_TOO_SMALL                         \
	((__wum_kernel_NTSTATUS)0xC002002B)
#define _WUM_MSVCRT_RPC_NT_STRING_TOO_LONG                             \
	((__wum_kernel_NTSTATUS)0xC002002C)
#define _WUM_MSVCRT_RPC_NT_PROTSEQ_NOT_FOUND                           \
	((__wum_kernel_NTSTATUS)0xC002002D)
#define _WUM_MSVCRT_RPC_NT_PROCNUM_OUT_OF_RANGE                        \
	((__wum_kernel_NTSTATUS)0xC002002E)
#define _WUM_MSVCRT_RPC_NT_BINDING_HAS_NO_AUTH                         \
	((__wum_kernel_NTSTATUS)0xC002002F)
#define _WUM_MSVCRT_RPC_NT_UNKNOWN_AUTHN_SERVICE                       \
	((__wum_kernel_NTSTATUS)0xC0020030)
#define _WUM_MSVCRT_RPC_NT_UNKNOWN_AUTHN_LEVEL                         \
	((__wum_kernel_NTSTATUS)0xC0020031)
#define _WUM_MSVCRT_RPC_NT_INVALID_AUTH_IDENTITY                       \
	((__wum_kernel_NTSTATUS)0xC0020032)
#define _WUM_MSVCRT_RPC_NT_UNKNOWN_AUTHZ_SERVICE                       \
	((__wum_kernel_NTSTATUS)0xC0020033)
#define _WUM_MSVCRT_EPT_NT_INVALID_ENTRY                               \
	((__wum_kernel_NTSTATUS)0xC0020034)
#define _WUM_MSVCRT_EPT_NT_CANT_PERFORM_OP                             \
	((__wum_kernel_NTSTATUS)0xC0020035)
#define _WUM_MSVCRT_EPT_NT_NOT_REGISTERED                              \
	((__wum_kernel_NTSTATUS)0xC0020036)
#define _WUM_MSVCRT_RPC_NT_NOTHING_TO_EXPORT                           \
	((__wum_kernel_NTSTATUS)0xC0020037)
#define _WUM_MSVCRT_RPC_NT_INCOMPLETE_NAME                             \
	((__wum_kernel_NTSTATUS)0xC0020038)
#define _WUM_MSVCRT_RPC_NT_INVALID_VERS_OPTION                         \
	((__wum_kernel_NTSTATUS)0xC0020039)
#define _WUM_MSVCRT_RPC_NT_NO_MORE_MEMBERS                             \
	((__wum_kernel_NTSTATUS)0xC002003A)
#define _WUM_MSVCRT_RPC_NT_NOT_ALL_OBJS_UNEXPORTED                     \
	((__wum_kernel_NTSTATUS)0xC002003B)
#define _WUM_MSVCRT_RPC_NT_INTERFACE_NOT_FOUND                         \
	((__wum_kernel_NTSTATUS)0xC002003C)
#define _WUM_MSVCRT_RPC_NT_ENTRY_ALREADY_EXISTS                        \
	((__wum_kernel_NTSTATUS)0xC002003D)
#define _WUM_MSVCRT_RPC_NT_ENTRY_NOT_FOUND                             \
	((__wum_kernel_NTSTATUS)0xC002003E)
#define _WUM_MSVCRT_RPC_NT_NAME_SERVICE_UNAVAILABLE                    \
	((__wum_kernel_NTSTATUS)0xC002003F)
#define _WUM_MSVCRT_RPC_NT_INVALID_NAF_ID                              \
	((__wum_kernel_NTSTATUS)0xC0020040)
#define _WUM_MSVCRT_RPC_NT_CANNOT_SUPPORT                              \
	((__wum_kernel_NTSTATUS)0xC0020041)
#define _WUM_MSVCRT_RPC_NT_NO_CONTEXT_AVAILABLE                        \
	((__wum_kernel_NTSTATUS)0xC0020042)
#define _WUM_MSVCRT_RPC_NT_INTERNAL_ERROR                              \
	((__wum_kernel_NTSTATUS)0xC0020043)
#define _WUM_MSVCRT_RPC_NT_ZERO_DIVIDE                                 \
	((__wum_kernel_NTSTATUS)0xC0020044)
#define _WUM_MSVCRT_RPC_NT_ADDRESS_ERROR                               \
	((__wum_kernel_NTSTATUS)0xC0020045)
#define _WUM_MSVCRT_RPC_NT_FP_DIV_ZERO                                 \
	((__wum_kernel_NTSTATUS)0xC0020046)
#define _WUM_MSVCRT_RPC_NT_FP_UNDERFLOW                                \
	((__wum_kernel_NTSTATUS)0xC0020047)
#define _WUM_MSVCRT_RPC_NT_FP_OVERFLOW                                 \
	((__wum_kernel_NTSTATUS)0xC0020048)
#define _WUM_MSVCRT_RPC_NT_CALL_IN_PROGRESS                            \
	((__wum_kernel_NTSTATUS)0xC0020049)
#define _WUM_MSVCRT_RPC_NT_NO_MORE_BINDINGS                            \
	((__wum_kernel_NTSTATUS)0xC002004A)
#define _WUM_MSVCRT_RPC_NT_GROUP_MEMBER_NOT_FOUND                      \
	((__wum_kernel_NTSTATUS)0xC002004B)
#define _WUM_MSVCRT_EPT_NT_CANT_CREATE                                 \
	((__wum_kernel_NTSTATUS)0xC002004C)
#define _WUM_MSVCRT_RPC_NT_INVALID_OBJECT                              \
	((__wum_kernel_NTSTATUS)0xC002004D)
#define _WUM_MSVCRT_RPC_NT_NO_INTERFACES                               \
	((__wum_kernel_NTSTATUS)0xC002004F)
#define _WUM_MSVCRT_RPC_NT_CALL_CANCELLED                              \
	((__wum_kernel_NTSTATUS)0xC0020050)
#define _WUM_MSVCRT_RPC_NT_BINDING_INCOMPLETE                          \
	((__wum_kernel_NTSTATUS)0xC0020051)
#define _WUM_MSVCRT_RPC_NT_COMM_FAILURE                                \
	((__wum_kernel_NTSTATUS)0xC0020052)
#define _WUM_MSVCRT_RPC_NT_UNSUPPORTED_AUTHN_LEVEL                     \
	((__wum_kernel_NTSTATUS)0xC0020053)
#define _WUM_MSVCRT_RPC_NT_NO_PRINC_NAME                               \
	((__wum_kernel_NTSTATUS)0xC0020054)
#define _WUM_MSVCRT_RPC_NT_NOT_RPC_ERROR                               \
	((__wum_kernel_NTSTATUS)0xC0020055)
#define _WUM_MSVCRT_RPC_NT_SEC_PKG_ERROR                               \
	((__wum_kernel_NTSTATUS)0xC0020057)
#define _WUM_MSVCRT_RPC_NT_NOT_CANCELLED                               \
	((__wum_kernel_NTSTATUS)0xC0020058)
#define _WUM_MSVCRT_RPC_NT_INVALID_ASYNC_HANDLE                        \
	((__wum_kernel_NTSTATUS)0xC0020062)
#define _WUM_MSVCRT_RPC_NT_INVALID_ASYNC_CALL                          \
	((__wum_kernel_NTSTATUS)0xC0020063)
#define _WUM_MSVCRT_RPC_NT_PROXY_ACCESS_DENIED                         \
	((__wum_kernel_NTSTATUS)0xC0020064)
#define _WUM_MSVCRT_RPC_NT_NO_MORE_ENTRIES                             \
	((__wum_kernel_NTSTATUS)0xC0030001)
#define _WUM_MSVCRT_RPC_NT_SS_CHAR_TRANS_OPEN_FAIL                     \
	((__wum_kernel_NTSTATUS)0xC0030002)
#define _WUM_MSVCRT_RPC_NT_SS_CHAR_TRANS_SHORT_FILE                    \
	((__wum_kernel_NTSTATUS)0xC0030003)
#define _WUM_MSVCRT_RPC_NT_SS_IN_NULL_CONTEXT                          \
	((__wum_kernel_NTSTATUS)0xC0030004)
#define _WUM_MSVCRT_RPC_NT_SS_CONTEXT_MISMATCH                         \
	((__wum_kernel_NTSTATUS)0xC0030005)
#define _WUM_MSVCRT_RPC_NT_SS_CONTEXT_DAMAGED                          \
	((__wum_kernel_NTSTATUS)0xC0030006)
#define _WUM_MSVCRT_RPC_NT_SS_HANDLES_MISMATCH                         \
	((__wum_kernel_NTSTATUS)0xC0030007)
#define _WUM_MSVCRT_RPC_NT_SS_CANNOT_GET_CALL_HANDLE                   \
	((__wum_kernel_NTSTATUS)0xC0030008)
#define _WUM_MSVCRT_RPC_NT_NULL_REF_POINTER                            \
	((__wum_kernel_NTSTATUS)0xC0030009)
#define _WUM_MSVCRT_RPC_NT_ENUM_VALUE_OUT_OF_RANGE                     \
	((__wum_kernel_NTSTATUS)0xC003000A)
#define _WUM_MSVCRT_RPC_NT_BYTE_COUNT_TOO_SMALL                        \
	((__wum_kernel_NTSTATUS)0xC003000B)
#define _WUM_MSVCRT_RPC_NT_BAD_STUB_DATA                               \
	((__wum_kernel_NTSTATUS)0xC003000C)
#define _WUM_MSVCRT_RPC_NT_INVALID_ES_ACTION                           \
	((__wum_kernel_NTSTATUS)0xC0030059)
#define _WUM_MSVCRT_RPC_NT_WRONG_ES_VERSION                            \
	((__wum_kernel_NTSTATUS)0xC003005A)
#define _WUM_MSVCRT_RPC_NT_WRONG_STUB_VERSION                          \
	((__wum_kernel_NTSTATUS)0xC003005B)
#define _WUM_MSVCRT_RPC_NT_INVALID_PIPE_OBJECT                         \
	((__wum_kernel_NTSTATUS)0xC003005C)
#define _WUM_MSVCRT_RPC_NT_INVALID_PIPE_OPERATION                      \
	((__wum_kernel_NTSTATUS)0xC003005D)
#define _WUM_MSVCRT_RPC_NT_WRONG_PIPE_VERSION                          \
	((__wum_kernel_NTSTATUS)0xC003005E)
#define _WUM_MSVCRT_RPC_NT_PIPE_CLOSED                                 \
	((__wum_kernel_NTSTATUS)0xC003005F)
#define _WUM_MSVCRT_RPC_NT_PIPE_DISCIPLINE_ERROR                       \
	((__wum_kernel_NTSTATUS)0xC0030060)
#define _WUM_MSVCRT_RPC_NT_PIPE_EMPTY                                  \
	((__wum_kernel_NTSTATUS)0xC0030061)
#define _WUM_KERNEL_STATUS_PNP_BAD_MPS_TABLE                           \
	((__wum_kernel_NTSTATUS)0xC0040035)
#define _WUM_KERNEL_STATUS_PNP_TRANSLATION_FAILED                      \
	((__wum_kernel_NTSTATUS)0xC0040036)
#define _WUM_KERNEL_STATUS_PNP_IRQ_TRANSLATION_FAILED                  \
	((__wum_kernel_NTSTATUS)0xC0040037)
#define _WUM_KERNEL_STATUS_PNP_INVALID_ID                              \
	((__wum_kernel_NTSTATUS)0xC0040038)
#define _WUM_KERNEL_STATUS_IO_REISSUE_AS_CACHED                        \
	((__wum_kernel_NTSTATUS)0xC0040039)
#define _WUM_KERNEL_STATUS_CTX_WINSTATION_NAME_INVALID                 \
	((__wum_kernel_NTSTATUS)0xC00A0001)
#define _WUM_KERNEL_STATUS_CTX_INVALID_PD                              \
	((__wum_kernel_NTSTATUS)0xC00A0002)
#define _WUM_KERNEL_STATUS_CTX_PD_NOT_FOUND                            \
	((__wum_kernel_NTSTATUS)0xC00A0003)
#define _WUM_KERNEL_STATUS_CTX_CLOSE_PENDING                           \
	((__wum_kernel_NTSTATUS)0xC00A0006)
#define _WUM_KERNEL_STATUS_CTX_NO_OUTBUF                               \
	((__wum_kernel_NTSTATUS)0xC00A0007)
#define _WUM_KERNEL_STATUS_CTX_MODEM_INF_NOT_FOUND                     \
	((__wum_kernel_NTSTATUS)0xC00A0008)
#define _WUM_KERNEL_STATUS_CTX_INVALID_MODEMNAME                       \
	((__wum_kernel_NTSTATUS)0xC00A0009)
#define _WUM_KERNEL_STATUS_CTX_RESPONSE_ERROR                          \
	((__wum_kernel_NTSTATUS)0xC00A000A)
#define _WUM_KERNEL_STATUS_CTX_MODEM_RESPONSE_TIMEOUT                  \
	((__wum_kernel_NTSTATUS)0xC00A000B)
#define _WUM_KERNEL_STATUS_CTX_MODEM_RESPONSE_NO_CARRIER               \
	((__wum_kernel_NTSTATUS)0xC00A000C)
#define _WUM_KERNEL_STATUS_CTX_MODEM_RESPONSE_NO_DIALTONE              \
	((__wum_kernel_NTSTATUS)0xC00A000D)
#define _WUM_KERNEL_STATUS_CTX_MODEM_RESPONSE_BUSY                     \
	((__wum_kernel_NTSTATUS)0xC00A000E)
#define _WUM_KERNEL_STATUS_CTX_MODEM_RESPONSE_VOICE                    \
	((__wum_kernel_NTSTATUS)0xC00A000F)
#define _WUM_KERNEL_STATUS_CTX_TD_ERROR                                \
	((__wum_kernel_NTSTATUS)0xC00A0010)
#define _WUM_KERNEL_STATUS_CTX_LICENSE_CLIENT_INVALID                  \
	((__wum_kernel_NTSTATUS)0xC00A0012)
#define _WUM_KERNEL_STATUS_CTX_LICENSE_NOT_AVAILABLE                   \
	((__wum_kernel_NTSTATUS)0xC00A0013)
#define _WUM_KERNEL_STATUS_CTX_LICENSE_EXPIRED                         \
	((__wum_kernel_NTSTATUS)0xC00A0014)
#define _WUM_KERNEL_STATUS_CTX_WINSTATION_NOT_FOUND                    \
	((__wum_kernel_NTSTATUS)0xC00A0015)
#define _WUM_KERNEL_STATUS_CTX_WINSTATION_NAME_COLLISION               \
	((__wum_kernel_NTSTATUS)0xC00A0016)
#define _WUM_KERNEL_STATUS_CTX_WINSTATION_BUSY                         \
	((__wum_kernel_NTSTATUS)0xC00A0017)
#define _WUM_KERNEL_STATUS_CTX_BAD_VIDEO_MODE                          \
	((__wum_kernel_NTSTATUS)0xC00A0018)
#define _WUM_KERNEL_STATUS_CTX_GRAPHICS_INVALID                        \
	((__wum_kernel_NTSTATUS)0xC00A0022)
#define _WUM_KERNEL_STATUS_CTX_NOT_CONSOLE                             \
	((__wum_kernel_NTSTATUS)0xC00A0024)
#define _WUM_KERNEL_STATUS_CTX_CLIENT_QUERY_TIMEOUT                    \
	((__wum_kernel_NTSTATUS)0xC00A0026)
#define _WUM_KERNEL_STATUS_CTX_CONSOLE_DISCONNECT                      \
	((__wum_kernel_NTSTATUS)0xC00A0027)
#define _WUM_KERNEL_STATUS_CTX_CONSOLE_CONNECT                         \
	((__wum_kernel_NTSTATUS)0xC00A0028)
#define _WUM_KERNEL_STATUS_CTX_SHADOW_DENIED                           \
	((__wum_kernel_NTSTATUS)0xC00A002A)
#define _WUM_KERNEL_STATUS_CTX_WINSTATION_ACCESS_DENIED                \
	((__wum_kernel_NTSTATUS)0xC00A002B)
#define _WUM_KERNEL_STATUS_CTX_INVALID_WD                              \
	((__wum_kernel_NTSTATUS)0xC00A002E)
#define _WUM_KERNEL_STATUS_CTX_WD_NOT_FOUND                            \
	((__wum_kernel_NTSTATUS)0xC00A002F)
#define _WUM_KERNEL_STATUS_CTX_SHADOW_INVALID                          \
	((__wum_kernel_NTSTATUS)0xC00A0030)
#define _WUM_KERNEL_STATUS_CTX_SHADOW_DISABLED                         \
	((__wum_kernel_NTSTATUS)0xC00A0031)
#define _WUM_KERNEL_STATUS_RDP_PROTOCOL_ERROR                          \
	((__wum_kernel_NTSTATUS)0xC00A0032)
#define _WUM_KERNEL_STATUS_CTX_CLIENT_LICENSE_NOT_SET                  \
	((__wum_kernel_NTSTATUS)0xC00A0033)
#define _WUM_KERNEL_STATUS_CTX_CLIENT_LICENSE_IN_USE                   \
	((__wum_kernel_NTSTATUS)0xC00A0034)
#define _WUM_KERNEL_STATUS_CTX_SHADOW_ENDED_BY_MODE_CHANGE             \
	((__wum_kernel_NTSTATUS)0xC00A0035)
#define _WUM_KERNEL_STATUS_CTX_SHADOW_NOT_RUNNING                      \
	((__wum_kernel_NTSTATUS)0xC00A0036)
#define _WUM_KERNEL_STATUS_CTX_LOGON_DISABLED                          \
	((__wum_kernel_NTSTATUS)0xC00A0037)
#define _WUM_KERNEL_STATUS_CTX_SECURITY_LAYER_ERROR                    \
	((__wum_kernel_NTSTATUS)0xC00A0038)
#define _WUM_KERNEL_STATUS_TS_INCOMPATIBLE_SESSIONS                    \
	((__wum_kernel_NTSTATUS)0xC00A0039)
#define _WUM_KERNEL_STATUS_MUI_FILE_NOT_FOUND                          \
	((__wum_kernel_NTSTATUS)0xC00B0001)
#define _WUM_KERNEL_STATUS_MUI_INVALID_FILE                            \
	((__wum_kernel_NTSTATUS)0xC00B0002)
#define _WUM_KERNEL_STATUS_MUI_INVALID_RC_CONFIG                       \
	((__wum_kernel_NTSTATUS)0xC00B0003)
#define _WUM_KERNEL_STATUS_MUI_INVALID_LOCALE_NAME                     \
	((__wum_kernel_NTSTATUS)0xC00B0004)
#define _WUM_KERNEL_STATUS_MUI_INVALID_ULTIMATEFALLBACK_NAME           \
	((__wum_kernel_NTSTATUS)0xC00B0005)
#define _WUM_KERNEL_STATUS_MUI_FILE_NOT_LOADED                         \
	((__wum_kernel_NTSTATUS)0xC00B0006)
#define _WUM_KERNEL_STATUS_RESOURCE_ENUM_USER_STOP                     \
	((__wum_kernel_NTSTATUS)0xC00B0007)
#define _WUM_KERNEL_STATUS_CLUSTER_INVALID_NODE                        \
	((__wum_kernel_NTSTATUS)0xC0130001)
#define _WUM_KERNEL_STATUS_CLUSTER_NODE_EXISTS                         \
	((__wum_kernel_NTSTATUS)0xC0130002)
#define _WUM_KERNEL_STATUS_CLUSTER_JOIN_IN_PROGRESS                    \
	((__wum_kernel_NTSTATUS)0xC0130003)
#define _WUM_KERNEL_STATUS_CLUSTER_NODE_NOT_FOUND                      \
	((__wum_kernel_NTSTATUS)0xC0130004)
#define _WUM_KERNEL_STATUS_CLUSTER_LOCAL_NODE_NOT_FOUND                \
	((__wum_kernel_NTSTATUS)0xC0130005)
#define _WUM_KERNEL_STATUS_CLUSTER_NETWORK_EXISTS                      \
	((__wum_kernel_NTSTATUS)0xC0130006)
#define _WUM_KERNEL_STATUS_CLUSTER_NETWORK_NOT_FOUND                   \
	((__wum_kernel_NTSTATUS)0xC0130007)
#define _WUM_KERNEL_STATUS_CLUSTER_NETINTERFACE_EXISTS                 \
	((__wum_kernel_NTSTATUS)0xC0130008)
#define _WUM_KERNEL_STATUS_CLUSTER_NETINTERFACE_NOT_FOUND              \
	((__wum_kernel_NTSTATUS)0xC0130009)
#define _WUM_KERNEL_STATUS_CLUSTER_INVALID_REQUEST                     \
	((__wum_kernel_NTSTATUS)0xC013000A)
#define _WUM_KERNEL_STATUS_CLUSTER_INVALID_NETWORK_PROVIDER            \
	((__wum_kernel_NTSTATUS)0xC013000B)
#define _WUM_KERNEL_STATUS_CLUSTER_NODE_DOWN                           \
	((__wum_kernel_NTSTATUS)0xC013000C)
#define _WUM_KERNEL_STATUS_CLUSTER_NODE_UNREACHABLE                    \
	((__wum_kernel_NTSTATUS)0xC013000D)
#define _WUM_KERNEL_STATUS_CLUSTER_NODE_NOT_MEMBER                     \
	((__wum_kernel_NTSTATUS)0xC013000E)
#define _WUM_KERNEL_STATUS_CLUSTER_JOIN_NOT_IN_PROGRESS                \
	((__wum_kernel_NTSTATUS)0xC013000F)
#define _WUM_KERNEL_STATUS_CLUSTER_INVALID_NETWORK                     \
	((__wum_kernel_NTSTATUS)0xC0130010)
#define _WUM_KERNEL_STATUS_CLUSTER_NO_NET_ADAPTERS                     \
	((__wum_kernel_NTSTATUS)0xC0130011)
#define _WUM_KERNEL_STATUS_CLUSTER_NODE_UP                             \
	((__wum_kernel_NTSTATUS)0xC0130012)
#define _WUM_KERNEL_STATUS_CLUSTER_NODE_PAUSED                         \
	((__wum_kernel_NTSTATUS)0xC0130013)
#define _WUM_KERNEL_STATUS_CLUSTER_NODE_NOT_PAUSED                     \
	((__wum_kernel_NTSTATUS)0xC0130014)
#define _WUM_KERNEL_STATUS_CLUSTER_NO_SECURITY_CONTEXT                 \
	((__wum_kernel_NTSTATUS)0xC0130015)
#define _WUM_KERNEL_STATUS_CLUSTER_NETWORK_NOT_INTERNAL                \
	((__wum_kernel_NTSTATUS)0xC0130016)
#define _WUM_KERNEL_STATUS_CLUSTER_POISONED                            \
	((__wum_kernel_NTSTATUS)0xC0130017)
#define _WUM_KERNEL_STATUS_ACPI_INVALID_OPCODE                         \
	((__wum_kernel_NTSTATUS)0xC0140001)
#define _WUM_KERNEL_STATUS_ACPI_STACK_OVERFLOW                         \
	((__wum_kernel_NTSTATUS)0xC0140002)
#define _WUM_KERNEL_STATUS_ACPI_ASSERT_FAILED                          \
	((__wum_kernel_NTSTATUS)0xC0140003)
#define _WUM_KERNEL_STATUS_ACPI_INVALID_INDEX                          \
	((__wum_kernel_NTSTATUS)0xC0140004)
#define _WUM_KERNEL_STATUS_ACPI_INVALID_ARGUMENT                       \
	((__wum_kernel_NTSTATUS)0xC0140005)
#define _WUM_KERNEL_STATUS_ACPI_FATAL                                  \
	((__wum_kernel_NTSTATUS)0xC0140006)
#define _WUM_KERNEL_STATUS_ACPI_INVALID_SUPERNAME                      \
	((__wum_kernel_NTSTATUS)0xC0140007)
#define _WUM_KERNEL_STATUS_ACPI_INVALID_ARGTYPE                        \
	((__wum_kernel_NTSTATUS)0xC0140008)
#define _WUM_KERNEL_STATUS_ACPI_INVALID_OBJTYPE                        \
	((__wum_kernel_NTSTATUS)0xC0140009)
#define _WUM_KERNEL_STATUS_ACPI_INVALID_TARGETTYPE                     \
	((__wum_kernel_NTSTATUS)0xC014000A)
#define _WUM_KERNEL_STATUS_ACPI_INCORRECT_ARGUMENT_COUNT               \
	((__wum_kernel_NTSTATUS)0xC014000B)
#define _WUM_KERNEL_STATUS_ACPI_ADDRESS_NOT_MAPPED                     \
	((__wum_kernel_NTSTATUS)0xC014000C)
#define _WUM_KERNEL_STATUS_ACPI_INVALID_EVENTTYPE                      \
	((__wum_kernel_NTSTATUS)0xC014000D)
#define _WUM_KERNEL_STATUS_ACPI_HANDLER_COLLISION                      \
	((__wum_kernel_NTSTATUS)0xC014000E)
#define _WUM_KERNEL_STATUS_ACPI_INVALID_DATA                           \
	((__wum_kernel_NTSTATUS)0xC014000F)
#define _WUM_KERNEL_STATUS_ACPI_INVALID_REGION                         \
	((__wum_kernel_NTSTATUS)0xC0140010)
#define _WUM_KERNEL_STATUS_ACPI_INVALID_ACCESS_SIZE                    \
	((__wum_kernel_NTSTATUS)0xC0140011)
#define _WUM_KERNEL_STATUS_ACPI_ACQUIRE_GLOBAL_LOCK                    \
	((__wum_kernel_NTSTATUS)0xC0140012)
#define _WUM_KERNEL_STATUS_ACPI_ALREADY_INITIALIZED                    \
	((__wum_kernel_NTSTATUS)0xC0140013)
#define _WUM_KERNEL_STATUS_ACPI_NOT_INITIALIZED                        \
	((__wum_kernel_NTSTATUS)0xC0140014)
#define _WUM_KERNEL_STATUS_ACPI_INVALID_MUTEX_LEVEL                    \
	((__wum_kernel_NTSTATUS)0xC0140015)
#define _WUM_KERNEL_STATUS_ACPI_MUTEX_NOT_OWNED                        \
	((__wum_kernel_NTSTATUS)0xC0140016)
#define _WUM_KERNEL_STATUS_ACPI_MUTEX_NOT_OWNER                        \
	((__wum_kernel_NTSTATUS)0xC0140017)
#define _WUM_KERNEL_STATUS_ACPI_RS_ACCESS                              \
	((__wum_kernel_NTSTATUS)0xC0140018)
#define _WUM_KERNEL_STATUS_ACPI_INVALID_TABLE                          \
	((__wum_kernel_NTSTATUS)0xC0140019)
#define _WUM_KERNEL_STATUS_ACPI_REG_HANDLER_FAILED                     \
	((__wum_kernel_NTSTATUS)0xC0140020)
#define _WUM_KERNEL_STATUS_ACPI_POWER_REQUEST_FAILED                   \
	((__wum_kernel_NTSTATUS)0xC0140021)
#define _WUM_KERNEL_STATUS_SXS_SECTION_NOT_FOUND                       \
	((__wum_kernel_NTSTATUS)0xC0150001)
#define _WUM_KERNEL_STATUS_SXS_CANT_GEN_ACTCTX                         \
	((__wum_kernel_NTSTATUS)0xC0150002)
#define _WUM_KERNEL_STATUS_SXS_INVALID_ACTCTXDATA_FORMAT               \
	((__wum_kernel_NTSTATUS)0xC0150003)
#define _WUM_KERNEL_STATUS_SXS_ASSEMBLY_NOT_FOUND                      \
	((__wum_kernel_NTSTATUS)0xC0150004)
#define _WUM_KERNEL_STATUS_SXS_MANIFEST_FORMAT_ERROR                   \
	((__wum_kernel_NTSTATUS)0xC0150005)
#define _WUM_KERNEL_STATUS_SXS_MANIFEST_PARSE_ERROR                    \
	((__wum_kernel_NTSTATUS)0xC0150006)
#define _WUM_KERNEL_STATUS_SXS_ACTIVATION_CONTEXT_DISABLED             \
	((__wum_kernel_NTSTATUS)0xC0150007)
#define _WUM_KERNEL_STATUS_SXS_KEY_NOT_FOUND                           \
	((__wum_kernel_NTSTATUS)0xC0150008)
#define _WUM_KERNEL_STATUS_SXS_VERSION_CONFLICT                        \
	((__wum_kernel_NTSTATUS)0xC0150009)
#define _WUM_KERNEL_STATUS_SXS_WRONG_SECTION_TYPE                      \
	((__wum_kernel_NTSTATUS)0xC015000A)
#define _WUM_KERNEL_STATUS_SXS_THREAD_QUERIES_DISABLED                 \
	((__wum_kernel_NTSTATUS)0xC015000B)
#define _WUM_KERNEL_STATUS_SXS_ASSEMBLY_MISSING                        \
	((__wum_kernel_NTSTATUS)0xC015000C)
#define _WUM_KERNEL_STATUS_SXS_PROCESS_DEFAULT_ALREADY_SET             \
	((__wum_kernel_NTSTATUS)0xC015000E)
#define _WUM_KERNEL_STATUS_SXS_EARLY_DEACTIVATION                      \
	((__wum_kernel_NTSTATUS)0xC015000F)
#define _WUM_KERNEL_STATUS_SXS_INVALID_DEACTIVATION                    \
	((__wum_kernel_NTSTATUS)0xC0150010)
#define _WUM_KERNEL_STATUS_SXS_MULTIPLE_DEACTIVATION                   \
	((__wum_kernel_NTSTATUS)0xC0150011)
#define _WUM_KERNEL_STATUS_SXS_SYSTEM_DEFAULT_ACTIVATION_CONTEXT_EMPTY \
	((__wum_kernel_NTSTATUS)0xC0150012)
#define _WUM_KERNEL_STATUS_SXS_PROCESS_TERMINATION_REQUESTED           \
	((__wum_kernel_NTSTATUS)0xC0150013)
#define _WUM_KERNEL_STATUS_SXS_CORRUPT_ACTIVATION_STACK                \
	((__wum_kernel_NTSTATUS)0xC0150014)
#define _WUM_KERNEL_STATUS_SXS_CORRUPTION                              \
	((__wum_kernel_NTSTATUS)0xC0150015)
#define _WUM_KERNEL_STATUS_SXS_INVALID_IDENTITY_ATTRIBUTE_VALUE        \
	((__wum_kernel_NTSTATUS)0xC0150016)
#define _WUM_KERNEL_STATUS_SXS_INVALID_IDENTITY_ATTRIBUTE_NAME         \
	((__wum_kernel_NTSTATUS)0xC0150017)
#define _WUM_KERNEL_STATUS_SXS_IDENTITY_DUPLICATE_ATTRIBUTE            \
	((__wum_kernel_NTSTATUS)0xC0150018)
#define _WUM_KERNEL_STATUS_SXS_IDENTITY_PARSE_ERROR                    \
	((__wum_kernel_NTSTATUS)0xC0150019)
#define _WUM_KERNEL_STATUS_SXS_COMPONENT_STORE_CORRUPT                 \
	((__wum_kernel_NTSTATUS)0xC015001A)
#define _WUM_KERNEL_STATUS_SXS_FILE_HASH_MISMATCH                      \
	((__wum_kernel_NTSTATUS)0xC015001B)
#define _WUM_KERNEL_STATUS_SXS_MANIFEST_IDENTITY_SAME_BUT_CONTENTS_DIFFERENT \
	((__wum_kernel_NTSTATUS)0xC015001C)
#define _WUM_KERNEL_STATUS_SXS_IDENTITIES_DIFFERENT                    \
	((__wum_kernel_NTSTATUS)0xC015001D)
#define _WUM_KERNEL_STATUS_SXS_ASSEMBLY_IS_NOT_A_DEPLOYMENT            \
	((__wum_kernel_NTSTATUS)0xC015001E)
#define _WUM_KERNEL_STATUS_SXS_FILE_NOT_PART_OF_ASSEMBLY               \
	((__wum_kernel_NTSTATUS)0xC015001F)
#define _WUM_KERNEL_STATUS_ADVANCED_INSTALLER_FAILED                   \
	((__wum_kernel_NTSTATUS)0xC0150020)
#define _WUM_KERNEL_STATUS_XML_ENCODING_MISMATCH                       \
	((__wum_kernel_NTSTATUS)0xC0150021)
#define _WUM_KERNEL_STATUS_SXS_MANIFEST_TOO_BIG                        \
	((__wum_kernel_NTSTATUS)0xC0150022)
#define _WUM_KERNEL_STATUS_SXS_SETTING_NOT_REGISTERED                  \
	((__wum_kernel_NTSTATUS)0xC0150023)
#define _WUM_KERNEL_STATUS_SXS_TRANSACTION_CLOSURE_INCOMPLETE          \
	((__wum_kernel_NTSTATUS)0xC0150024)
#define _WUM_KERNEL_STATUS_SMI_PRIMITIVE_INSTALLER_FAILED              \
	((__wum_kernel_NTSTATUS)0xC0150025)
#define _WUM_KERNEL_STATUS_GENERIC_COMMAND_FAILED                      \
	((__wum_kernel_NTSTATUS)0xC0150026)
#define _WUM_KERNEL_STATUS_SXS_FILE_HASH_MISSING                       \
	((__wum_kernel_NTSTATUS)0xC0150027)
#define _WUM_KERNEL_STATUS_TRANSACTIONAL_CONFLICT                      \
	((__wum_kernel_NTSTATUS)0xC0190001)
#define _WUM_KERNEL_STATUS_INVALID_TRANSACTION                         \
	((__wum_kernel_NTSTATUS)0xC0190002)
#define _WUM_KERNEL_STATUS_TRANSACTION_NOT_ACTIVE                      \
	((__wum_kernel_NTSTATUS)0xC0190003)
#define _WUM_KERNEL_STATUS_TM_INITIALIZATION_FAILED                    \
	((__wum_kernel_NTSTATUS)0xC0190004)
#define _WUM_KERNEL_STATUS_RM_NOT_ACTIVE                               \
	((__wum_kernel_NTSTATUS)0xC0190005)
#define _WUM_KERNEL_STATUS_RM_METADATA_CORRUPT                         \
	((__wum_kernel_NTSTATUS)0xC0190006)
#define _WUM_KERNEL_STATUS_TRANSACTION_NOT_JOINED                      \
	((__wum_kernel_NTSTATUS)0xC0190007)
#define _WUM_KERNEL_STATUS_DIRECTORY_NOT_RM                            \
	((__wum_kernel_NTSTATUS)0xC0190008)
#define _WUM_KERNEL_STATUS_TRANSACTIONS_UNSUPPORTED_REMOTE             \
	((__wum_kernel_NTSTATUS)0xC019000A)
#define _WUM_KERNEL_STATUS_LOG_RESIZE_INVALID_SIZE                     \
	((__wum_kernel_NTSTATUS)0xC019000B)
#define _WUM_KERNEL_STATUS_REMOTE_FILE_VERSION_MISMATCH                \
	((__wum_kernel_NTSTATUS)0xC019000C)
#define _WUM_KERNEL_STATUS_CRM_PROTOCOL_ALREADY_EXISTS                 \
	((__wum_kernel_NTSTATUS)0xC019000F)
#define _WUM_KERNEL_STATUS_TRANSACTION_PROPAGATION_FAILED              \
	((__wum_kernel_NTSTATUS)0xC0190010)
#define _WUM_KERNEL_STATUS_CRM_PROTOCOL_NOT_FOUND                      \
	((__wum_kernel_NTSTATUS)0xC0190011)
#define _WUM_KERNEL_STATUS_TRANSACTION_SUPERIOR_EXISTS                 \
	((__wum_kernel_NTSTATUS)0xC0190012)
#define _WUM_KERNEL_STATUS_TRANSACTION_REQUEST_NOT_VALID               \
	((__wum_kernel_NTSTATUS)0xC0190013)
#define _WUM_KERNEL_STATUS_TRANSACTION_NOT_REQUESTED                   \
	((__wum_kernel_NTSTATUS)0xC0190014)
#define _WUM_KERNEL_STATUS_TRANSACTION_ALREADY_ABORTED                 \
	((__wum_kernel_NTSTATUS)0xC0190015)
#define _WUM_KERNEL_STATUS_TRANSACTION_ALREADY_COMMITTED               \
	((__wum_kernel_NTSTATUS)0xC0190016)
#define _WUM_KERNEL_STATUS_TRANSACTION_INVALID_MARSHALL_BUFFER         \
	((__wum_kernel_NTSTATUS)0xC0190017)
#define _WUM_KERNEL_STATUS_CURRENT_TRANSACTION_NOT_VALID               \
	((__wum_kernel_NTSTATUS)0xC0190018)
#define _WUM_KERNEL_STATUS_LOG_GROWTH_FAILED                           \
	((__wum_kernel_NTSTATUS)0xC0190019)
#define _WUM_KERNEL_STATUS_OBJECT_NO_LONGER_EXISTS                     \
	((__wum_kernel_NTSTATUS)0xC0190021)
#define _WUM_KERNEL_STATUS_STREAM_MINIVERSION_NOT_FOUND                \
	((__wum_kernel_NTSTATUS)0xC0190022)
#define _WUM_KERNEL_STATUS_STREAM_MINIVERSION_NOT_VALID                \
	((__wum_kernel_NTSTATUS)0xC0190023)
#define _WUM_KERNEL_STATUS_MINIVERSION_INACCESSIBLE_FROM_SPECIFIED_TRANSACTION \
	((__wum_kernel_NTSTATUS)0xC0190024)
#define _WUM_KERNEL_STATUS_CANT_OPEN_MINIVERSION_WITH_MODIFY_INTENT    \
	((__wum_kernel_NTSTATUS)0xC0190025)
#define _WUM_KERNEL_STATUS_CANT_CREATE_MORE_STREAM_MINIVERSIONS        \
	((__wum_kernel_NTSTATUS)0xC0190026)
#define _WUM_KERNEL_STATUS_HANDLE_NO_LONGER_VALID                      \
	((__wum_kernel_NTSTATUS)0xC0190028)
#define _WUM_KERNEL_STATUS_LOG_CORRUPTION_DETECTED                     \
	((__wum_kernel_NTSTATUS)0xC0190030)
#define _WUM_KERNEL_STATUS_RM_DISCONNECTED                             \
	((__wum_kernel_NTSTATUS)0xC0190032)
#define _WUM_KERNEL_STATUS_ENLISTMENT_NOT_SUPERIOR                     \
	((__wum_kernel_NTSTATUS)0xC0190033)
#define _WUM_KERNEL_STATUS_FILE_IDENTITY_NOT_PERSISTENT                \
	((__wum_kernel_NTSTATUS)0xC0190036)
#define _WUM_KERNEL_STATUS_CANT_BREAK_TRANSACTIONAL_DEPENDENCY         \
	((__wum_kernel_NTSTATUS)0xC0190037)
#define _WUM_KERNEL_STATUS_CANT_CROSS_RM_BOUNDARY                      \
	((__wum_kernel_NTSTATUS)0xC0190038)
#define _WUM_KERNEL_STATUS_TXF_DIR_NOT_EMPTY                           \
	((__wum_kernel_NTSTATUS)0xC0190039)
#define _WUM_KERNEL_STATUS_INDOUBT_TRANSACTIONS_EXIST                  \
	((__wum_kernel_NTSTATUS)0xC019003A)
#define _WUM_KERNEL_STATUS_TM_VOLATILE                                 \
	((__wum_kernel_NTSTATUS)0xC019003B)
#define _WUM_KERNEL_STATUS_ROLLBACK_TIMER_EXPIRED                      \
	((__wum_kernel_NTSTATUS)0xC019003C)
#define _WUM_KERNEL_STATUS_TXF_ATTRIBUTE_CORRUPT                       \
	((__wum_kernel_NTSTATUS)0xC019003D)
#define _WUM_KERNEL_STATUS_EFS_NOT_ALLOWED_IN_TRANSACTION              \
	((__wum_kernel_NTSTATUS)0xC019003E)
#define _WUM_KERNEL_STATUS_TRANSACTIONAL_OPEN_NOT_ALLOWED              \
	((__wum_kernel_NTSTATUS)0xC019003F)
#define _WUM_KERNEL_STATUS_TRANSACTED_MAPPING_UNSUPPORTED_REMOTE       \
	((__wum_kernel_NTSTATUS)0xC0190040)
#define _WUM_KERNEL_STATUS_TRANSACTION_REQUIRED_PROMOTION              \
	((__wum_kernel_NTSTATUS)0xC0190043)
#define _WUM_KERNEL_STATUS_CANNOT_EXECUTE_FILE_IN_TRANSACTION          \
	((__wum_kernel_NTSTATUS)0xC0190044)
#define _WUM_KERNEL_STATUS_TRANSACTIONS_NOT_FROZEN                     \
	((__wum_kernel_NTSTATUS)0xC0190045)
#define _WUM_KERNEL_STATUS_TRANSACTION_FREEZE_IN_PROGRESS              \
	((__wum_kernel_NTSTATUS)0xC0190046)
#define _WUM_KERNEL_STATUS_NOT_SNAPSHOT_VOLUME                         \
	((__wum_kernel_NTSTATUS)0xC0190047)
#define _WUM_KERNEL_STATUS_NO_SAVEPOINT_WITH_OPEN_FILES                \
	((__wum_kernel_NTSTATUS)0xC0190048)
#define _WUM_KERNEL_STATUS_SPARSE_NOT_ALLOWED_IN_TRANSACTION           \
	((__wum_kernel_NTSTATUS)0xC0190049)
#define _WUM_KERNEL_STATUS_TM_IDENTITY_MISMATCH                        \
	((__wum_kernel_NTSTATUS)0xC019004A)
#define _WUM_KERNEL_STATUS_FLOATED_SECTION                             \
	((__wum_kernel_NTSTATUS)0xC019004B)
#define _WUM_KERNEL_STATUS_CANNOT_ACCEPT_TRANSACTED_WORK               \
	((__wum_kernel_NTSTATUS)0xC019004C)
#define _WUM_KERNEL_STATUS_CANNOT_ABORT_TRANSACTIONS                   \
	((__wum_kernel_NTSTATUS)0xC019004D)
#define _WUM_KERNEL_STATUS_TRANSACTION_NOT_FOUND                       \
	((__wum_kernel_NTSTATUS)0xC019004E)
#define _WUM_KERNEL_STATUS_RESOURCEMANAGER_NOT_FOUND                   \
	((__wum_kernel_NTSTATUS)0xC019004F)
#define _WUM_KERNEL_STATUS_ENLISTMENT_NOT_FOUND                        \
	((__wum_kernel_NTSTATUS)0xC0190050)
#define _WUM_KERNEL_STATUS_TRANSACTIONMANAGER_NOT_FOUND                \
	((__wum_kernel_NTSTATUS)0xC0190051)
#define _WUM_KERNEL_STATUS_TRANSACTIONMANAGER_NOT_ONLINE               \
	((__wum_kernel_NTSTATUS)0xC0190052)
#define _WUM_KERNEL_STATUS_TRANSACTIONMANAGER_RECOVERY_NAME_COLLISION  \
	((__wum_kernel_NTSTATUS)0xC0190053)
#define _WUM_KERNEL_STATUS_TRANSACTION_NOT_ROOT                        \
	((__wum_kernel_NTSTATUS)0xC0190054)
#define _WUM_KERNEL_STATUS_TRANSACTION_OBJECT_EXPIRED                  \
	((__wum_kernel_NTSTATUS)0xC0190055)
#define _WUM_KERNEL_STATUS_COMPRESSION_NOT_ALLOWED_IN_TRANSACTION      \
	((__wum_kernel_NTSTATUS)0xC0190056)
#define _WUM_KERNEL_STATUS_TRANSACTION_RESPONSE_NOT_ENLISTED           \
	((__wum_kernel_NTSTATUS)0xC0190057)
#define _WUM_KERNEL_STATUS_TRANSACTION_RECORD_TOO_LONG                 \
	((__wum_kernel_NTSTATUS)0xC0190058)
#define _WUM_KERNEL_STATUS_NO_LINK_TRACKING_IN_TRANSACTION             \
	((__wum_kernel_NTSTATUS)0xC0190059)
#define _WUM_KERNEL_STATUS_OPERATION_NOT_SUPPORTED_IN_TRANSACTION      \
	((__wum_kernel_NTSTATUS)0xC019005A)
#define _WUM_KERNEL_STATUS_TRANSACTION_INTEGRITY_VIOLATED              \
	((__wum_kernel_NTSTATUS)0xC019005B)
#define _WUM_KERNEL_STATUS_EXPIRED_HANDLE                              \
	((__wum_kernel_NTSTATUS)0xC0190060)
#define _WUM_KERNEL_STATUS_TRANSACTION_NOT_ENLISTED                    \
	((__wum_kernel_NTSTATUS)0xC0190061)
#define _WUM_KERNEL_STATUS_LOG_SECTOR_INVALID                          \
	((__wum_kernel_NTSTATUS)0xC01A0001)
#define _WUM_KERNEL_STATUS_LOG_SECTOR_PARITY_INVALID                   \
	((__wum_kernel_NTSTATUS)0xC01A0002)
#define _WUM_KERNEL_STATUS_LOG_SECTOR_REMAPPED                         \
	((__wum_kernel_NTSTATUS)0xC01A0003)
#define _WUM_KERNEL_STATUS_LOG_BLOCK_INCOMPLETE                        \
	((__wum_kernel_NTSTATUS)0xC01A0004)
#define _WUM_KERNEL_STATUS_LOG_INVALID_RANGE                           \
	((__wum_kernel_NTSTATUS)0xC01A0005)
#define _WUM_KERNEL_STATUS_LOG_BLOCKS_EXHAUSTED                        \
	((__wum_kernel_NTSTATUS)0xC01A0006)
#define _WUM_KERNEL_STATUS_LOG_READ_CONTEXT_INVALID                    \
	((__wum_kernel_NTSTATUS)0xC01A0007)
#define _WUM_KERNEL_STATUS_LOG_RESTART_INVALID                         \
	((__wum_kernel_NTSTATUS)0xC01A0008)
#define _WUM_KERNEL_STATUS_LOG_BLOCK_VERSION                           \
	((__wum_kernel_NTSTATUS)0xC01A0009)
#define _WUM_KERNEL_STATUS_LOG_BLOCK_INVALID                           \
	((__wum_kernel_NTSTATUS)0xC01A000A)
#define _WUM_KERNEL_STATUS_LOG_READ_MODE_INVALID                       \
	((__wum_kernel_NTSTATUS)0xC01A000B)
#define _WUM_KERNEL_STATUS_LOG_METADATA_CORRUPT                        \
	((__wum_kernel_NTSTATUS)0xC01A000D)
#define _WUM_KERNEL_STATUS_LOG_METADATA_INVALID                        \
	((__wum_kernel_NTSTATUS)0xC01A000E)
#define _WUM_KERNEL_STATUS_LOG_METADATA_INCONSISTENT                   \
	((__wum_kernel_NTSTATUS)0xC01A000F)
#define _WUM_KERNEL_STATUS_LOG_RESERVATION_INVALID                     \
	((__wum_kernel_NTSTATUS)0xC01A0010)
#define _WUM_KERNEL_STATUS_LOG_CANT_DELETE                             \
	((__wum_kernel_NTSTATUS)0xC01A0011)
#define _WUM_KERNEL_STATUS_LOG_CONTAINER_LIMIT_EXCEEDED                \
	((__wum_kernel_NTSTATUS)0xC01A0012)
#define _WUM_KERNEL_STATUS_LOG_START_OF_LOG                            \
	((__wum_kernel_NTSTATUS)0xC01A0013)
#define _WUM_KERNEL_STATUS_LOG_POLICY_ALREADY_INSTALLED                \
	((__wum_kernel_NTSTATUS)0xC01A0014)
#define _WUM_KERNEL_STATUS_LOG_POLICY_NOT_INSTALLED                    \
	((__wum_kernel_NTSTATUS)0xC01A0015)
#define _WUM_KERNEL_STATUS_LOG_POLICY_INVALID                          \
	((__wum_kernel_NTSTATUS)0xC01A0016)
#define _WUM_KERNEL_STATUS_LOG_POLICY_CONFLICT                         \
	((__wum_kernel_NTSTATUS)0xC01A0017)
#define _WUM_KERNEL_STATUS_LOG_PINNED_ARCHIVE_TAIL                     \
	((__wum_kernel_NTSTATUS)0xC01A0018)
#define _WUM_KERNEL_STATUS_LOG_RECORD_NONEXISTENT                      \
	((__wum_kernel_NTSTATUS)0xC01A0019)
#define _WUM_KERNEL_STATUS_LOG_RECORDS_RESERVED_INVALID                \
	((__wum_kernel_NTSTATUS)0xC01A001A)
#define _WUM_KERNEL_STATUS_LOG_SPACE_RESERVED_INVALID                  \
	((__wum_kernel_NTSTATUS)0xC01A001B)
#define _WUM_KERNEL_STATUS_LOG_TAIL_INVALID                            \
	((__wum_kernel_NTSTATUS)0xC01A001C)
#define _WUM_KERNEL_STATUS_LOG_FULL ((__wum_kernel_NTSTATUS)0xC01A001D)
#define _WUM_KERNEL_STATUS_LOG_MULTIPLEXED                             \
	((__wum_kernel_NTSTATUS)0xC01A001E)
#define _WUM_KERNEL_STATUS_LOG_DEDICATED                               \
	((__wum_kernel_NTSTATUS)0xC01A001F)
#define _WUM_KERNEL_STATUS_LOG_ARCHIVE_NOT_IN_PROGRESS                 \
	((__wum_kernel_NTSTATUS)0xC01A0020)
#define _WUM_KERNEL_STATUS_LOG_ARCHIVE_IN_PROGRESS                     \
	((__wum_kernel_NTSTATUS)0xC01A0021)
#define _WUM_KERNEL_STATUS_LOG_EPHEMERAL                               \
	((__wum_kernel_NTSTATUS)0xC01A0022)
#define _WUM_KERNEL_STATUS_LOG_NOT_ENOUGH_CONTAINERS                   \
	((__wum_kernel_NTSTATUS)0xC01A0023)
#define _WUM_KERNEL_STATUS_LOG_CLIENT_ALREADY_REGISTERED               \
	((__wum_kernel_NTSTATUS)0xC01A0024)
#define _WUM_KERNEL_STATUS_LOG_CLIENT_NOT_REGISTERED                   \
	((__wum_kernel_NTSTATUS)0xC01A0025)
#define _WUM_KERNEL_STATUS_LOG_FULL_HANDLER_IN_PROGRESS                \
	((__wum_kernel_NTSTATUS)0xC01A0026)
#define _WUM_KERNEL_STATUS_LOG_CONTAINER_READ_FAILED                   \
	((__wum_kernel_NTSTATUS)0xC01A0027)
#define _WUM_KERNEL_STATUS_LOG_CONTAINER_WRITE_FAILED                  \
	((__wum_kernel_NTSTATUS)0xC01A0028)
#define _WUM_KERNEL_STATUS_LOG_CONTAINER_OPEN_FAILED                   \
	((__wum_kernel_NTSTATUS)0xC01A0029)
#define _WUM_KERNEL_STATUS_LOG_CONTAINER_STATE_INVALID                 \
	((__wum_kernel_NTSTATUS)0xC01A002A)
#define _WUM_KERNEL_STATUS_LOG_STATE_INVALID                           \
	((__wum_kernel_NTSTATUS)0xC01A002B)
#define _WUM_KERNEL_STATUS_LOG_PINNED                                  \
	((__wum_kernel_NTSTATUS)0xC01A002C)
#define _WUM_KERNEL_STATUS_LOG_METADATA_FLUSH_FAILED                   \
	((__wum_kernel_NTSTATUS)0xC01A002D)
#define _WUM_KERNEL_STATUS_LOG_INCONSISTENT_SECURITY                   \
	((__wum_kernel_NTSTATUS)0xC01A002E)
#define _WUM_KERNEL_STATUS_LOG_APPENDED_FLUSH_FAILED                   \
	((__wum_kernel_NTSTATUS)0xC01A002F)
#define _WUM_KERNEL_STATUS_LOG_PINNED_RESERVATION                      \
	((__wum_kernel_NTSTATUS)0xC01A0030)
#define _WUM_KERNEL_STATUS_VIDEO_HUNG_DISPLAY_DRIVER_THREAD            \
	((__wum_kernel_NTSTATUS)0xC01B00EA)
#define _WUM_KERNEL_STATUS_FLT_NO_HANDLER_DEFINED                      \
	((__wum_kernel_NTSTATUS)0xC01C0001)
#define _WUM_KERNEL_STATUS_FLT_CONTEXT_ALREADY_DEFINED                 \
	((__wum_kernel_NTSTATUS)0xC01C0002)
#define _WUM_KERNEL_STATUS_FLT_INVALID_ASYNCHRONOUS_REQUEST            \
	((__wum_kernel_NTSTATUS)0xC01C0003)
#define _WUM_KERNEL_STATUS_FLT_DISALLOW_FAST_IO                        \
	((__wum_kernel_NTSTATUS)0xC01C0004)
#define _WUM_KERNEL_STATUS_FLT_INVALID_NAME_REQUEST                    \
	((__wum_kernel_NTSTATUS)0xC01C0005)
#define _WUM_KERNEL_STATUS_FLT_NOT_SAFE_TO_POST_OPERATION              \
	((__wum_kernel_NTSTATUS)0xC01C0006)
#define _WUM_KERNEL_STATUS_FLT_NOT_INITIALIZED                         \
	((__wum_kernel_NTSTATUS)0xC01C0007)
#define _WUM_KERNEL_STATUS_FLT_FILTER_NOT_READY                        \
	((__wum_kernel_NTSTATUS)0xC01C0008)
#define _WUM_KERNEL_STATUS_FLT_POST_OPERATION_CLEANUP                  \
	((__wum_kernel_NTSTATUS)0xC01C0009)
#define _WUM_KERNEL_STATUS_FLT_INTERNAL_ERROR                          \
	((__wum_kernel_NTSTATUS)0xC01C000A)
#define _WUM_KERNEL_STATUS_FLT_DELETING_OBJECT                         \
	((__wum_kernel_NTSTATUS)0xC01C000B)
#define _WUM_KERNEL_STATUS_FLT_MUST_BE_NONPAGED_POOL                   \
	((__wum_kernel_NTSTATUS)0xC01C000C)
#define _WUM_KERNEL_STATUS_FLT_DUPLICATE_ENTRY                         \
	((__wum_kernel_NTSTATUS)0xC01C000D)
#define _WUM_KERNEL_STATUS_FLT_CBDQ_DISABLED                           \
	((__wum_kernel_NTSTATUS)0xC01C000E)
#define _WUM_KERNEL_STATUS_FLT_DO_NOT_ATTACH                           \
	((__wum_kernel_NTSTATUS)0xC01C000F)
#define _WUM_KERNEL_STATUS_FLT_DO_NOT_DETACH                           \
	((__wum_kernel_NTSTATUS)0xC01C0010)
#define _WUM_KERNEL_STATUS_FLT_INSTANCE_ALTITUDE_COLLISION             \
	((__wum_kernel_NTSTATUS)0xC01C0011)
#define _WUM_KERNEL_STATUS_FLT_INSTANCE_NAME_COLLISION                 \
	((__wum_kernel_NTSTATUS)0xC01C0012)
#define _WUM_KERNEL_STATUS_FLT_FILTER_NOT_FOUND                        \
	((__wum_kernel_NTSTATUS)0xC01C0013)
#define _WUM_KERNEL_STATUS_FLT_VOLUME_NOT_FOUND                        \
	((__wum_kernel_NTSTATUS)0xC01C0014)
#define _WUM_KERNEL_STATUS_FLT_INSTANCE_NOT_FOUND                      \
	((__wum_kernel_NTSTATUS)0xC01C0015)
#define _WUM_KERNEL_STATUS_FLT_CONTEXT_ALLOCATION_NOT_FOUND            \
	((__wum_kernel_NTSTATUS)0xC01C0016)
#define _WUM_KERNEL_STATUS_FLT_INVALID_CONTEXT_REGISTRATION            \
	((__wum_kernel_NTSTATUS)0xC01C0017)
#define _WUM_KERNEL_STATUS_FLT_NAME_CACHE_MISS                         \
	((__wum_kernel_NTSTATUS)0xC01C0018)
#define _WUM_KERNEL_STATUS_FLT_NO_DEVICE_OBJECT                        \
	((__wum_kernel_NTSTATUS)0xC01C0019)
#define _WUM_KERNEL_STATUS_FLT_VOLUME_ALREADY_MOUNTED                  \
	((__wum_kernel_NTSTATUS)0xC01C001A)
#define _WUM_KERNEL_STATUS_FLT_ALREADY_ENLISTED                        \
	((__wum_kernel_NTSTATUS)0xC01C001B)
#define _WUM_KERNEL_STATUS_FLT_CONTEXT_ALREADY_LINKED                  \
	((__wum_kernel_NTSTATUS)0xC01C001C)
#define _WUM_KERNEL_STATUS_FLT_NO_WAITER_FOR_REPLY                     \
	((__wum_kernel_NTSTATUS)0xC01C0020)
#define _WUM_KERNEL_STATUS_MONITOR_NO_DESCRIPTOR                       \
	((__wum_kernel_NTSTATUS)0xC01D0001)
#define _WUM_KERNEL_STATUS_MONITOR_UNKNOWN_DESCRIPTOR_FORMAT           \
	((__wum_kernel_NTSTATUS)0xC01D0002)
#define _WUM_KERNEL_STATUS_MONITOR_INVALID_DESCRIPTOR_CHECKSUM         \
	((__wum_kernel_NTSTATUS)0xC01D0003)
#define _WUM_KERNEL_STATUS_MONITOR_INVALID_STANDARD_TIMING_BLOCK       \
	((__wum_kernel_NTSTATUS)0xC01D0004)
#define _WUM_KERNEL_STATUS_MONITOR_WMI_DATABLOCK_REGISTRATION_FAILED   \
	((__wum_kernel_NTSTATUS)0xC01D0005)
#define _WUM_KERNEL_STATUS_MONITOR_INVALID_SERIAL_NUMBER_MONDSC_BLOCK  \
	((__wum_kernel_NTSTATUS)0xC01D0006)
#define _WUM_KERNEL_STATUS_MONITOR_INVALID_USER_FRIENDLY_MONDSC_BLOCK  \
	((__wum_kernel_NTSTATUS)0xC01D0007)
#define _WUM_KERNEL_STATUS_MONITOR_NO_MORE_DESCRIPTOR_DATA             \
	((__wum_kernel_NTSTATUS)0xC01D0008)
#define _WUM_KERNEL_STATUS_MONITOR_INVALID_DETAILED_TIMING_BLOCK       \
	((__wum_kernel_NTSTATUS)0xC01D0009)
#define _WUM_KERNEL_STATUS_MONITOR_INVALID_MANUFACTURE_DATE            \
	((__wum_kernel_NTSTATUS)0xC01D000A)
#define _WUM_KERNEL_STATUS_GRAPHICS_NOT_EXCLUSIVE_MODE_OWNER           \
	((__wum_kernel_NTSTATUS)0xC01E0000)
#define _WUM_KERNEL_STATUS_GRAPHICS_INSUFFICIENT_DMA_BUFFER            \
	((__wum_kernel_NTSTATUS)0xC01E0001)
#define _WUM_KERNEL_STATUS_GRAPHICS_INVALID_DISPLAY_ADAPTER            \
	((__wum_kernel_NTSTATUS)0xC01E0002)
#define _WUM_KERNEL_STATUS_GRAPHICS_ADAPTER_WAS_RESET                  \
	((__wum_kernel_NTSTATUS)0xC01E0003)
#define _WUM_KERNEL_STATUS_GRAPHICS_INVALID_DRIVER_MODEL               \
	((__wum_kernel_NTSTATUS)0xC01E0004)
#define _WUM_KERNEL_STATUS_GRAPHICS_PRESENT_MODE_CHANGED               \
	((__wum_kernel_NTSTATUS)0xC01E0005)
#define _WUM_KERNEL_STATUS_GRAPHICS_PRESENT_OCCLUDED                   \
	((__wum_kernel_NTSTATUS)0xC01E0006)
#define _WUM_KERNEL_STATUS_GRAPHICS_PRESENT_DENIED                     \
	((__wum_kernel_NTSTATUS)0xC01E0007)
#define _WUM_KERNEL_STATUS_GRAPHICS_CANNOTCOLORCONVERT                 \
	((__wum_kernel_NTSTATUS)0xC01E0008)
#define _WUM_KERNEL_STATUS_GRAPHICS_PRESENT_REDIRECTION_DISABLED       \
	((__wum_kernel_NTSTATUS)0xC01E000B)
#define _WUM_KERNEL_STATUS_GRAPHICS_PRESENT_UNOCCLUDED                 \
	((__wum_kernel_NTSTATUS)0xC01E000C)
#define _WUM_KERNEL_STATUS_GRAPHICS_NO_VIDEO_MEMORY                    \
	((__wum_kernel_NTSTATUS)0xC01E0100)
#define _WUM_KERNEL_STATUS_GRAPHICS_CANT_LOCK_MEMORY                   \
	((__wum_kernel_NTSTATUS)0xC01E0101)
#define _WUM_KERNEL_STATUS_GRAPHICS_ALLOCATION_BUSY                    \
	((__wum_kernel_NTSTATUS)0xC01E0102)
#define _WUM_KERNEL_STATUS_GRAPHICS_TOO_MANY_REFERENCES                \
	((__wum_kernel_NTSTATUS)0xC01E0103)
#define _WUM_KERNEL_STATUS_GRAPHICS_TRY_AGAIN_LATER                    \
	((__wum_kernel_NTSTATUS)0xC01E0104)
#define _WUM_KERNEL_STATUS_GRAPHICS_TRY_AGAIN_NOW                      \
	((__wum_kernel_NTSTATUS)0xC01E0105)
#define _WUM_KERNEL_STATUS_GRAPHICS_ALLOCATION_INVALID                 \
	((__wum_kernel_NTSTATUS)0xC01E0106)
#define _WUM_KERNEL_STATUS_GRAPHICS_UNSWIZZLING_APERTURE_UNAVAILABLE   \
	((__wum_kernel_NTSTATUS)0xC01E0107)
#define _WUM_KERNEL_STATUS_GRAPHICS_UNSWIZZLING_APERTURE_UNSUPPORTED   \
	((__wum_kernel_NTSTATUS)0xC01E0108)
#define _WUM_KERNEL_STATUS_GRAPHICS_CANT_EVICT_PINNED_ALLOCATION       \
	((__wum_kernel_NTSTATUS)0xC01E0109)
#define _WUM_KERNEL_STATUS_GRAPHICS_INVALID_ALLOCATION_USAGE           \
	((__wum_kernel_NTSTATUS)0xC01E0110)
#define _WUM_KERNEL_STATUS_GRAPHICS_CANT_RENDER_LOCKED_ALLOCATION      \
	((__wum_kernel_NTSTATUS)0xC01E0111)
#define _WUM_KERNEL_STATUS_GRAPHICS_ALLOCATION_CLOSED                  \
	((__wum_kernel_NTSTATUS)0xC01E0112)
#define _WUM_KERNEL_STATUS_GRAPHICS_INVALID_ALLOCATION_INSTANCE        \
	((__wum_kernel_NTSTATUS)0xC01E0113)
#define _WUM_KERNEL_STATUS_GRAPHICS_INVALID_ALLOCATION_HANDLE          \
	((__wum_kernel_NTSTATUS)0xC01E0114)
#define _WUM_KERNEL_STATUS_GRAPHICS_WRONG_ALLOCATION_DEVICE            \
	((__wum_kernel_NTSTATUS)0xC01E0115)
#define _WUM_KERNEL_STATUS_GRAPHICS_ALLOCATION_CONTENT_LOST            \
	((__wum_kernel_NTSTATUS)0xC01E0116)
#define _WUM_KERNEL_STATUS_GRAPHICS_GPU_EXCEPTION_ON_DEVICE            \
	((__wum_kernel_NTSTATUS)0xC01E0200)
#define _WUM_KERNEL_STATUS_GRAPHICS_INVALID_VIDPN_TOPOLOGY             \
	((__wum_kernel_NTSTATUS)0xC01E0300)
#define _WUM_KERNEL_STATUS_GRAPHICS_VIDPN_TOPOLOGY_NOT_SUPPORTED       \
	((__wum_kernel_NTSTATUS)0xC01E0301)
#define _WUM_KERNEL_STATUS_GRAPHICS_VIDPN_TOPOLOGY_CURRENTLY_NOT_SUPPORTED \
	((__wum_kernel_NTSTATUS)0xC01E0302)
#define _WUM_KERNEL_STATUS_GRAPHICS_INVALID_VIDPN                      \
	((__wum_kernel_NTSTATUS)0xC01E0303)
#define _WUM_KERNEL_STATUS_GRAPHICS_INVALID_VIDEO_PRESENT_SOURCE       \
	((__wum_kernel_NTSTATUS)0xC01E0304)
#define _WUM_KERNEL_STATUS_GRAPHICS_INVALID_VIDEO_PRESENT_TARGET       \
	((__wum_kernel_NTSTATUS)0xC01E0305)
#define _WUM_KERNEL_STATUS_GRAPHICS_VIDPN_MODALITY_NOT_SUPPORTED       \
	((__wum_kernel_NTSTATUS)0xC01E0306)
#define _WUM_KERNEL_STATUS_GRAPHICS_INVALID_VIDPN_SOURCEMODESET        \
	((__wum_kernel_NTSTATUS)0xC01E0308)
#define _WUM_KERNEL_STATUS_GRAPHICS_INVALID_VIDPN_TARGETMODESET        \
	((__wum_kernel_NTSTATUS)0xC01E0309)
#define _WUM_KERNEL_STATUS_GRAPHICS_INVALID_FREQUENCY                  \
	((__wum_kernel_NTSTATUS)0xC01E030A)
#define _WUM_KERNEL_STATUS_GRAPHICS_INVALID_ACTIVE_REGION              \
	((__wum_kernel_NTSTATUS)0xC01E030B)
#define _WUM_KERNEL_STATUS_GRAPHICS_INVALID_TOTAL_REGION               \
	((__wum_kernel_NTSTATUS)0xC01E030C)
#define _WUM_KERNEL_STATUS_GRAPHICS_INVALID_VIDEO_PRESENT_SOURCE_MODE  \
	((__wum_kernel_NTSTATUS)0xC01E0310)
#define _WUM_KERNEL_STATUS_GRAPHICS_INVALID_VIDEO_PRESENT_TARGET_MODE  \
	((__wum_kernel_NTSTATUS)0xC01E0311)
#define _WUM_KERNEL_STATUS_GRAPHICS_PINNED_MODE_MUST_REMAIN_IN_SET     \
	((__wum_kernel_NTSTATUS)0xC01E0312)
#define _WUM_KERNEL_STATUS_GRAPHICS_PATH_ALREADY_IN_TOPOLOGY           \
	((__wum_kernel_NTSTATUS)0xC01E0313)
#define _WUM_KERNEL_STATUS_GRAPHICS_MODE_ALREADY_IN_MODESET            \
	((__wum_kernel_NTSTATUS)0xC01E0314)
#define _WUM_KERNEL_STATUS_GRAPHICS_INVALID_VIDEOPRESENTSOURCESET      \
	((__wum_kernel_NTSTATUS)0xC01E0315)
#define _WUM_KERNEL_STATUS_GRAPHICS_INVALID_VIDEOPRESENTTARGETSET      \
	((__wum_kernel_NTSTATUS)0xC01E0316)
#define _WUM_KERNEL_STATUS_GRAPHICS_SOURCE_ALREADY_IN_SET              \
	((__wum_kernel_NTSTATUS)0xC01E0317)
#define _WUM_KERNEL_STATUS_GRAPHICS_TARGET_ALREADY_IN_SET              \
	((__wum_kernel_NTSTATUS)0xC01E0318)
#define _WUM_KERNEL_STATUS_GRAPHICS_INVALID_VIDPN_PRESENT_PATH         \
	((__wum_kernel_NTSTATUS)0xC01E0319)
#define _WUM_KERNEL_STATUS_GRAPHICS_NO_RECOMMENDED_VIDPN_TOPOLOGY      \
	((__wum_kernel_NTSTATUS)0xC01E031A)
#define _WUM_KERNEL_STATUS_GRAPHICS_INVALID_MONITOR_FREQUENCYRANGESET  \
	((__wum_kernel_NTSTATUS)0xC01E031B)
#define _WUM_KERNEL_STATUS_GRAPHICS_INVALID_MONITOR_FREQUENCYRANGE     \
	((__wum_kernel_NTSTATUS)0xC01E031C)
#define _WUM_KERNEL_STATUS_GRAPHICS_FREQUENCYRANGE_NOT_IN_SET          \
	((__wum_kernel_NTSTATUS)0xC01E031D)
#define _WUM_KERNEL_STATUS_GRAPHICS_FREQUENCYRANGE_ALREADY_IN_SET      \
	((__wum_kernel_NTSTATUS)0xC01E031F)
#define _WUM_KERNEL_STATUS_GRAPHICS_STALE_MODESET                      \
	((__wum_kernel_NTSTATUS)0xC01E0320)
#define _WUM_KERNEL_STATUS_GRAPHICS_INVALID_MONITOR_SOURCEMODESET      \
	((__wum_kernel_NTSTATUS)0xC01E0321)
#define _WUM_KERNEL_STATUS_GRAPHICS_INVALID_MONITOR_SOURCE_MODE        \
	((__wum_kernel_NTSTATUS)0xC01E0322)
#define _WUM_KERNEL_STATUS_GRAPHICS_NO_RECOMMENDED_FUNCTIONAL_VIDPN    \
	((__wum_kernel_NTSTATUS)0xC01E0323)
#define _WUM_KERNEL_STATUS_GRAPHICS_MODE_ID_MUST_BE_UNIQUE             \
	((__wum_kernel_NTSTATUS)0xC01E0324)
#define _WUM_KERNEL_STATUS_GRAPHICS_EMPTY_ADAPTER_MONITOR_MODE_SUPPORT_INTERSECTION \
	((__wum_kernel_NTSTATUS)0xC01E0325)
#define _WUM_KERNEL_STATUS_GRAPHICS_VIDEO_PRESENT_TARGETS_LESS_THAN_SOURCES \
	((__wum_kernel_NTSTATUS)0xC01E0326)
#define _WUM_KERNEL_STATUS_GRAPHICS_PATH_NOT_IN_TOPOLOGY               \
	((__wum_kernel_NTSTATUS)0xC01E0327)
#define _WUM_KERNEL_STATUS_GRAPHICS_ADAPTER_MUST_HAVE_AT_LEAST_ONE_SOURCE \
	((__wum_kernel_NTSTATUS)0xC01E0328)
#define _WUM_KERNEL_STATUS_GRAPHICS_ADAPTER_MUST_HAVE_AT_LEAST_ONE_TARGET \
	((__wum_kernel_NTSTATUS)0xC01E0329)
#define _WUM_KERNEL_STATUS_GRAPHICS_INVALID_MONITORDESCRIPTORSET       \
	((__wum_kernel_NTSTATUS)0xC01E032A)
#define _WUM_KERNEL_STATUS_GRAPHICS_INVALID_MONITORDESCRIPTOR          \
	((__wum_kernel_NTSTATUS)0xC01E032B)
#define _WUM_KERNEL_STATUS_GRAPHICS_MONITORDESCRIPTOR_NOT_IN_SET       \
	((__wum_kernel_NTSTATUS)0xC01E032C)
#define _WUM_KERNEL_STATUS_GRAPHICS_MONITORDESCRIPTOR_ALREADY_IN_SET   \
	((__wum_kernel_NTSTATUS)0xC01E032D)
#define _WUM_KERNEL_STATUS_GRAPHICS_MONITORDESCRIPTOR_ID_MUST_BE_UNIQUE \
	((__wum_kernel_NTSTATUS)0xC01E032E)
#define _WUM_KERNEL_STATUS_GRAPHICS_INVALID_VIDPN_TARGET_SUBSET_TYPE   \
	((__wum_kernel_NTSTATUS)0xC01E032F)
#define _WUM_KERNEL_STATUS_GRAPHICS_RESOURCES_NOT_RELATED              \
	((__wum_kernel_NTSTATUS)0xC01E0330)
#define _WUM_KERNEL_STATUS_GRAPHICS_SOURCE_ID_MUST_BE_UNIQUE           \
	((__wum_kernel_NTSTATUS)0xC01E0331)
#define _WUM_KERNEL_STATUS_GRAPHICS_TARGET_ID_MUST_BE_UNIQUE           \
	((__wum_kernel_NTSTATUS)0xC01E0332)
#define _WUM_KERNEL_STATUS_GRAPHICS_NO_AVAILABLE_VIDPN_TARGET          \
	((__wum_kernel_NTSTATUS)0xC01E0333)
#define _WUM_KERNEL_STATUS_GRAPHICS_MONITOR_COULD_NOT_BE_ASSOCIATED_WITH_ADAPTER \
	((__wum_kernel_NTSTATUS)0xC01E0334)
#define _WUM_KERNEL_STATUS_GRAPHICS_NO_VIDPNMGR                        \
	((__wum_kernel_NTSTATUS)0xC01E0335)
#define _WUM_KERNEL_STATUS_GRAPHICS_NO_ACTIVE_VIDPN                    \
	((__wum_kernel_NTSTATUS)0xC01E0336)
#define _WUM_KERNEL_STATUS_GRAPHICS_STALE_VIDPN_TOPOLOGY               \
	((__wum_kernel_NTSTATUS)0xC01E0337)
#define _WUM_KERNEL_STATUS_GRAPHICS_MONITOR_NOT_CONNECTED              \
	((__wum_kernel_NTSTATUS)0xC01E0338)
#define _WUM_KERNEL_STATUS_GRAPHICS_SOURCE_NOT_IN_TOPOLOGY             \
	((__wum_kernel_NTSTATUS)0xC01E0339)
#define _WUM_KERNEL_STATUS_GRAPHICS_INVALID_PRIMARYSURFACE_SIZE        \
	((__wum_kernel_NTSTATUS)0xC01E033A)
#define _WUM_KERNEL_STATUS_GRAPHICS_INVALID_VISIBLEREGION_SIZE         \
	((__wum_kernel_NTSTATUS)0xC01E033B)
#define _WUM_KERNEL_STATUS_GRAPHICS_INVALID_STRIDE                     \
	((__wum_kernel_NTSTATUS)0xC01E033C)
#define _WUM_KERNEL_STATUS_GRAPHICS_INVALID_PIXELFORMAT                \
	((__wum_kernel_NTSTATUS)0xC01E033D)
#define _WUM_KERNEL_STATUS_GRAPHICS_INVALID_COLORBASIS                 \
	((__wum_kernel_NTSTATUS)0xC01E033E)
#define _WUM_KERNEL_STATUS_GRAPHICS_INVALID_PIXELVALUEACCESSMODE       \
	((__wum_kernel_NTSTATUS)0xC01E033F)
#define _WUM_KERNEL_STATUS_GRAPHICS_TARGET_NOT_IN_TOPOLOGY             \
	((__wum_kernel_NTSTATUS)0xC01E0340)
#define _WUM_KERNEL_STATUS_GRAPHICS_NO_DISPLAY_MODE_MANAGEMENT_SUPPORT \
	((__wum_kernel_NTSTATUS)0xC01E0341)
#define _WUM_KERNEL_STATUS_GRAPHICS_VIDPN_SOURCE_IN_USE                \
	((__wum_kernel_NTSTATUS)0xC01E0342)
#define _WUM_KERNEL_STATUS_GRAPHICS_CANT_ACCESS_ACTIVE_VIDPN           \
	((__wum_kernel_NTSTATUS)0xC01E0343)
#define _WUM_KERNEL_STATUS_GRAPHICS_INVALID_PATH_IMPORTANCE_ORDINAL    \
	((__wum_kernel_NTSTATUS)0xC01E0344)
#define _WUM_KERNEL_STATUS_GRAPHICS_INVALID_PATH_CONTENT_GEOMETRY_TRANSFORMATION \
	((__wum_kernel_NTSTATUS)0xC01E0345)
#define _WUM_KERNEL_STATUS_GRAPHICS_PATH_CONTENT_GEOMETRY_TRANSFORMATION_NOT_SUPPORTED \
	((__wum_kernel_NTSTATUS)0xC01E0346)
#define _WUM_KERNEL_STATUS_GRAPHICS_INVALID_GAMMA_RAMP                 \
	((__wum_kernel_NTSTATUS)0xC01E0347)
#define _WUM_KERNEL_STATUS_GRAPHICS_GAMMA_RAMP_NOT_SUPPORTED           \
	((__wum_kernel_NTSTATUS)0xC01E0348)
#define _WUM_KERNEL_STATUS_GRAPHICS_MULTISAMPLING_NOT_SUPPORTED        \
	((__wum_kernel_NTSTATUS)0xC01E0349)
#define _WUM_KERNEL_STATUS_GRAPHICS_MODE_NOT_IN_MODESET                \
	((__wum_kernel_NTSTATUS)0xC01E034A)
#define _WUM_KERNEL_STATUS_GRAPHICS_INVALID_VIDPN_TOPOLOGY_RECOMMENDATION_REASON \
	((__wum_kernel_NTSTATUS)0xC01E034D)
#define _WUM_KERNEL_STATUS_GRAPHICS_INVALID_PATH_CONTENT_TYPE          \
	((__wum_kernel_NTSTATUS)0xC01E034E)
#define _WUM_KERNEL_STATUS_GRAPHICS_INVALID_COPYPROTECTION_TYPE        \
	((__wum_kernel_NTSTATUS)0xC01E034F)
#define _WUM_KERNEL_STATUS_GRAPHICS_UNASSIGNED_MODESET_ALREADY_EXISTS  \
	((__wum_kernel_NTSTATUS)0xC01E0350)
#define _WUM_KERNEL_STATUS_GRAPHICS_INVALID_SCANLINE_ORDERING          \
	((__wum_kernel_NTSTATUS)0xC01E0352)
#define _WUM_KERNEL_STATUS_GRAPHICS_TOPOLOGY_CHANGES_NOT_ALLOWED       \
	((__wum_kernel_NTSTATUS)0xC01E0353)
#define _WUM_KERNEL_STATUS_GRAPHICS_NO_AVAILABLE_IMPORTANCE_ORDINALS   \
	((__wum_kernel_NTSTATUS)0xC01E0354)
#define _WUM_KERNEL_STATUS_GRAPHICS_INCOMPATIBLE_PRIVATE_FORMAT        \
	((__wum_kernel_NTSTATUS)0xC01E0355)
#define _WUM_KERNEL_STATUS_GRAPHICS_INVALID_MODE_PRUNING_ALGORITHM     \
	((__wum_kernel_NTSTATUS)0xC01E0356)
#define _WUM_KERNEL_STATUS_GRAPHICS_INVALID_MONITOR_CAPABILITY_ORIGIN  \
	((__wum_kernel_NTSTATUS)0xC01E0357)
#define _WUM_KERNEL_STATUS_GRAPHICS_INVALID_MONITOR_FREQUENCYRANGE_CONSTRAINT \
	((__wum_kernel_NTSTATUS)0xC01E0358)
#define _WUM_KERNEL_STATUS_GRAPHICS_MAX_NUM_PATHS_REACHED              \
	((__wum_kernel_NTSTATUS)0xC01E0359)
#define _WUM_KERNEL_STATUS_GRAPHICS_CANCEL_VIDPN_TOPOLOGY_AUGMENTATION \
	((__wum_kernel_NTSTATUS)0xC01E035A)
#define _WUM_KERNEL_STATUS_GRAPHICS_INVALID_CLIENT_TYPE                \
	((__wum_kernel_NTSTATUS)0xC01E035B)
#define _WUM_KERNEL_STATUS_GRAPHICS_CLIENTVIDPN_NOT_SET                \
	((__wum_kernel_NTSTATUS)0xC01E035C)
#define _WUM_KERNEL_STATUS_GRAPHICS_SPECIFIED_CHILD_ALREADY_CONNECTED  \
	((__wum_kernel_NTSTATUS)0xC01E0400)
#define _WUM_KERNEL_STATUS_GRAPHICS_CHILD_DESCRIPTOR_NOT_SUPPORTED     \
	((__wum_kernel_NTSTATUS)0xC01E0401)
#define _WUM_KERNEL_STATUS_GRAPHICS_NOT_A_LINKED_ADAPTER               \
	((__wum_kernel_NTSTATUS)0xC01E0430)
#define _WUM_KERNEL_STATUS_GRAPHICS_LEADLINK_NOT_ENUMERATED            \
	((__wum_kernel_NTSTATUS)0xC01E0431)
#define _WUM_KERNEL_STATUS_GRAPHICS_CHAINLINKS_NOT_ENUMERATED          \
	((__wum_kernel_NTSTATUS)0xC01E0432)
#define _WUM_KERNEL_STATUS_GRAPHICS_ADAPTER_CHAIN_NOT_READY            \
	((__wum_kernel_NTSTATUS)0xC01E0433)
#define _WUM_KERNEL_STATUS_GRAPHICS_CHAINLINKS_NOT_STARTED             \
	((__wum_kernel_NTSTATUS)0xC01E0434)
#define _WUM_KERNEL_STATUS_GRAPHICS_CHAINLINKS_NOT_POWERED_ON          \
	((__wum_kernel_NTSTATUS)0xC01E0435)
#define _WUM_KERNEL_STATUS_GRAPHICS_INCONSISTENT_DEVICE_LINK_STATE     \
	((__wum_kernel_NTSTATUS)0xC01E0436)
#define _WUM_KERNEL_STATUS_GRAPHICS_NOT_POST_DEVICE_DRIVER             \
	((__wum_kernel_NTSTATUS)0xC01E0438)
#define _WUM_KERNEL_STATUS_GRAPHICS_ADAPTER_ACCESS_NOT_EXCLUDED        \
	((__wum_kernel_NTSTATUS)0xC01E043B)
#define _WUM_KERNEL_STATUS_GRAPHICS_OPM_NOT_SUPPORTED                  \
	((__wum_kernel_NTSTATUS)0xC01E0500)
#define _WUM_KERNEL_STATUS_GRAPHICS_COPP_NOT_SUPPORTED                 \
	((__wum_kernel_NTSTATUS)0xC01E0501)
#define _WUM_KERNEL_STATUS_GRAPHICS_UAB_NOT_SUPPORTED                  \
	((__wum_kernel_NTSTATUS)0xC01E0502)
#define _WUM_KERNEL_STATUS_GRAPHICS_OPM_INVALID_ENCRYPTED_PARAMETERS   \
	((__wum_kernel_NTSTATUS)0xC01E0503)
#define _WUM_KERNEL_STATUS_GRAPHICS_OPM_PARAMETER_ARRAY_TOO_SMALL      \
	((__wum_kernel_NTSTATUS)0xC01E0504)
#define _WUM_KERNEL_STATUS_GRAPHICS_OPM_NO_PROTECTED_OUTPUTS_EXIST     \
	((__wum_kernel_NTSTATUS)0xC01E0505)
#define _WUM_KERNEL_STATUS_GRAPHICS_PVP_NO_DISPLAY_DEVICE_CORRESPONDS_TO_NAME \
	((__wum_kernel_NTSTATUS)0xC01E0506)
#define _WUM_KERNEL_STATUS_GRAPHICS_PVP_DISPLAY_DEVICE_NOT_ATTACHED_TO_DESKTOP \
	((__wum_kernel_NTSTATUS)0xC01E0507)
#define _WUM_KERNEL_STATUS_GRAPHICS_PVP_MIRRORING_DEVICES_NOT_SUPPORTED \
	((__wum_kernel_NTSTATUS)0xC01E0508)
#define _WUM_KERNEL_STATUS_GRAPHICS_OPM_INVALID_POINTER                \
	((__wum_kernel_NTSTATUS)0xC01E050A)
#define _WUM_KERNEL_STATUS_GRAPHICS_OPM_INTERNAL_ERROR                 \
	((__wum_kernel_NTSTATUS)0xC01E050B)
#define _WUM_KERNEL_STATUS_GRAPHICS_OPM_INVALID_HANDLE                 \
	((__wum_kernel_NTSTATUS)0xC01E050C)
#define _WUM_KERNEL_STATUS_GRAPHICS_PVP_NO_MONITORS_CORRESPOND_TO_DISPLAY_DEVICE \
	((__wum_kernel_NTSTATUS)0xC01E050D)
#define _WUM_KERNEL_STATUS_GRAPHICS_PVP_INVALID_CERTIFICATE_LENGTH     \
	((__wum_kernel_NTSTATUS)0xC01E050E)
#define _WUM_KERNEL_STATUS_GRAPHICS_OPM_SPANNING_MODE_ENABLED          \
	((__wum_kernel_NTSTATUS)0xC01E050F)
#define _WUM_KERNEL_STATUS_GRAPHICS_OPM_THEATER_MODE_ENABLED           \
	((__wum_kernel_NTSTATUS)0xC01E0510)
#define _WUM_KERNEL_STATUS_GRAPHICS_PVP_HFS_FAILED                     \
	((__wum_kernel_NTSTATUS)0xC01E0511)
#define _WUM_KERNEL_STATUS_GRAPHICS_OPM_INVALID_SRM                    \
	((__wum_kernel_NTSTATUS)0xC01E0512)
#define _WUM_KERNEL_STATUS_GRAPHICS_OPM_OUTPUT_DOES_NOT_SUPPORT_HDCP   \
	((__wum_kernel_NTSTATUS)0xC01E0513)
#define _WUM_KERNEL_STATUS_GRAPHICS_OPM_OUTPUT_DOES_NOT_SUPPORT_ACP    \
	((__wum_kernel_NTSTATUS)0xC01E0514)
#define _WUM_KERNEL_STATUS_GRAPHICS_OPM_OUTPUT_DOES_NOT_SUPPORT_CGMSA  \
	((__wum_kernel_NTSTATUS)0xC01E0515)
#define _WUM_KERNEL_STATUS_GRAPHICS_OPM_HDCP_SRM_NEVER_SET             \
	((__wum_kernel_NTSTATUS)0xC01E0516)
#define _WUM_KERNEL_STATUS_GRAPHICS_OPM_RESOLUTION_TOO_HIGH            \
	((__wum_kernel_NTSTATUS)0xC01E0517)
#define _WUM_KERNEL_STATUS_GRAPHICS_OPM_ALL_HDCP_HARDWARE_ALREADY_IN_USE \
	((__wum_kernel_NTSTATUS)0xC01E0518)
#define _WUM_KERNEL_STATUS_GRAPHICS_OPM_PROTECTED_OUTPUT_NO_LONGER_EXISTS \
	((__wum_kernel_NTSTATUS)0xC01E051A)
#define _WUM_KERNEL_STATUS_GRAPHICS_OPM_SESSION_TYPE_CHANGE_IN_PROGRESS \
	((__wum_kernel_NTSTATUS)0xC01E051B)
#define _WUM_KERNEL_STATUS_GRAPHICS_OPM_PROTECTED_OUTPUT_DOES_NOT_HAVE_COPP_SEMANTICS \
	((__wum_kernel_NTSTATUS)0xC01E051C)
#define _WUM_KERNEL_STATUS_GRAPHICS_OPM_INVALID_INFORMATION_REQUEST    \
	((__wum_kernel_NTSTATUS)0xC01E051D)
#define _WUM_KERNEL_STATUS_GRAPHICS_OPM_DRIVER_INTERNAL_ERROR          \
	((__wum_kernel_NTSTATUS)0xC01E051E)
#define _WUM_KERNEL_STATUS_GRAPHICS_OPM_PROTECTED_OUTPUT_DOES_NOT_HAVE_OPM_SEMANTICS \
	((__wum_kernel_NTSTATUS)0xC01E051F)
#define _WUM_KERNEL_STATUS_GRAPHICS_OPM_SIGNALING_NOT_SUPPORTED        \
	((__wum_kernel_NTSTATUS)0xC01E0520)
#define _WUM_KERNEL_STATUS_GRAPHICS_OPM_INVALID_CONFIGURATION_REQUEST  \
	((__wum_kernel_NTSTATUS)0xC01E0521)
#define _WUM_KERNEL_STATUS_GRAPHICS_I2C_NOT_SUPPORTED                  \
	((__wum_kernel_NTSTATUS)0xC01E0580)
#define _WUM_KERNEL_STATUS_GRAPHICS_I2C_DEVICE_DOES_NOT_EXIST          \
	((__wum_kernel_NTSTATUS)0xC01E0581)
#define _WUM_KERNEL_STATUS_GRAPHICS_I2C_ERROR_TRANSMITTING_DATA        \
	((__wum_kernel_NTSTATUS)0xC01E0582)
#define _WUM_KERNEL_STATUS_GRAPHICS_I2C_ERROR_RECEIVING_DATA           \
	((__wum_kernel_NTSTATUS)0xC01E0583)
#define _WUM_KERNEL_STATUS_GRAPHICS_DDCCI_VCP_NOT_SUPPORTED            \
	((__wum_kernel_NTSTATUS)0xC01E0584)
#define _WUM_KERNEL_STATUS_GRAPHICS_DDCCI_INVALID_DATA                 \
	((__wum_kernel_NTSTATUS)0xC01E0585)
#define _WUM_KERNEL_STATUS_GRAPHICS_DDCCI_MONITOR_RETURNED_INVALID_TIMING_STATUS_BYTE \
	((__wum_kernel_NTSTATUS)0xC01E0586)
#define _WUM_KERNEL_STATUS_GRAPHICS_DDCCI_INVALID_CAPABILITIES_STRING  \
	((__wum_kernel_NTSTATUS)0xC01E0587)
#define _WUM_KERNEL_STATUS_GRAPHICS_MCA_INTERNAL_ERROR                 \
	((__wum_kernel_NTSTATUS)0xC01E0588)
#define _WUM_KERNEL_STATUS_GRAPHICS_DDCCI_INVALID_MESSAGE_COMMAND      \
	((__wum_kernel_NTSTATUS)0xC01E0589)
#define _WUM_KERNEL_STATUS_GRAPHICS_DDCCI_INVALID_MESSAGE_LENGTH       \
	((__wum_kernel_NTSTATUS)0xC01E058A)
#define _WUM_KERNEL_STATUS_GRAPHICS_DDCCI_INVALID_MESSAGE_CHECKSUM     \
	((__wum_kernel_NTSTATUS)0xC01E058B)
#define _WUM_KERNEL_STATUS_GRAPHICS_INVALID_PHYSICAL_MONITOR_HANDLE    \
	((__wum_kernel_NTSTATUS)0xC01E058C)
#define _WUM_KERNEL_STATUS_GRAPHICS_MONITOR_NO_LONGER_EXISTS           \
	((__wum_kernel_NTSTATUS)0xC01E058D)
#define _WUM_KERNEL_STATUS_GRAPHICS_ONLY_CONSOLE_SESSION_SUPPORTED     \
	((__wum_kernel_NTSTATUS)0xC01E05E0)
#define _WUM_KERNEL_STATUS_GRAPHICS_NO_DISPLAY_DEVICE_CORRESPONDS_TO_NAME \
	((__wum_kernel_NTSTATUS)0xC01E05E1)
#define _WUM_KERNEL_STATUS_GRAPHICS_DISPLAY_DEVICE_NOT_ATTACHED_TO_DESKTOP \
	((__wum_kernel_NTSTATUS)0xC01E05E2)
#define _WUM_KERNEL_STATUS_GRAPHICS_MIRRORING_DEVICES_NOT_SUPPORTED    \
	((__wum_kernel_NTSTATUS)0xC01E05E3)
#define _WUM_KERNEL_STATUS_GRAPHICS_INVALID_POINTER                    \
	((__wum_kernel_NTSTATUS)0xC01E05E4)
#define _WUM_KERNEL_STATUS_GRAPHICS_NO_MONITORS_CORRESPOND_TO_DISPLAY_DEVICE \
	((__wum_kernel_NTSTATUS)0xC01E05E5)
#define _WUM_KERNEL_STATUS_GRAPHICS_PARAMETER_ARRAY_TOO_SMALL          \
	((__wum_kernel_NTSTATUS)0xC01E05E6)
#define _WUM_KERNEL_STATUS_GRAPHICS_INTERNAL_ERROR                     \
	((__wum_kernel_NTSTATUS)0xC01E05E7)
#define _WUM_KERNEL_STATUS_GRAPHICS_SESSION_TYPE_CHANGE_IN_PROGRESS    \
	((__wum_kernel_NTSTATUS)0xC01E05E8)
#define _WUM_KERNEL_STATUS_FVE_LOCKED_VOLUME                           \
	((__wum_kernel_NTSTATUS)0xC0210000)
#define _WUM_KERNEL_STATUS_FVE_NOT_ENCRYPTED                           \
	((__wum_kernel_NTSTATUS)0xC0210001)
#define _WUM_KERNEL_STATUS_FVE_BAD_INFORMATION                         \
	((__wum_kernel_NTSTATUS)0xC0210002)
#define _WUM_KERNEL_STATUS_FVE_TOO_SMALL                               \
	((__wum_kernel_NTSTATUS)0xC0210003)
#define _WUM_KERNEL_STATUS_FVE_FAILED_WRONG_FS                         \
	((__wum_kernel_NTSTATUS)0xC0210004)
#define _WUM_KERNEL_STATUS_FVE_FAILED_BAD_FS                           \
	((__wum_kernel_NTSTATUS)0xC0210005)
#define _WUM_KERNEL_STATUS_FVE_FS_NOT_EXTENDED                         \
	((__wum_kernel_NTSTATUS)0xC0210006)
#define _WUM_KERNEL_STATUS_FVE_FS_MOUNTED                              \
	((__wum_kernel_NTSTATUS)0xC0210007)
#define _WUM_KERNEL_STATUS_FVE_NO_LICENSE                              \
	((__wum_kernel_NTSTATUS)0xC0210008)
#define _WUM_KERNEL_STATUS_FVE_ACTION_NOT_ALLOWED                      \
	((__wum_kernel_NTSTATUS)0xC0210009)
#define _WUM_KERNEL_STATUS_FVE_BAD_DATA                                \
	((__wum_kernel_NTSTATUS)0xC021000A)
#define _WUM_KERNEL_STATUS_FVE_VOLUME_NOT_BOUND                        \
	((__wum_kernel_NTSTATUS)0xC021000B)
#define _WUM_KERNEL_STATUS_FVE_NOT_DATA_VOLUME                         \
	((__wum_kernel_NTSTATUS)0xC021000C)
#define _WUM_KERNEL_STATUS_FVE_CONV_READ_ERROR                         \
	((__wum_kernel_NTSTATUS)0xC021000D)
#define _WUM_KERNEL_STATUS_FVE_CONV_WRITE_ERROR                        \
	((__wum_kernel_NTSTATUS)0xC021000E)
#define _WUM_KERNEL_STATUS_FVE_OVERLAPPED_UPDATE                       \
	((__wum_kernel_NTSTATUS)0xC021000F)
#define _WUM_KERNEL_STATUS_FVE_FAILED_SECTOR_SIZE                      \
	((__wum_kernel_NTSTATUS)0xC0210010)
#define _WUM_KERNEL_STATUS_FVE_FAILED_AUTHENTICATION                   \
	((__wum_kernel_NTSTATUS)0xC0210011)
#define _WUM_KERNEL_STATUS_FVE_NOT_OS_VOLUME                           \
	((__wum_kernel_NTSTATUS)0xC0210012)
#define _WUM_KERNEL_STATUS_FVE_KEYFILE_NOT_FOUND                       \
	((__wum_kernel_NTSTATUS)0xC0210013)
#define _WUM_KERNEL_STATUS_FVE_KEYFILE_INVALID                         \
	((__wum_kernel_NTSTATUS)0xC0210014)
#define _WUM_KERNEL_STATUS_FVE_KEYFILE_NO_VMK                          \
	((__wum_kernel_NTSTATUS)0xC0210015)
#define _WUM_KERNEL_STATUS_FVE_TPM_DISABLED                            \
	((__wum_kernel_NTSTATUS)0xC0210016)
#define _WUM_KERNEL_STATUS_FVE_TPM_SRK_AUTH_NOT_ZERO                   \
	((__wum_kernel_NTSTATUS)0xC0210017)
#define _WUM_KERNEL_STATUS_FVE_TPM_INVALID_PCR                         \
	((__wum_kernel_NTSTATUS)0xC0210018)
#define _WUM_KERNEL_STATUS_FVE_TPM_NO_VMK                              \
	((__wum_kernel_NTSTATUS)0xC0210019)
#define _WUM_KERNEL_STATUS_FVE_PIN_INVALID                             \
	((__wum_kernel_NTSTATUS)0xC021001A)
#define _WUM_KERNEL_STATUS_FVE_AUTH_INVALID_APPLICATION                \
	((__wum_kernel_NTSTATUS)0xC021001B)
#define _WUM_KERNEL_STATUS_FVE_AUTH_INVALID_CONFIG                     \
	((__wum_kernel_NTSTATUS)0xC021001C)
#define _WUM_KERNEL_STATUS_FVE_DEBUGGER_ENABLED                        \
	((__wum_kernel_NTSTATUS)0xC021001D)
#define _WUM_KERNEL_STATUS_FVE_DRY_RUN_FAILED                          \
	((__wum_kernel_NTSTATUS)0xC021001E)
#define _WUM_KERNEL_STATUS_FVE_BAD_METADATA_POINTER                    \
	((__wum_kernel_NTSTATUS)0xC021001F)
#define _WUM_KERNEL_STATUS_FVE_OLD_METADATA_COPY                       \
	((__wum_kernel_NTSTATUS)0xC0210020)
#define _WUM_KERNEL_STATUS_FVE_REBOOT_REQUIRED                         \
	((__wum_kernel_NTSTATUS)0xC0210021)
#define _WUM_KERNEL_STATUS_FVE_RAW_ACCESS                              \
	((__wum_kernel_NTSTATUS)0xC0210022)
#define _WUM_KERNEL_STATUS_FVE_RAW_BLOCKED                             \
	((__wum_kernel_NTSTATUS)0xC0210023)
#define _WUM_KERNEL_STATUS_FVE_NO_FEATURE_LICENSE                      \
	((__wum_kernel_NTSTATUS)0xC0210026)
#define _WUM_KERNEL_STATUS_FVE_POLICY_USER_DISABLE_RDV_NOT_ALLOWED     \
	((__wum_kernel_NTSTATUS)0xC0210027)
#define _WUM_KERNEL_STATUS_FVE_CONV_RECOVERY_FAILED                    \
	((__wum_kernel_NTSTATUS)0xC0210028)
#define _WUM_KERNEL_STATUS_FVE_VIRTUALIZED_SPACE_TOO_BIG               \
	((__wum_kernel_NTSTATUS)0xC0210029)
#define _WUM_KERNEL_STATUS_FVE_VOLUME_TOO_SMALL                        \
	((__wum_kernel_NTSTATUS)0xC0210030)
#define _WUM_KERNEL_STATUS_FWP_CALLOUT_NOT_FOUND                       \
	((__wum_kernel_NTSTATUS)0xC0220001)
#define _WUM_KERNEL_STATUS_FWP_CONDITION_NOT_FOUND                     \
	((__wum_kernel_NTSTATUS)0xC0220002)
#define _WUM_KERNEL_STATUS_FWP_FILTER_NOT_FOUND                        \
	((__wum_kernel_NTSTATUS)0xC0220003)
#define _WUM_KERNEL_STATUS_FWP_LAYER_NOT_FOUND                         \
	((__wum_kernel_NTSTATUS)0xC0220004)
#define _WUM_KERNEL_STATUS_FWP_PROVIDER_NOT_FOUND                      \
	((__wum_kernel_NTSTATUS)0xC0220005)
#define _WUM_KERNEL_STATUS_FWP_PROVIDER_CONTEXT_NOT_FOUND              \
	((__wum_kernel_NTSTATUS)0xC0220006)
#define _WUM_KERNEL_STATUS_FWP_SUBLAYER_NOT_FOUND                      \
	((__wum_kernel_NTSTATUS)0xC0220007)
#define _WUM_KERNEL_STATUS_FWP_NOT_FOUND                               \
	((__wum_kernel_NTSTATUS)0xC0220008)
#define _WUM_KERNEL_STATUS_FWP_ALREADY_EXISTS                          \
	((__wum_kernel_NTSTATUS)0xC0220009)
#define _WUM_KERNEL_STATUS_FWP_IN_USE                                  \
	((__wum_kernel_NTSTATUS)0xC022000A)
#define _WUM_KERNEL_STATUS_FWP_DYNAMIC_SESSION_IN_PROGRESS             \
	((__wum_kernel_NTSTATUS)0xC022000B)
#define _WUM_KERNEL_STATUS_FWP_WRONG_SESSION                           \
	((__wum_kernel_NTSTATUS)0xC022000C)
#define _WUM_KERNEL_STATUS_FWP_NO_TXN_IN_PROGRESS                      \
	((__wum_kernel_NTSTATUS)0xC022000D)
#define _WUM_KERNEL_STATUS_FWP_TXN_IN_PROGRESS                         \
	((__wum_kernel_NTSTATUS)0xC022000E)
#define _WUM_KERNEL_STATUS_FWP_TXN_ABORTED                             \
	((__wum_kernel_NTSTATUS)0xC022000F)
#define _WUM_KERNEL_STATUS_FWP_SESSION_ABORTED                         \
	((__wum_kernel_NTSTATUS)0xC0220010)
#define _WUM_KERNEL_STATUS_FWP_INCOMPATIBLE_TXN                        \
	((__wum_kernel_NTSTATUS)0xC0220011)
#define _WUM_KERNEL_STATUS_FWP_TIMEOUT                                 \
	((__wum_kernel_NTSTATUS)0xC0220012)
#define _WUM_KERNEL_STATUS_FWP_NET_EVENTS_DISABLED                     \
	((__wum_kernel_NTSTATUS)0xC0220013)
#define _WUM_KERNEL_STATUS_FWP_INCOMPATIBLE_LAYER                      \
	((__wum_kernel_NTSTATUS)0xC0220014)
#define _WUM_KERNEL_STATUS_FWP_KM_CLIENTS_ONLY                         \
	((__wum_kernel_NTSTATUS)0xC0220015)
#define _WUM_KERNEL_STATUS_FWP_LIFETIME_MISMATCH                       \
	((__wum_kernel_NTSTATUS)0xC0220016)
#define _WUM_KERNEL_STATUS_FWP_BUILTIN_OBJECT                          \
	((__wum_kernel_NTSTATUS)0xC0220017)
#define _WUM_KERNEL_STATUS_FWP_TOO_MANY_BOOTTIME_FILTERS               \
	((__wum_kernel_NTSTATUS)0xC0220018)
#define _WUM_KERNEL_STATUS_FWP_TOO_MANY_CALLOUTS                       \
	((__wum_kernel_NTSTATUS)0xC0220018)
#define _WUM_KERNEL_STATUS_FWP_NOTIFICATION_DROPPED                    \
	((__wum_kernel_NTSTATUS)0xC0220019)
#define _WUM_KERNEL_STATUS_FWP_TRAFFIC_MISMATCH                        \
	((__wum_kernel_NTSTATUS)0xC022001A)
#define _WUM_KERNEL_STATUS_FWP_INCOMPATIBLE_SA_STATE                   \
	((__wum_kernel_NTSTATUS)0xC022001B)
#define _WUM_KERNEL_STATUS_FWP_NULL_POINTER                            \
	((__wum_kernel_NTSTATUS)0xC022001C)
#define _WUM_KERNEL_STATUS_FWP_INVALID_ENUMERATOR                      \
	((__wum_kernel_NTSTATUS)0xC022001D)
#define _WUM_KERNEL_STATUS_FWP_INVALID_FLAGS                           \
	((__wum_kernel_NTSTATUS)0xC022001E)
#define _WUM_KERNEL_STATUS_FWP_INVALID_NET_MASK                        \
	((__wum_kernel_NTSTATUS)0xC022001F)
#define _WUM_KERNEL_STATUS_FWP_INVALID_RANGE                           \
	((__wum_kernel_NTSTATUS)0xC0220020)
#define _WUM_KERNEL_STATUS_FWP_INVALID_INTERVAL                        \
	((__wum_kernel_NTSTATUS)0xC0220021)
#define _WUM_KERNEL_STATUS_FWP_ZERO_LENGTH_ARRAY                       \
	((__wum_kernel_NTSTATUS)0xC0220022)
#define _WUM_KERNEL_STATUS_FWP_NULL_DISPLAY_NAME                       \
	((__wum_kernel_NTSTATUS)0xC0220023)
#define _WUM_KERNEL_STATUS_FWP_INVALID_ACTION_TYPE                     \
	((__wum_kernel_NTSTATUS)0xC0220024)
#define _WUM_KERNEL_STATUS_FWP_INVALID_WEIGHT                          \
	((__wum_kernel_NTSTATUS)0xC0220025)
#define _WUM_KERNEL_STATUS_FWP_MATCH_TYPE_MISMATCH                     \
	((__wum_kernel_NTSTATUS)0xC0220026)
#define _WUM_KERNEL_STATUS_FWP_TYPE_MISMATCH                           \
	((__wum_kernel_NTSTATUS)0xC0220027)
#define _WUM_KERNEL_STATUS_FWP_OUT_OF_BOUNDS                           \
	((__wum_kernel_NTSTATUS)0xC0220028)
#define _WUM_KERNEL_STATUS_FWP_RESERVED                                \
	((__wum_kernel_NTSTATUS)0xC0220029)
#define _WUM_KERNEL_STATUS_FWP_DUPLICATE_CONDITION                     \
	((__wum_kernel_NTSTATUS)0xC022002A)
#define _WUM_KERNEL_STATUS_FWP_DUPLICATE_KEYMOD                        \
	((__wum_kernel_NTSTATUS)0xC022002B)
#define _WUM_KERNEL_STATUS_FWP_ACTION_INCOMPATIBLE_WITH_LAYER          \
	((__wum_kernel_NTSTATUS)0xC022002C)
#define _WUM_KERNEL_STATUS_FWP_ACTION_INCOMPATIBLE_WITH_SUBLAYER       \
	((__wum_kernel_NTSTATUS)0xC022002D)
#define _WUM_KERNEL_STATUS_FWP_CONTEXT_INCOMPATIBLE_WITH_LAYER         \
	((__wum_kernel_NTSTATUS)0xC022002E)
#define _WUM_KERNEL_STATUS_FWP_CONTEXT_INCOMPATIBLE_WITH_CALLOUT       \
	((__wum_kernel_NTSTATUS)0xC022002F)
#define _WUM_KERNEL_STATUS_FWP_INCOMPATIBLE_AUTH_METHOD                \
	((__wum_kernel_NTSTATUS)0xC0220030)
#define _WUM_KERNEL_STATUS_FWP_INCOMPATIBLE_DH_GROUP                   \
	((__wum_kernel_NTSTATUS)0xC0220031)
#define _WUM_KERNEL_STATUS_FWP_EM_NOT_SUPPORTED                        \
	((__wum_kernel_NTSTATUS)0xC0220032)
#define _WUM_KERNEL_STATUS_FWP_NEVER_MATCH                             \
	((__wum_kernel_NTSTATUS)0xC0220033)
#define _WUM_KERNEL_STATUS_FWP_PROVIDER_CONTEXT_MISMATCH               \
	((__wum_kernel_NTSTATUS)0xC0220034)
#define _WUM_KERNEL_STATUS_FWP_INVALID_PARAMETER                       \
	((__wum_kernel_NTSTATUS)0xC0220035)
#define _WUM_KERNEL_STATUS_FWP_TOO_MANY_SUBLAYERS                      \
	((__wum_kernel_NTSTATUS)0xC0220036)
#define _WUM_KERNEL_STATUS_FWP_CALLOUT_NOTIFICATION_FAILED             \
	((__wum_kernel_NTSTATUS)0xC0220037)
#define _WUM_KERNEL_STATUS_FWP_INCOMPATIBLE_AUTH_CONFIG                \
	((__wum_kernel_NTSTATUS)0xC0220038)
#define _WUM_KERNEL_STATUS_FWP_INCOMPATIBLE_CIPHER_CONFIG              \
	((__wum_kernel_NTSTATUS)0xC0220039)
#define _WUM_KERNEL_STATUS_FWP_DUPLICATE_AUTH_METHOD                   \
	((__wum_kernel_NTSTATUS)0xC022003C)
#define _WUM_KERNEL_STATUS_FWP_TCPIP_NOT_READY                         \
	((__wum_kernel_NTSTATUS)0xC0220100)
#define _WUM_KERNEL_STATUS_FWP_INJECT_HANDLE_CLOSING                   \
	((__wum_kernel_NTSTATUS)0xC0220101)
#define _WUM_KERNEL_STATUS_FWP_INJECT_HANDLE_STALE                     \
	((__wum_kernel_NTSTATUS)0xC0220102)
#define _WUM_KERNEL_STATUS_FWP_CANNOT_PEND                             \
	((__wum_kernel_NTSTATUS)0xC0220103)
#define _WUM_KERNEL_STATUS_NDIS_CLOSING                                \
	((__wum_kernel_NTSTATUS)0xC0230002)
#define _WUM_KERNEL_STATUS_NDIS_BAD_VERSION                            \
	((__wum_kernel_NTSTATUS)0xC0230004)
#define _WUM_KERNEL_STATUS_NDIS_BAD_CHARACTERISTICS                    \
	((__wum_kernel_NTSTATUS)0xC0230005)
#define _WUM_KERNEL_STATUS_NDIS_ADAPTER_NOT_FOUND                      \
	((__wum_kernel_NTSTATUS)0xC0230006)
#define _WUM_KERNEL_STATUS_NDIS_OPEN_FAILED                            \
	((__wum_kernel_NTSTATUS)0xC0230007)
#define _WUM_KERNEL_STATUS_NDIS_DEVICE_FAILED                          \
	((__wum_kernel_NTSTATUS)0xC0230008)
#define _WUM_KERNEL_STATUS_NDIS_MULTICAST_FULL                         \
	((__wum_kernel_NTSTATUS)0xC0230009)
#define _WUM_KERNEL_STATUS_NDIS_MULTICAST_EXISTS                       \
	((__wum_kernel_NTSTATUS)0xC023000A)
#define _WUM_KERNEL_STATUS_NDIS_MULTICAST_NOT_FOUND                    \
	((__wum_kernel_NTSTATUS)0xC023000B)
#define _WUM_KERNEL_STATUS_NDIS_REQUEST_ABORTED                        \
	((__wum_kernel_NTSTATUS)0xC023000C)
#define _WUM_KERNEL_STATUS_NDIS_RESET_IN_PROGRESS                      \
	((__wum_kernel_NTSTATUS)0xC023000D)
#define _WUM_KERNEL_STATUS_NDIS_INVALID_PACKET                         \
	((__wum_kernel_NTSTATUS)0xC023000F)
#define _WUM_KERNEL_STATUS_NDIS_INVALID_DEVICE_REQUEST                 \
	((__wum_kernel_NTSTATUS)0xC0230010)
#define _WUM_KERNEL_STATUS_NDIS_ADAPTER_NOT_READY                      \
	((__wum_kernel_NTSTATUS)0xC0230011)
#define _WUM_KERNEL_STATUS_NDIS_INVALID_LENGTH                         \
	((__wum_kernel_NTSTATUS)0xC0230014)
#define _WUM_KERNEL_STATUS_NDIS_INVALID_DATA                           \
	((__wum_kernel_NTSTATUS)0xC0230015)
#define _WUM_KERNEL_STATUS_NDIS_BUFFER_TOO_SHORT                       \
	((__wum_kernel_NTSTATUS)0xC0230016)
#define _WUM_KERNEL_STATUS_NDIS_INVALID_OID                            \
	((__wum_kernel_NTSTATUS)0xC0230017)
#define _WUM_KERNEL_STATUS_NDIS_ADAPTER_REMOVED                        \
	((__wum_kernel_NTSTATUS)0xC0230018)
#define _WUM_KERNEL_STATUS_NDIS_UNSUPPORTED_MEDIA                      \
	((__wum_kernel_NTSTATUS)0xC0230019)
#define _WUM_KERNEL_STATUS_NDIS_GROUP_ADDRESS_IN_USE                   \
	((__wum_kernel_NTSTATUS)0xC023001A)
#define _WUM_KERNEL_STATUS_NDIS_FILE_NOT_FOUND                         \
	((__wum_kernel_NTSTATUS)0xC023001B)
#define _WUM_KERNEL_STATUS_NDIS_ERROR_READING_FILE                     \
	((__wum_kernel_NTSTATUS)0xC023001C)
#define _WUM_KERNEL_STATUS_NDIS_ALREADY_MAPPED                         \
	((__wum_kernel_NTSTATUS)0xC023001D)
#define _WUM_KERNEL_STATUS_NDIS_RESOURCE_CONFLICT                      \
	((__wum_kernel_NTSTATUS)0xC023001E)
#define _WUM_KERNEL_STATUS_NDIS_MEDIA_DISCONNECTED                     \
	((__wum_kernel_NTSTATUS)0xC023001F)
#define _WUM_KERNEL_STATUS_NDIS_INVALID_ADDRESS                        \
	((__wum_kernel_NTSTATUS)0xC0230022)
#define _WUM_KERNEL_STATUS_NDIS_PAUSED                                 \
	((__wum_kernel_NTSTATUS)0xC023002A)
#define _WUM_KERNEL_STATUS_NDIS_INTERFACE_NOT_FOUND                    \
	((__wum_kernel_NTSTATUS)0xC023002B)
#define _WUM_KERNEL_STATUS_NDIS_UNSUPPORTED_REVISION                   \
	((__wum_kernel_NTSTATUS)0xC023002C)
#define _WUM_KERNEL_STATUS_NDIS_INVALID_PORT                           \
	((__wum_kernel_NTSTATUS)0xC023002D)
#define _WUM_KERNEL_STATUS_NDIS_INVALID_PORT_STATE                     \
	((__wum_kernel_NTSTATUS)0xC023002E)
#define _WUM_KERNEL_STATUS_NDIS_LOW_POWER_STATE                        \
	((__wum_kernel_NTSTATUS)0xC023002F)
#define _WUM_KERNEL_STATUS_NDIS_NOT_SUPPORTED                          \
	((__wum_kernel_NTSTATUS)0xC02300BB)
#define _WUM_KERNEL_STATUS_NDIS_OFFLOAD_POLICY                         \
	((__wum_kernel_NTSTATUS)0xC023100F)
#define _WUM_KERNEL_STATUS_NDIS_OFFLOAD_CONNECTION_REJECTED            \
	((__wum_kernel_NTSTATUS)0xC0231012)
#define _WUM_KERNEL_STATUS_NDIS_OFFLOAD_PATH_REJECTED                  \
	((__wum_kernel_NTSTATUS)0xC0231013)
#define _WUM_KERNEL_STATUS_NDIS_DOT11_AUTO_CONFIG_ENABLED              \
	((__wum_kernel_NTSTATUS)0xC0232000)
#define _WUM_KERNEL_STATUS_NDIS_DOT11_MEDIA_IN_USE                     \
	((__wum_kernel_NTSTATUS)0xC0232001)
#define _WUM_KERNEL_STATUS_NDIS_DOT11_POWER_STATE_INVALID              \
	((__wum_kernel_NTSTATUS)0xC0232002)
#define _WUM_KERNEL_STATUS_NDIS_PM_WOL_PATTERN_LIST_FULL               \
	((__wum_kernel_NTSTATUS)0xC0232003)
#define _WUM_KERNEL_STATUS_NDIS_PM_PROTOCOL_OFFLOAD_LIST_FULL          \
	((__wum_kernel_NTSTATUS)0xC0232004)
#define _WUM_KERNEL_STATUS_IPSEC_BAD_SPI                               \
	((__wum_kernel_NTSTATUS)0xC0360001)
#define _WUM_KERNEL_STATUS_IPSEC_SA_LIFETIME_EXPIRED                   \
	((__wum_kernel_NTSTATUS)0xC0360002)
#define _WUM_KERNEL_STATUS_IPSEC_WRONG_SA                              \
	((__wum_kernel_NTSTATUS)0xC0360003)
#define _WUM_KERNEL_STATUS_IPSEC_REPLAY_CHECK_FAILED                   \
	((__wum_kernel_NTSTATUS)0xC0360004)
#define _WUM_KERNEL_STATUS_IPSEC_INVALID_PACKET                        \
	((__wum_kernel_NTSTATUS)0xC0360005)
#define _WUM_KERNEL_STATUS_IPSEC_INTEGRITY_CHECK_FAILED                \
	((__wum_kernel_NTSTATUS)0xC0360006)
#define _WUM_KERNEL_STATUS_IPSEC_CLEAR_TEXT_DROP                       \
	((__wum_kernel_NTSTATUS)0xC0360007)
#define _WUM_KERNEL_STATUS_IPSEC_AUTH_FIREWALL_DROP                    \
	((__wum_kernel_NTSTATUS)0xC0360008)
#define _WUM_KERNEL_STATUS_IPSEC_THROTTLE_DROP                         \
	((__wum_kernel_NTSTATUS)0xC0360009)
#define _WUM_KERNEL_STATUS_IPSEC_DOSP_BLOCK                            \
	((__wum_kernel_NTSTATUS)0xC0368000)
#define _WUM_KERNEL_STATUS_IPSEC_DOSP_RECEIVED_MULTICAST               \
	((__wum_kernel_NTSTATUS)0xC0368001)
#define _WUM_KERNEL_STATUS_IPSEC_DOSP_INVALID_PACKET                   \
	((__wum_kernel_NTSTATUS)0xC0368002)
#define _WUM_KERNEL_STATUS_IPSEC_DOSP_STATE_LOOKUP_FAILED              \
	((__wum_kernel_NTSTATUS)0xC0368003)
#define _WUM_KERNEL_STATUS_IPSEC_DOSP_MAX_ENTRIES                      \
	((__wum_kernel_NTSTATUS)0xC0368004)
#define _WUM_KERNEL_STATUS_IPSEC_DOSP_KEYMOD_NOT_ALLOWED               \
	((__wum_kernel_NTSTATUS)0xC0368005)
#define _WUM_KERNEL_STATUS_IPSEC_DOSP_MAX_PER_IP_RATELIMIT_QUEUES      \
	((__wum_kernel_NTSTATUS)0xC0368006)
#define _WUM_KERNEL_STATUS_VOLMGR_MIRROR_NOT_SUPPORTED                 \
	((__wum_kernel_NTSTATUS)0xC038005B)
#define _WUM_KERNEL_STATUS_VOLMGR_RAID5_NOT_SUPPORTED                  \
	((__wum_kernel_NTSTATUS)0xC038005C)
#define _WUM_KERNEL_STATUS_VIRTDISK_PROVIDER_NOT_FOUND                 \
	((__wum_kernel_NTSTATUS)0xC03A0014)
#define _WUM_KERNEL_STATUS_VIRTDISK_NOT_VIRTUAL_DISK                   \
	((__wum_kernel_NTSTATUS)0xC03A0015)
#define _WUM_KERNEL_STATUS_VHD_PARENT_VHD_ACCESS_DENIED                \
	((__wum_kernel_NTSTATUS)0xC03A0016)
#define _WUM_KERNEL_STATUS_VHD_CHILD_PARENT_SIZE_MISMATCH              \
	((__wum_kernel_NTSTATUS)0xC03A0017)
#define _WUM_KERNEL_STATUS_VHD_DIFFERENCING_CHAIN_CYCLE_DETECTED       \
	((__wum_kernel_NTSTATUS)0xC03A0018)
#define _WUM_KERNEL_STATUS_VHD_DIFFERENCING_CHAIN_ERROR_IN_PARENT      \
	((__wum_kernel_NTSTATUS)0xC03A0019)

#endif
