/*
 * Copyright 2015 Steven Stewart-Gallus
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
 * implied.  See the License for the specific language governing
 * permissions and limitations under the License.
 */
#ifndef _WUM_KERNEL_WINNT_H
#define _WUM_KERNEL_WINNT_H 1

#define _WUM_KERNEL_PAGE_NOACCESS 0x01
#define _WUM_KERNEL_PAGE_READONLY 0x02
#define _WUM_KERNEL_PAGE_READWRITE 0x04
#define _WUM_KERNEL_PAGE_WRITECOPY 0x08
#define _WUM_KERNEL_PAGE_EXECUTE 0x10
#define _WUM_KERNEL_PAGE_EXECUTE_READ 0x20
#define _WUM_KERNEL_PAGE_EXECUTE_READWRITE 0x40
#define _WUM_KERNEL_PAGE_EXECUTE_WRITECOPY 0x80
#define _WUM_KERNEL_PAGE_GUARD 0x100
#define _WUM_KERNEL_PAGE_NOCACHE 0x200
#define _WUM_KERNEL_PAGE_WRITECOMBINE 0x400

#define _WUM_KERNEL_MEM_COMMIT 0x1000
#define _WUM_KERNEL_MEM_RESERVE 0x2000
#define _WUM_KERNEL_MEM_DECOMMIT 0x4000
#define _WUM_KERNEL_MEM_RELEASE 0x8000
#define _WUM_KERNEL_MEM_FREE 0x10000
#define _WUM_KERNEL_MEM_PRIVATE 0x20000
#define _WUM_KERNEL_MEM_MAPPED 0x40000
#define _WUM_KERNEL_MEM_RESET 0x80000
#define _WUM_KERNEL_MEM_TOP_DOWN 0x100000
#define _WUM_KERNEL_MEM_WRITE_WATCH 0x200000
#define _WUM_KERNEL_MEM_PHYSICAL 0x400000
#define _WUM_KERNEL_MEM_ROTATE 0x800000
#define _WUM_KERNEL_MEM_LARGE_PAGES 0x20000000
#define _WUM_KERNEL_MEM_4MB_PAGES 0x80000000

#ifdef STRICT
#define __wum_kernel_DECLARE_HANDLE(name)                              \
	struct name##__ {                                              \
		int unused;                                            \
	};                                                             \
	typedef struct name##__ *name
#else
#define __wum_kernel_DECLARE_HANDLE(name)                              \
	typedef __wum_kernel_HANDLE name
#endif

#define _WUM_KERNEL_DUPLICATE_CLOSE_SOURCE 0x00000001
#define _WUM_KERNEL_DUPLICATE_SAME_ACCESS 0x00000002

struct _wum_kernel_IMAGE_DOS_HEADER {
	__wum_kernel_uint16_t e_magic;
	__wum_kernel_uint16_t e_cblp;
	__wum_kernel_uint16_t e_cp;
	__wum_kernel_uint16_t e_crlc;
	__wum_kernel_uint16_t e_cparhdr;
	__wum_kernel_uint16_t e_minalloc;
	__wum_kernel_uint16_t e_maxalloc;
	__wum_kernel_uint16_t e_ss;
	__wum_kernel_uint16_t e_sp;
	__wum_kernel_uint16_t e_csum;
	__wum_kernel_uint16_t e_ip;
	__wum_kernel_uint16_t e_cs;
	__wum_kernel_uint16_t e_lfarlc;
	__wum_kernel_uint16_t e_ovno;
	__wum_kernel_uint16_t e_res[4];
	__wum_kernel_uint16_t e_oemid;
	__wum_kernel_uint16_t e_oeminfo;
	__wum_kernel_uint16_t e_res2[10];
	__wum_kernel_uint32_t e_lfanew;
};

struct _wum_kernel_IMAGE_FILE_HEADER {
	__wum_kernel_uint16_t Machine;
	__wum_kernel_uint16_t NumberOfSections;
	__wum_kernel_uint32_t TimeDateStamp;
	__wum_kernel_uint32_t PointerToSymbolTable;
	__wum_kernel_uint32_t NumberOfSymbols;
	__wum_kernel_uint16_t SizeOfOptionalHeader;
	__wum_kernel_uint16_t Characteristics;
};

#define _WUM_KERNEL_IMAGE_FILE_HEADER_SIZE 20U

struct _wum_kernel_IMAGE_DATA_DIRECTORY {
	__wum_kernel_uint32_t VirtualAddress;
	__wum_kernel_uint32_t Size;
};

struct _wum_kernel_IMAGE_OPTIONAL_HEADER {
	__wum_kernel_uint16_t Magic;
	__wum_kernel_uint8_t MajorLinkerVersion;
	__wum_kernel_uint8_t MinorLinkerVersion;
	__wum_kernel_uint32_t SizeOfCode;
	__wum_kernel_uint32_t SizeOfInitializedData;
	__wum_kernel_uint32_t SizeOfUninitializedData;
	__wum_kernel_uint32_t AddressOfEntryPoint;
	__wum_kernel_uint32_t BaseOfCode;
	__wum_kernel_uint64_t ImageBase;
	__wum_kernel_uint32_t SectionAlignment;
	__wum_kernel_uint32_t FileAlignment;
	__wum_kernel_uint16_t MajorOperatingSystemVersion;
	__wum_kernel_uint16_t MinorOperatingSystemVersion;
	__wum_kernel_uint16_t MajorImageVersion;
	__wum_kernel_uint16_t MinorImageVersion;
	__wum_kernel_uint16_t MajorSubsystemVersion;
	__wum_kernel_uint16_t MinorSubsystemVersion;
	__wum_kernel_uint32_t Win32VersionValue;
	__wum_kernel_uint32_t SizeOfImage;
	__wum_kernel_uint32_t SizeOfHeaders;
	__wum_kernel_uint32_t CheckSum;
	__wum_kernel_uint16_t Subsystem;
	__wum_kernel_uint16_t DllCharacteristics;
	__wum_kernel_uint64_t SizeOfStackReserve;
	__wum_kernel_uint64_t SizeOfStackCommit;
	__wum_kernel_uint64_t SizeOfHeapReserve;
	__wum_kernel_uint64_t SizeOfHeapCommit;
	__wum_kernel_uint32_t LoaderFlags;
	__wum_kernel_uint32_t NumberOfRvaAndSizes;
	struct _wum_kernel_IMAGE_DATA_DIRECTORY DataDirectory[16U];
};

struct _wum_kernel_IMAGE_SECTION_HEADER {
	__wum_kernel_uint8_t Name[8U];
	union {
		__wum_kernel_uint32_t PhysicalAddress;
		__wum_kernel_uint32_t VirtualSize;
	} Misc;
	__wum_kernel_uint32_t VirtualAddress;
	__wum_kernel_uint32_t SizeOfRawData;
	__wum_kernel_uint32_t PointerToRawData;
	__wum_kernel_uint32_t PointerToRelocations;
	__wum_kernel_uint32_t PointerToLinenumbers;
	__wum_kernel_uint16_t NumberOfRelocations;
	__wum_kernel_uint16_t NumberOfLinenumbers;
	__wum_kernel_uint32_t Characteristics;
};

#define _WUM_KERNEL_IMAGE_OPTIONAL_HEADER_SIZE 240U

#define _WUM_KERNEL_IMAGE_SCN_CNT_CODE 0x00000020
#define _WUM_KERNEL_IMAGE_SCN_CNT_INITIALIZED_DATA 0x00000040
#define _WUM_KERNEL_IMAGE_SCN_CNT_UNINITIALIZED_DATA 0x00000080

#define _WUM_KERNEL_IMAGE_SCN_LNK_NRELOC_OVFL 0x01000000
#define _WUM_KERNEL_IMAGE_SCN_MEM_DISCARDABLE 0x02000000
#define _WUM_KERNEL_IMAGE_SCN_MEM_NOT_CACHED 0x04000000
#define _WUM_KERNEL_IMAGE_SCN_MEM_NOT_PAGED 0x08000000
#define _WUM_KERNEL_IMAGE_SCN_MEM_SHARED 0x10000000
#define _WUM_KERNEL_IMAGE_SCN_MEM_EXECUTE 0x20000000
#define _WUM_KERNEL_IMAGE_SCN_MEM_READ 0x40000000
#define _WUM_KERNEL_IMAGE_SCN_MEM_WRITE 0x80000000

#endif
