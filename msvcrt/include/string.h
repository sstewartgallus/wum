/*
 * Copyright 2015 Steven Stewart-Gallus
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
 * implied.  See the License for the specific language governing
 * permissions and limitations under the License.
 */
#ifndef _WUM_MSVCRT_STRING_H
#define _WUM_MSVCRT_STRING_H 1

#include "internal/api.h"
#include "internal/errno.h"

#include <wum-kernel/stddef.h>

_WUM_MSVCRT_INTERNAL_API char *
strerror(__wum_msvcrt_errno_t __err) _WUM_MSVCRT_INTERNAL_API_END;

_WUM_MSVCRT_INTERNAL_API __wum_kernel_size_t
strlen(char const *__str) _WUM_MSVCRT_INTERNAL_API_END;

#endif
