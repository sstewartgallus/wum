/*
 * Copyright 2015 Steven Stewart-Gallus
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
 * implied.  See the License for the specific language governing
 * permissions and limitations under the License.
 */
#ifndef _WUM_MSVCRT_WINBASE_H
#define _WUM_MSVCRT_WINBASE_H 1

#include "internal/api.h"
#include "internal/mwindef.h"

#include <wum-kernel/stdint.h>
#include <wum-kernel/wchar.h>
#include <wum-kernel/winbase.h>

typedef __wum_kernel_HANDLE HANDLE;

#define STD_INPUT_HANDLE ((__wum_kernel_uint32_t)-10)
#define STD_OUTPUT_HANDLE ((__wum_kernel_uint32_t)-11)
#define STD_ERROR_HANDLE ((__wum_kernel_uint32_t)-12)

_WUM_MSVCRT_INTERNAL_API __wum_kernel_HANDLE
GetCurrentProcess(void) _WUM_MSVCRT_INTERNAL_API_END;

_WUM_MSVCRT_INTERNAL_API __wum_msvcrt_HMODULE
LoadLibraryW(__wum_kernel_wchar_t const *lpLibFileName)
    _WUM_MSVCRT_INTERNAL_API_END;

static inline void SetLastError(__wum_kernel_uint32_t err);
static inline __wum_kernel_uint32_t GetLastError(void);

static inline void SetLastError(__wum_kernel_uint32_t err)
{
#if defined __x86_64__
	__asm__ __volatile__("movl %0, %%fs:0x68" ::"r"(err)
	                     : "memory");
#else
#error No SetLastError implementation for this architecture
#endif
}

static inline __wum_kernel_uint32_t GetLastError(void)
{
	__wum_kernel_uint32_t err;
#if defined __x86_64__
	__asm__ __volatile__("movl %%fs:0x68, %0" : "=r"(err));
#else
#error No SetLastError implementation for this architecture
#endif
	return err;
}

#endif
