/*
 * Copyright 2015 Steven Stewart-Gallus
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
 * implied.  See the License for the specific language governing
 * permissions and limitations under the License.
 */
#ifndef _WUM_MSVCRT_PROCESSENV_H
#define _WUM_MSVCRT_PROCESSENV_H 1

#include "internal/api.h"

#include <wum-kernel/winbase.h>
#include <wum-kernel/stdint.h>

_WUM_MSVCRT_INTERNAL_API
__wum_kernel_HANDLE GetStdHandle(__wum_kernel_uint32_t nStdHandle)
    _WUM_MSVCRT_INTERNAL_API_END;

#endif
