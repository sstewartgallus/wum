/*
 * Copyright 2015 Steven Stewart-Gallus
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
 * implied.  See the License for the specific language governing
 * permissions and limitations under the License.
 */
#define STRICT

#include "config.h"

#include <stddef.h>
#include <handleapi.h>
#include <processenv.h>
#include <wchar.h>
#include <winbase.h>

#include "ntdll/nt.h"

_WUM_MSVCRT_INTERNAL_API
__wum_kernel_HANDLE GetStdHandle(__wum_kernel_uint32_t nStdHandle)
    _WUM_MSVCRT_INTERNAL_API_END
{
	/* TODO: Locking */
	struct _wum_kernel_RTL_USER_PROCESS_PARAMETERS *params =
	    NtCurrentTeb()->Peb->ProcessParameters;
	switch (nStdHandle) {
	case STD_INPUT_HANDLE:
		return params->hStdInput;

	case STD_OUTPUT_HANDLE:
		return params->hStdOutput;

	case STD_ERROR_HANDLE:
		return params->hStdOutput;

	default:
		return INVALID_HANDLE_VALUE;
	}
}

_WUM_MSVCRT_INTERNAL_API __wum_kernel_HANDLE
GetCurrentProcess(void) _WUM_MSVCRT_INTERNAL_API_END
{
	return (__wum_kernel_HANDLE *)-1;
}

static struct __wum_msvcrt_HINSTANCE__ ntdll_proxy;
_WUM_MSVCRT_INTERNAL_API __wum_msvcrt_HMODULE
LoadLibraryW(wchar_t const *lpLibFileName) _WUM_MSVCRT_INTERNAL_API_END
{
	static wchar_t const ntdll[] = L"ntdll.dll";

	size_t size = (sizeof ntdll / sizeof ntdll[0U]) - 1U;

	size_t ii;
	for (ii = 0U; ii < size; ++ii) {
		if (lpLibFileName[ii] != ntdll[ii]) {
			/* TODO: SetLastError */
			return 0;
		}
	}
	if ('\0' != lpLibFileName[size])
		return 0;

	return &ntdll_proxy;
}
