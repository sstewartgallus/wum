/*
 * Copyright 2015 Steven Stewart-Gallus
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
 * implied.  See the License for the specific language governing
 * permissions and limitations under the License.
 */
#define STRICT

#include "config.h"

#include <errno.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>

struct __wum_msvcrt_FILE {
	int fd;
};

_WUM_MSVCRT_INTERNAL_API struct __wum_msvcrt_FILE *
__wum_msvcrt_stdin(void) _WUM_MSVCRT_INTERNAL_API_END
{
	static struct __wum_msvcrt_FILE stdin_file = {.fd =
	                                                  STDIN_FILENO};
	return &stdin_file;
}

_WUM_MSVCRT_INTERNAL_API struct __wum_msvcrt_FILE *
__wum_msvcrt_stderr(void) _WUM_MSVCRT_INTERNAL_API_END
{
	static struct __wum_msvcrt_FILE stderr_file = {
	    .fd = STDERR_FILENO};
	return &stderr_file;
}

_WUM_MSVCRT_INTERNAL_API struct __wum_msvcrt_FILE *
__wum_msvcrt_stdout(void) _WUM_MSVCRT_INTERNAL_API_END
{
	static struct __wum_msvcrt_FILE stdout_file = {
	    .fd = STDOUT_FILENO};
	return &stdout_file;
}
_WUM_MSVCRT_INTERNAL_API int
fputc(int __c, FILE *__stream) _WUM_MSVCRT_INTERNAL_API_END
{
	return write(__stream->fd, &__c, 1U);
}

_WUM_MSVCRT_INTERNAL_API __wum_kernel_size_t
fwrite(void const *__ptr, __wum_kernel_size_t __size,
       __wum_kernel_size_t __nmemb,
       FILE *__stream) _WUM_MSVCRT_INTERNAL_API_END
{
	__wum_msvcrt_ssize_t results =
	    write(__stream->fd, __ptr, __size * __nmemb);
	if (results < 0)
		return 0;
	return results;
}

_WUM_MSVCRT_INTERNAL_API int
fputs(const char *__s, FILE *__stream) _WUM_MSVCRT_INTERNAL_API_END
{
	return write(__stream->fd, __s, strlen(__s));
}

_WUM_MSVCRT_INTERNAL_API int
puts(char const *__s) _WUM_MSVCRT_INTERNAL_API_END
{
	return fputs(__s, stdout);
}

_WUM_MSVCRT_INTERNAL_API void
perror(char const *__s) _WUM_MSVCRT_INTERNAL_API_END
{
	errno_t err = errno;
	fputs(__s, stderr);
	if (err != 0) {
		fputs(": ", stderr);
		fputs(strerror(err), stderr);
	}
	fputs("\n", stderr);
}
