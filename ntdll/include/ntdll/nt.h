/*
 * Copyright 2015 Steven Stewart-Gallus
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
 * implied.  See the License for the specific language governing
 * permissions and limitations under the License.
 */
#ifndef _WUM_NTDLL_NT_H
#define _WUM_NTDLL_NT_H 1

#include "ntdll/internal/api.h"

#include <wum-kernel/ntdef.h>
#include <wum-kernel/stddef.h>
#include <wum-kernel/stdint.h>
#include <wum-kernel/winbase.h>
#include <wum-kernel/winternl.h>

#define _In_
#define _Inout_
#define _In_opt_
#define _Out_opt_

typedef struct _IO_STATUS_BLOCK {
	union {
		__wum_kernel_NTSTATUS Status;
		void *Pointer;
	} DUMMYUNIONNAME;

	__wum_kernel_uintptr_t Information;
} IO_STATUS_BLOCK, *PIO_STATUS_BLOCK;

typedef union _LARGE_INTEGER {
	struct {
#ifdef WORDS_BIGENDIAN
		__wum_kernel_int32_t HighPart;
		__wum_kernel_uint32_t LowPart;
#else
		__wum_kernel_uint32_t LowPart;
		__wum_kernel_int32_t HighPart;
#endif
	} u;
	__wum_kernel_int64_t QuadPart;
} LARGE_INTEGER, *PLARGE_INTEGER;

typedef void (*PIO_APC_ROUTINE)(void *, PIO_STATUS_BLOCK,
                                __wum_kernel_uint32_t foo);

typedef __wum_kernel_uint32_t ACCESS_MASK;

_WUM_NTDLL_INTERNAL_API struct _wum_kernel_TEB *
NtCurrentTeb(void) _WUM_NTDLL_INTERNAL_API_END;

_WUM_NTDLL_INTERNAL_API __wum_kernel_NTSTATUS
NtWriteFile(__wum_kernel_HANDLE hFile, __wum_kernel_HANDLE hEvent,
            PIO_APC_ROUTINE apc, void *apc_user,
            PIO_STATUS_BLOCK io_status, void const *buffer,
            __wum_kernel_uint32_t length, PLARGE_INTEGER offset,
            __wum_kernel_uint32_t *key) _WUM_NTDLL_INTERNAL_API_END;

_WUM_NTDLL_INTERNAL_API __wum_kernel_NTSTATUS
NtClose(__wum_kernel_HANDLE handle) _WUM_NTDLL_INTERNAL_API_END;

_WUM_NTDLL_INTERNAL_API __wum_kernel_NTSTATUS NtTerminateProcess(
    __wum_kernel_HANDLE process,
    __wum_kernel_int32_t exit_code) _WUM_NTDLL_INTERNAL_API_END;

_WUM_NTDLL_INTERNAL_API __wum_kernel_NTSTATUS NtAllocateVirtualMemory(
    _In_ __wum_kernel_HANDLE ProcessHandle, _Inout_ void **BaseAddress,
    _In_ __wum_kernel_uintptr_t ZeroBits,
    _Inout_ __wum_kernel_size_t *RegionSize,
    _In_ __wum_kernel_uint32_t AllocationType,
    _In_ __wum_kernel_uint32_t Protect) _WUM_NTDLL_INTERNAL_API_END;

_WUM_NTDLL_INTERNAL_API
__wum_kernel_NTSTATUS NtFreeVirtualMemory(
    _In_ __wum_kernel_HANDLE ProcessHandle, _Inout_ void **BaseAddress,
    _Inout_ __wum_kernel_size_t *RegionSize,
    _In_ __wum_kernel_uint32_t FreeType) _WUM_NTDLL_INTERNAL_API_END;

_WUM_NTDLL_INTERNAL_API
__wum_kernel_NTSTATUS
NtDuplicateObject(_In_ __wum_kernel_HANDLE SourceProcessHandle,
                  _In_ __wum_kernel_HANDLE SourceHandle,
                  _In_opt_ __wum_kernel_HANDLE TargetProcessHandle,
                  _Out_opt_ __wum_kernel_HANDLE *TargetHandle,
                  _In_ ACCESS_MASK DesiredAccess,
                  _In_ __wum_kernel_uint32_t HandleAttributes,
                  _In_ __wum_kernel_uint32_t
                      Options) _WUM_NTDLL_INTERNAL_API_END;

_WUM_NTDLL_INTERNAL_API void
NtAcceptConnectPort(void) _WUM_NTDLL_INTERNAL_API_END;
_WUM_NTDLL_INTERNAL_API void
NtAccessCheck(void) _WUM_NTDLL_INTERNAL_API_END;
_WUM_NTDLL_INTERNAL_API void
NtAccessCheckAndAuditAlarm(void) _WUM_NTDLL_INTERNAL_API_END;
_WUM_NTDLL_INTERNAL_API void
NtAddAtom(void) _WUM_NTDLL_INTERNAL_API_END;
_WUM_NTDLL_INTERNAL_API void
NtAdjustGroupsToken(void) _WUM_NTDLL_INTERNAL_API_END;
_WUM_NTDLL_INTERNAL_API void
NtAdjustPrivilegesToken(void) _WUM_NTDLL_INTERNAL_API_END;
_WUM_NTDLL_INTERNAL_API void
NtAlertResumeThread(void) _WUM_NTDLL_INTERNAL_API_END;
_WUM_NTDLL_INTERNAL_API void
NtAlertThread(void) _WUM_NTDLL_INTERNAL_API_END;
_WUM_NTDLL_INTERNAL_API void
NtAllocateLocallyUniqueId(void) _WUM_NTDLL_INTERNAL_API_END;
_WUM_NTDLL_INTERNAL_API void
NtAllocateUuids(void) _WUM_NTDLL_INTERNAL_API_END;
_WUM_NTDLL_INTERNAL_API void
NtAreMappedFilesTheSame(void) _WUM_NTDLL_INTERNAL_API_END;
_WUM_NTDLL_INTERNAL_API void
NtAssignProcessToJobObject(void) _WUM_NTDLL_INTERNAL_API_END;
_WUM_NTDLL_INTERNAL_API void
NtCancelIoFile(void) _WUM_NTDLL_INTERNAL_API_END;
_WUM_NTDLL_INTERNAL_API void
NtCancelIoFileEx(void) _WUM_NTDLL_INTERNAL_API_END;
_WUM_NTDLL_INTERNAL_API void
NtCancelTimer(void) _WUM_NTDLL_INTERNAL_API_END;
_WUM_NTDLL_INTERNAL_API void
NtClearEvent(void) _WUM_NTDLL_INTERNAL_API_END;
_WUM_NTDLL_INTERNAL_API void
NtCompleteConnectPort(void) _WUM_NTDLL_INTERNAL_API_END;
_WUM_NTDLL_INTERNAL_API void
NtConnectPort(void) _WUM_NTDLL_INTERNAL_API_END;
_WUM_NTDLL_INTERNAL_API void
NtCreateDirectoryObject(void) _WUM_NTDLL_INTERNAL_API_END;
_WUM_NTDLL_INTERNAL_API void
NtCreateEvent(void) _WUM_NTDLL_INTERNAL_API_END;
_WUM_NTDLL_INTERNAL_API void
NtCreateFile(void) _WUM_NTDLL_INTERNAL_API_END;
_WUM_NTDLL_INTERNAL_API void
NtCreateIoCompletion(void) _WUM_NTDLL_INTERNAL_API_END;
_WUM_NTDLL_INTERNAL_API void
NtCreateJobObject(void) _WUM_NTDLL_INTERNAL_API_END;
_WUM_NTDLL_INTERNAL_API void
NtCreateKey(void) _WUM_NTDLL_INTERNAL_API_END;
_WUM_NTDLL_INTERNAL_API void
NtCreateKeyedEvent(void) _WUM_NTDLL_INTERNAL_API_END;
_WUM_NTDLL_INTERNAL_API void
NtCreateMailslotFile(void) _WUM_NTDLL_INTERNAL_API_END;
_WUM_NTDLL_INTERNAL_API void
NtCreateMutant(void) _WUM_NTDLL_INTERNAL_API_END;
_WUM_NTDLL_INTERNAL_API void
NtCreateNamedPipeFile(void) _WUM_NTDLL_INTERNAL_API_END;
_WUM_NTDLL_INTERNAL_API void
NtCreatePagingFile(void) _WUM_NTDLL_INTERNAL_API_END;
_WUM_NTDLL_INTERNAL_API void
NtCreatePort(void) _WUM_NTDLL_INTERNAL_API_END;
_WUM_NTDLL_INTERNAL_API void
NtCreateSection(void) _WUM_NTDLL_INTERNAL_API_END;
_WUM_NTDLL_INTERNAL_API void
NtCreateSemaphore(void) _WUM_NTDLL_INTERNAL_API_END;
_WUM_NTDLL_INTERNAL_API void
NtCreateSymbolicLinkObject(void) _WUM_NTDLL_INTERNAL_API_END;
_WUM_NTDLL_INTERNAL_API void
NtCreateTimer(void) _WUM_NTDLL_INTERNAL_API_END;
_WUM_NTDLL_INTERNAL_API void
NtDelayExecution(void) _WUM_NTDLL_INTERNAL_API_END;
_WUM_NTDLL_INTERNAL_API void
NtDeleteAtom(void) _WUM_NTDLL_INTERNAL_API_END;
_WUM_NTDLL_INTERNAL_API void
NtDeleteFile(void) _WUM_NTDLL_INTERNAL_API_END;
_WUM_NTDLL_INTERNAL_API void
NtDeleteKey(void) _WUM_NTDLL_INTERNAL_API_END;
_WUM_NTDLL_INTERNAL_API void
NtDeleteValueKey(void) _WUM_NTDLL_INTERNAL_API_END;
_WUM_NTDLL_INTERNAL_API void
NtDeviceIoControlFile(void) _WUM_NTDLL_INTERNAL_API_END;
_WUM_NTDLL_INTERNAL_API void
NtDisplayString(void) _WUM_NTDLL_INTERNAL_API_END;
_WUM_NTDLL_INTERNAL_API void
NtDuplicateToken(void) _WUM_NTDLL_INTERNAL_API_END;
_WUM_NTDLL_INTERNAL_API void
NtEnumerateKey(void) _WUM_NTDLL_INTERNAL_API_END;
_WUM_NTDLL_INTERNAL_API void
NtEnumerateValueKey(void) _WUM_NTDLL_INTERNAL_API_END;
_WUM_NTDLL_INTERNAL_API void
NtFindAtom(void) _WUM_NTDLL_INTERNAL_API_END;
_WUM_NTDLL_INTERNAL_API void
NtFlushBuffersFile(void) _WUM_NTDLL_INTERNAL_API_END;
_WUM_NTDLL_INTERNAL_API void
NtFlushInstructionCache(void) _WUM_NTDLL_INTERNAL_API_END;
_WUM_NTDLL_INTERNAL_API void
NtFlushKey(void) _WUM_NTDLL_INTERNAL_API_END;
_WUM_NTDLL_INTERNAL_API void
NtFlushVirtualMemory(void) _WUM_NTDLL_INTERNAL_API_END;
_WUM_NTDLL_INTERNAL_API void
NtFsControlFile(void) _WUM_NTDLL_INTERNAL_API_END;
_WUM_NTDLL_INTERNAL_API void
NtGetContextThread(void) _WUM_NTDLL_INTERNAL_API_END;
_WUM_NTDLL_INTERNAL_API void
NtGetCurrentProcessorNumber(void) _WUM_NTDLL_INTERNAL_API_END;
_WUM_NTDLL_INTERNAL_API void
NtGetTickCount(void) _WUM_NTDLL_INTERNAL_API_END;
_WUM_NTDLL_INTERNAL_API void
NtGetWriteWatch(void) _WUM_NTDLL_INTERNAL_API_END;
_WUM_NTDLL_INTERNAL_API void
NtInitiatePowerAction(void) _WUM_NTDLL_INTERNAL_API_END;
_WUM_NTDLL_INTERNAL_API void
NtIsProcessInJob(void) _WUM_NTDLL_INTERNAL_API_END;
_WUM_NTDLL_INTERNAL_API void
NtListenPort(void) _WUM_NTDLL_INTERNAL_API_END;
_WUM_NTDLL_INTERNAL_API void
NtLoadDriver(void) _WUM_NTDLL_INTERNAL_API_END;
_WUM_NTDLL_INTERNAL_API void
NtLoadKey(void) _WUM_NTDLL_INTERNAL_API_END;
_WUM_NTDLL_INTERNAL_API void
NtLockFile(void) _WUM_NTDLL_INTERNAL_API_END;
_WUM_NTDLL_INTERNAL_API void
NtLockVirtualMemory(void) _WUM_NTDLL_INTERNAL_API_END;
_WUM_NTDLL_INTERNAL_API void
NtMakeTemporaryObject(void) _WUM_NTDLL_INTERNAL_API_END;
_WUM_NTDLL_INTERNAL_API void
NtMapViewOfSection(void) _WUM_NTDLL_INTERNAL_API_END;
_WUM_NTDLL_INTERNAL_API void
NtNotifyChangeDirectoryFile(void) _WUM_NTDLL_INTERNAL_API_END;
_WUM_NTDLL_INTERNAL_API void
NtNotifyChangeKey(void) _WUM_NTDLL_INTERNAL_API_END;
_WUM_NTDLL_INTERNAL_API void
NtOpenDirectoryObject(void) _WUM_NTDLL_INTERNAL_API_END;
_WUM_NTDLL_INTERNAL_API void
NtOpenEvent(void) _WUM_NTDLL_INTERNAL_API_END;
_WUM_NTDLL_INTERNAL_API void
NtOpenFile(void) _WUM_NTDLL_INTERNAL_API_END;
_WUM_NTDLL_INTERNAL_API void
NtOpenIoCompletion(void) _WUM_NTDLL_INTERNAL_API_END;
_WUM_NTDLL_INTERNAL_API void
NtOpenJobObject(void) _WUM_NTDLL_INTERNAL_API_END;
_WUM_NTDLL_INTERNAL_API void
NtOpenKey(void) _WUM_NTDLL_INTERNAL_API_END;
_WUM_NTDLL_INTERNAL_API void
NtOpenKeyedEvent(void) _WUM_NTDLL_INTERNAL_API_END;
_WUM_NTDLL_INTERNAL_API void
NtOpenMutant(void) _WUM_NTDLL_INTERNAL_API_END;
_WUM_NTDLL_INTERNAL_API void
NtOpenProcess(void) _WUM_NTDLL_INTERNAL_API_END;
_WUM_NTDLL_INTERNAL_API void
NtOpenProcessToken(void) _WUM_NTDLL_INTERNAL_API_END;
_WUM_NTDLL_INTERNAL_API void
NtOpenProcessTokenEx(void) _WUM_NTDLL_INTERNAL_API_END;
_WUM_NTDLL_INTERNAL_API void
NtOpenSection(void) _WUM_NTDLL_INTERNAL_API_END;
_WUM_NTDLL_INTERNAL_API void
NtOpenSemaphore(void) _WUM_NTDLL_INTERNAL_API_END;
_WUM_NTDLL_INTERNAL_API void
NtOpenSymbolicLinkObject(void) _WUM_NTDLL_INTERNAL_API_END;
_WUM_NTDLL_INTERNAL_API void
NtOpenThread(void) _WUM_NTDLL_INTERNAL_API_END;
_WUM_NTDLL_INTERNAL_API void
NtOpenThreadToken(void) _WUM_NTDLL_INTERNAL_API_END;
_WUM_NTDLL_INTERNAL_API void
NtOpenThreadTokenEx(void) _WUM_NTDLL_INTERNAL_API_END;
_WUM_NTDLL_INTERNAL_API void
NtOpenTimer(void) _WUM_NTDLL_INTERNAL_API_END;
_WUM_NTDLL_INTERNAL_API void
NtPowerInformation(void) _WUM_NTDLL_INTERNAL_API_END;
_WUM_NTDLL_INTERNAL_API void
NtPrivilegeCheck(void) _WUM_NTDLL_INTERNAL_API_END;
_WUM_NTDLL_INTERNAL_API void
NtProtectVirtualMemory(void) _WUM_NTDLL_INTERNAL_API_END;
_WUM_NTDLL_INTERNAL_API void
NtPulseEvent(void) _WUM_NTDLL_INTERNAL_API_END;
_WUM_NTDLL_INTERNAL_API void
NtQueryAttributesFile(void) _WUM_NTDLL_INTERNAL_API_END;
_WUM_NTDLL_INTERNAL_API void
NtQueryDefaultLocale(void) _WUM_NTDLL_INTERNAL_API_END;
_WUM_NTDLL_INTERNAL_API void
NtQueryDefaultUILanguage(void) _WUM_NTDLL_INTERNAL_API_END;
_WUM_NTDLL_INTERNAL_API void
NtQueryDirectoryFile(void) _WUM_NTDLL_INTERNAL_API_END;
_WUM_NTDLL_INTERNAL_API void
NtQueryDirectoryObject(void) _WUM_NTDLL_INTERNAL_API_END;
_WUM_NTDLL_INTERNAL_API void
NtQueryEaFile(void) _WUM_NTDLL_INTERNAL_API_END;
_WUM_NTDLL_INTERNAL_API void
NtQueryEvent(void) _WUM_NTDLL_INTERNAL_API_END;
_WUM_NTDLL_INTERNAL_API void
NtQueryFullAttributesFile(void) _WUM_NTDLL_INTERNAL_API_END;
_WUM_NTDLL_INTERNAL_API void
NtQueryInformationAtom(void) _WUM_NTDLL_INTERNAL_API_END;
_WUM_NTDLL_INTERNAL_API void
NtQueryInformationFile(void) _WUM_NTDLL_INTERNAL_API_END;
_WUM_NTDLL_INTERNAL_API void
NtQueryInformationJobObject(void) _WUM_NTDLL_INTERNAL_API_END;
_WUM_NTDLL_INTERNAL_API void
NtQueryInformationProcess(void) _WUM_NTDLL_INTERNAL_API_END;
_WUM_NTDLL_INTERNAL_API void
NtQueryInformationThread(void) _WUM_NTDLL_INTERNAL_API_END;
_WUM_NTDLL_INTERNAL_API void
NtQueryInformationToken(void) _WUM_NTDLL_INTERNAL_API_END;
_WUM_NTDLL_INTERNAL_API void
NtQueryInstallUILanguage(void) _WUM_NTDLL_INTERNAL_API_END;
_WUM_NTDLL_INTERNAL_API void
NtQueryIoCompletion(void) _WUM_NTDLL_INTERNAL_API_END;
_WUM_NTDLL_INTERNAL_API void
NtQueryKey(void) _WUM_NTDLL_INTERNAL_API_END;
_WUM_NTDLL_INTERNAL_API void
NtQueryMultipleValueKey(void) _WUM_NTDLL_INTERNAL_API_END;
_WUM_NTDLL_INTERNAL_API void
NtQueryMutant(void) _WUM_NTDLL_INTERNAL_API_END;
_WUM_NTDLL_INTERNAL_API void
NtQueryObject(void) _WUM_NTDLL_INTERNAL_API_END;
_WUM_NTDLL_INTERNAL_API void
NtQueryPerformanceCounter(void) _WUM_NTDLL_INTERNAL_API_END;
_WUM_NTDLL_INTERNAL_API void
NtQuerySection(void) _WUM_NTDLL_INTERNAL_API_END;
_WUM_NTDLL_INTERNAL_API void
NtQuerySecurityObject(void) _WUM_NTDLL_INTERNAL_API_END;
_WUM_NTDLL_INTERNAL_API void
NtQuerySemaphore(void) _WUM_NTDLL_INTERNAL_API_END;
_WUM_NTDLL_INTERNAL_API void
NtQuerySymbolicLinkObject(void) _WUM_NTDLL_INTERNAL_API_END;
_WUM_NTDLL_INTERNAL_API void
NtQuerySystemEnvironmentValue(void) _WUM_NTDLL_INTERNAL_API_END;
_WUM_NTDLL_INTERNAL_API void
NtQuerySystemEnvironmentValueEx(void) _WUM_NTDLL_INTERNAL_API_END;
_WUM_NTDLL_INTERNAL_API void
NtQuerySystemInformation(void) _WUM_NTDLL_INTERNAL_API_END;
_WUM_NTDLL_INTERNAL_API void
NtQuerySystemTime(void) _WUM_NTDLL_INTERNAL_API_END;
_WUM_NTDLL_INTERNAL_API void
NtQueryTimer(void) _WUM_NTDLL_INTERNAL_API_END;
_WUM_NTDLL_INTERNAL_API void
NtQueryTimerResolution(void) _WUM_NTDLL_INTERNAL_API_END;
_WUM_NTDLL_INTERNAL_API void
NtQueryValueKey(void) _WUM_NTDLL_INTERNAL_API_END;
_WUM_NTDLL_INTERNAL_API void
NtQueryVirtualMemory(void) _WUM_NTDLL_INTERNAL_API_END;
_WUM_NTDLL_INTERNAL_API void
NtQueryVolumeInformationFile(void) _WUM_NTDLL_INTERNAL_API_END;
_WUM_NTDLL_INTERNAL_API void
NtQueueApcThread(void) _WUM_NTDLL_INTERNAL_API_END;
_WUM_NTDLL_INTERNAL_API void
NtRaiseException(void) _WUM_NTDLL_INTERNAL_API_END;
_WUM_NTDLL_INTERNAL_API void
NtRaiseHardError(void) _WUM_NTDLL_INTERNAL_API_END;
_WUM_NTDLL_INTERNAL_API void
NtReadFile(void) _WUM_NTDLL_INTERNAL_API_END;
_WUM_NTDLL_INTERNAL_API void
NtReadFileScatter(void) _WUM_NTDLL_INTERNAL_API_END;
_WUM_NTDLL_INTERNAL_API void
NtReadVirtualMemory(void) _WUM_NTDLL_INTERNAL_API_END;
_WUM_NTDLL_INTERNAL_API void
NtRegisterThreadTerminatePort(void) _WUM_NTDLL_INTERNAL_API_END;
_WUM_NTDLL_INTERNAL_API void
NtReleaseKeyedEvent(void) _WUM_NTDLL_INTERNAL_API_END;
_WUM_NTDLL_INTERNAL_API void
NtReleaseMutant(void) _WUM_NTDLL_INTERNAL_API_END;
_WUM_NTDLL_INTERNAL_API void
NtReleaseSemaphore(void) _WUM_NTDLL_INTERNAL_API_END;
_WUM_NTDLL_INTERNAL_API void
NtRemoveIoCompletion(void) _WUM_NTDLL_INTERNAL_API_END;
_WUM_NTDLL_INTERNAL_API void
NtReplaceKey(void) _WUM_NTDLL_INTERNAL_API_END;
_WUM_NTDLL_INTERNAL_API void
NtReplyWaitReceivePort(void) _WUM_NTDLL_INTERNAL_API_END;
_WUM_NTDLL_INTERNAL_API void
NtRequestWaitReplyPort(void) _WUM_NTDLL_INTERNAL_API_END;
_WUM_NTDLL_INTERNAL_API void
NtResetEvent(void) _WUM_NTDLL_INTERNAL_API_END;
_WUM_NTDLL_INTERNAL_API void
NtResetWriteWatch(void) _WUM_NTDLL_INTERNAL_API_END;
_WUM_NTDLL_INTERNAL_API void
NtRestoreKey(void) _WUM_NTDLL_INTERNAL_API_END;
_WUM_NTDLL_INTERNAL_API void
NtResumeThread(void) _WUM_NTDLL_INTERNAL_API_END;
_WUM_NTDLL_INTERNAL_API void
NtSaveKey(void) _WUM_NTDLL_INTERNAL_API_END;
_WUM_NTDLL_INTERNAL_API void
NtSecureConnectPort(void) _WUM_NTDLL_INTERNAL_API_END;
_WUM_NTDLL_INTERNAL_API void
NtSetContextThread(void) _WUM_NTDLL_INTERNAL_API_END;
_WUM_NTDLL_INTERNAL_API void
NtSetDefaultLocale(void) _WUM_NTDLL_INTERNAL_API_END;
_WUM_NTDLL_INTERNAL_API void
NtSetDefaultUILanguage(void) _WUM_NTDLL_INTERNAL_API_END;
_WUM_NTDLL_INTERNAL_API void
NtSetEaFile(void) _WUM_NTDLL_INTERNAL_API_END;
_WUM_NTDLL_INTERNAL_API void
NtSetEvent(void) _WUM_NTDLL_INTERNAL_API_END;
_WUM_NTDLL_INTERNAL_API void
NtSetInformationFile(void) _WUM_NTDLL_INTERNAL_API_END;
_WUM_NTDLL_INTERNAL_API void
NtSetInformationJobObject(void) _WUM_NTDLL_INTERNAL_API_END;
_WUM_NTDLL_INTERNAL_API void
NtSetInformationKey(void) _WUM_NTDLL_INTERNAL_API_END;
_WUM_NTDLL_INTERNAL_API void
NtSetInformationObject(void) _WUM_NTDLL_INTERNAL_API_END;
_WUM_NTDLL_INTERNAL_API void
NtSetInformationProcess(void) _WUM_NTDLL_INTERNAL_API_END;
_WUM_NTDLL_INTERNAL_API void
NtSetInformationThread(void) _WUM_NTDLL_INTERNAL_API_END;
_WUM_NTDLL_INTERNAL_API void
NtSetInformationToken(void) _WUM_NTDLL_INTERNAL_API_END;
_WUM_NTDLL_INTERNAL_API void
NtSetIntervalProfile(void) _WUM_NTDLL_INTERNAL_API_END;
_WUM_NTDLL_INTERNAL_API void
NtSetIoCompletion(void) _WUM_NTDLL_INTERNAL_API_END;
_WUM_NTDLL_INTERNAL_API void
NtSetSecurityObject(void) _WUM_NTDLL_INTERNAL_API_END;
_WUM_NTDLL_INTERNAL_API void
NtSetSystemInformation(void) _WUM_NTDLL_INTERNAL_API_END;
_WUM_NTDLL_INTERNAL_API void
NtSetSystemTime(void) _WUM_NTDLL_INTERNAL_API_END;
_WUM_NTDLL_INTERNAL_API void
NtSetTimer(void) _WUM_NTDLL_INTERNAL_API_END;
_WUM_NTDLL_INTERNAL_API void
NtSetTimerResolution(void) _WUM_NTDLL_INTERNAL_API_END;
_WUM_NTDLL_INTERNAL_API void
NtSetValueKey(void) _WUM_NTDLL_INTERNAL_API_END;
_WUM_NTDLL_INTERNAL_API void
NtSetVolumeInformationFile(void) _WUM_NTDLL_INTERNAL_API_END;
_WUM_NTDLL_INTERNAL_API void
NtShutdownSystem(void) _WUM_NTDLL_INTERNAL_API_END;
_WUM_NTDLL_INTERNAL_API void
NtSignalAndWaitForSingleObject(void) _WUM_NTDLL_INTERNAL_API_END;
_WUM_NTDLL_INTERNAL_API void
NtSuspendThread(void) _WUM_NTDLL_INTERNAL_API_END;
_WUM_NTDLL_INTERNAL_API void
NtSystemDebugControl(void) _WUM_NTDLL_INTERNAL_API_END;
_WUM_NTDLL_INTERNAL_API void
NtTerminateJobObject(void) _WUM_NTDLL_INTERNAL_API_END;
_WUM_NTDLL_INTERNAL_API void
NtTerminateThread(void) _WUM_NTDLL_INTERNAL_API_END;
_WUM_NTDLL_INTERNAL_API void
NtUnloadDriver(void) _WUM_NTDLL_INTERNAL_API_END;
_WUM_NTDLL_INTERNAL_API void
NtUnloadKey(void) _WUM_NTDLL_INTERNAL_API_END;
_WUM_NTDLL_INTERNAL_API void
NtUnlockFile(void) _WUM_NTDLL_INTERNAL_API_END;
_WUM_NTDLL_INTERNAL_API void
NtUnlockVirtualMemory(void) _WUM_NTDLL_INTERNAL_API_END;
_WUM_NTDLL_INTERNAL_API void
NtUnmapViewOfSection(void) _WUM_NTDLL_INTERNAL_API_END;
_WUM_NTDLL_INTERNAL_API void
NtWaitForKeyedEvent(void) _WUM_NTDLL_INTERNAL_API_END;
_WUM_NTDLL_INTERNAL_API void
NtWaitForMultipleObjects(void) _WUM_NTDLL_INTERNAL_API_END;
_WUM_NTDLL_INTERNAL_API void
NtWaitForSingleObject(void) _WUM_NTDLL_INTERNAL_API_END;
_WUM_NTDLL_INTERNAL_API void
NtWriteFileGather(void) _WUM_NTDLL_INTERNAL_API_END;
_WUM_NTDLL_INTERNAL_API void
NtWriteVirtualMemory(void) _WUM_NTDLL_INTERNAL_API_END;
_WUM_NTDLL_INTERNAL_API void
NtYieldExecution(void) _WUM_NTDLL_INTERNAL_API_END;

#endif
